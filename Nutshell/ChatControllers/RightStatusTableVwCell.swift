//
//  RightStatusTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 01/04/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class RightStatusTableVwCell: UITableViewCell {

    @IBOutlet weak var loginUserImage: UIImageView!
   // @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
   // @IBOutlet weak var otherUserNameLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
   // @IBOutlet weak var mainVw: UIView!
    @IBOutlet weak var statusBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
