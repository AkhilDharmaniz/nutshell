//
//  ChatVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit
import SocketIO
import IQKeyboardManagerSwift
import SDWebImage
import SVProgressHUD

class ChatVC: UIViewController,UITextViewDelegate {
    var movetoroot = Bool()
    var roomId = ""
    var senderId = ""
    var pagingDone = false
    var responseArray =  [[String:AnyObject]]()
    var refreshControl =  UIRefreshControl()
    var receiverName = ""
    var LoginUserName = ""
    var LoginUserImage = ""
    var Message_Type = ""
    var profileImg = ""
    var responseDict = [String:AnyObject]()
    var appDelegate  = AppDelegate()
    var pageCount = 0
    var fromAppDelegate: String?
    let textViewMaxHeight: CGFloat = 120
    var message = String()
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var messageTV: UITextView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var bottomChatViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageTV.delegate = self
        messageTV.isScrollEnabled = false
        messageTV.textContainerInset = UIEdgeInsets(top: 7, left: 13, bottom: 11, right: 13)
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        userNameLbl.text = receiverName
        tabBarController?.tabBar.isHidden = true
        let refreshView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 0))
        chatTableView.insertSubview(refreshView, at: 0)
        refreshControl.addTarget(self, action: #selector(reloadtV), for: .valueChanged)
        refreshView.addSubview(refreshControl)
        pageCount = 1
        responseArray.removeAll()
        self.GetChatApi()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            debugPrint("connected")
            let socketConnectionStatus = SocketManger.shared.socket.status
            switch socketConnectionStatus {
            case .connected:
                debugPrint("socket connected")
                SocketManger.shared.socket.emit("ConncetedChat", self.roomId)
                self.newMessageSocketOn()
            case .connecting:
                debugPrint("socket connecting")
            case .disconnected:
                debugPrint("socket disconnected")
                debugPrint("socket not connected")
                SocketManger.shared.socket.connect()
                self.connectSocketOn()
                self.newMessageSocketOn()
            case .notConnected:
                debugPrint("socket not connected")
                SocketManger.shared.socket.connect()
                self.connectSocketOn()
                self.newMessageSocketOn()
            }
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func textViewDidChange(_ textView: UITextView) {
        messageTV.isScrollEnabled = true
           // get the current height of your text from the content size
           var height = textView.contentSize.height
           // clamp your height to desired values
           if height > 90 {
               height = 90
           } else if height < 50 {
               height = 33
           }
      //  messageTV.isScrollEnabled = true
           // update the constraint
           messageTVHeightConstraint.constant = height
           messageViewHeightConstraint.constant = height + 30
           self.view.layoutIfNeeded()
       }
    
    
       func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
         //  sendMessageBtn.isUserInteractionEnabled = true

           if textView.text.count == 0 {
               messageTV.isScrollEnabled = false
//               messageTVHeightConstraint.constant = 33
//               messageViewHeightConstraint.constant = 60
               
           }else  {
               //  messagePlaceholderLbl.isHidden = true
           }
           
           return true
       }
    
    
    
    
    
    @IBAction func backBtnClicked(_ sender:UIButton) {
        messageTV.resignFirstResponder()
        if fromAppDelegate == "YES"{
            self.navigationController?.popViewController(animated: true)
            let storyB = UIStoryboard(name: "Main", bundle: nil)
            if let SWRC = storyB.instantiateViewController(withIdentifier: "Tab") as? TabBarClass{
                navigationController?.pushViewController(SWRC, animated: false)
            }
        }else{
            self.updateMessageSeen(chatUserId:self.roomId)
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc  override func dismissKeyboard() {
           view.endEditing(false)
       }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatVC.self)
           IQKeyboardManager.shared.disabledToolbarClasses = [ChatVC.self]
           NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
           if let userInfo = notification.userInfo {
               let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
               let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
               bottomChatViewConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
               DispatchQueue.main.async(execute: {
                   self.chatTableView.reloadData()
                   if  self.pageCount == 1 {
                       if self.responseArray.count > 0 {
                           let ip = IndexPath(row: self.responseArray.count - 1, section: 0)
                           self.chatTableView.scrollToRow(at: ip, at: .bottom, animated: false)
                       }
                   }
               })
               UIView.animate(withDuration: 0.1, animations: { () -> Void in
                   self.view.layoutIfNeeded()
                   self.scrollEnd()
               })
           }
       }
    
    
    
       func scrollEnd(){
              if responseArray.count != 0{
                  let lastItemIndex = self.chatTableView.numberOfRows(inSection: 0) - 1
                  let indexPath:IndexPath = IndexPath(item: lastItemIndex, section: 0)
                  self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
              }
          }

    func connectSocketOn(){
           SocketManger.shared.onConnect {
               SocketManger.shared.socket.emit("ConncetedChat", self.roomId)
           }
       }
       open func updateMessageSeen(chatUserId:String){
           let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let chatData = Constant.shared.baseUrl + Constant.shared.UnseenChatApi
           
           let params:[String:Any] = [
               "user_id":idValue,
               "room_id":chatUserId
           ]
           AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
            _ = response["status"] as! Int
           })
           { (error) in
               print(error)
           }
       }
    
    func newMessageSocketOn(){
          SocketManger.shared.handleNewMessage { (message) in
              print(message)
              //  let data = message["data"] as? [String:AnyObject] ?? [:]
              self.responseArray.append(message as [String:AnyObject])
            UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
              DispatchQueue.main.async(execute: {
                  self.chatTableView.reloadData()
                  if  self.pageCount == 1 {
                      if self.responseArray.count > 0 {
                          let ip = IndexPath(row: self.responseArray.count - 1, section: 0)
                          self.chatTableView.scrollToRow(at: ip, at: .bottom, animated: false)
                      }
                  }
              })
          }
      }
    
    func joinedMessageSocketOn(){
           SocketManger.shared.handleJoinedMessage { (message) in
           }
       }
       func typeSocketOn(){
           SocketManger.shared.handleUserTyping { (trueIndex) in
           }
       }
       @objc func reloadtV() {
             if responseArray.count != 0{
                 if ((responseDict["last_page"] as? String ?? "") == "FALSE") {
                     pageCount = pageCount + 1
                    //self.responseArray.removeAll()
                     GetChatApi()
                 }
             }
             self.refreshControl.endRefreshing()
         }
         func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             textField.resignFirstResponder()
             return true
         }
    
    func GetChatApi(){
           let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let url = Constant.shared.baseUrl + Constant.shared.GetChatListing
           var params = [String:Any]()
        params = ["user_id":userid,"room_id":roomId,"page_no":"\(pageCount)"]
           print(params)
          // AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
           AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
           //    AFWrapperClass.svprogressHudDismiss(view: self)
               self.responseDict = dict as? [String:AnyObject] ?? [:]
               let result = dict as AnyObject
               print(result)
            _ = result["message"] as? String ?? ""
               let status = result["status"] as? Int ?? 0
               if status == 1{
               self.responseArray.removeAll()
                   self.responseArray = result["all_messages"] as? [[String:AnyObject]] ?? [[:]]
                   if  self.responseArray.count != 0 {
                       if Int(truncating: NSNumber(value: self.pageCount )) > 1 {
                           var firstPageArray = UserDefaults.standard.array(forKey: "FirstPageArray")
                           let pageDictArray = result["all_messages"] as? [[String:AnyObject]] ?? [[:]]
                           for obj in pageDictArray {
                               firstPageArray?.insert(obj, at: 0)
                               self.responseArray = (firstPageArray as! [[String : AnyObject]])
                           }
                           UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
                           UserDefaults.standard.synchronize()
                       } else {
                           let reversedArr = result["all_messages"] as? [[String:AnyObject]] ?? [[:]]
                           let reversedArray = ((reversedArr as NSArray?)?.reverseObjectEnumerator())?.allObjects
                           self.responseArray = reversedArray! as! [[String : AnyObject]]
                           UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
                           UserDefaults.standard.synchronize()
                       }
                       
                       
                       DispatchQueue.main.async(execute: {
                           self.chatTableView.reloadData()
                           if  self.pageCount == 1 {
                               if self.responseArray.count > 0 {
                                   let ip = IndexPath(row: self.responseArray.count-1, section: 0)
                                   self.chatTableView.scrollToRow(at: ip, at: .bottom, animated: false)
                               }
                           }
                       })
                   }
               }
           }) { (error) in
               alert("Nutshell", message: error.localizedDescription, view: self)
               AFWrapperClass.svprogressHudDismiss(view: self)
           }
       }
    
    func addMessageApi(){
           let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let url = Constant.shared.baseUrl + Constant.shared.addMessageApi
           var params = [String:Any]()
           
        params = ["room_id":roomId,"sender_id":userid,"message":messageTV.text!,"receiver_id" :senderId,"message_type":Message_Type,"status_id":""]
        //    AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
           AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
         //       AFWrapperClass.svprogressHudDismiss(view: self)
               let result = dict as AnyObject
               print(result)
            _ = result["message"] as? String ?? ""
               let status = result["status"] as? Int ?? 0
               if status == 1{
                
                   let appendData = result["data"] as? [String:Any] ?? [:]
                   SocketManger.shared.socket.emit("newMessage",self.roomId,appendData)
                   self.responseArray.append(appendData as [String : AnyObject])
                UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
                  // self.messageTV.resignFirstResponder()
                   self.messageTV.text = ""
                  // self.messageTVHeightConstraint.constant = 33
                   //  self.messagePlaceholderLbl.isHidden = false
                //   self.messageViewHeightConstraint.constant = 60
                   
                   DispatchQueue.main.async(execute: {
                       self.chatTableView.reloadData()
                       if  self.pageCount == 1 {
                           if self.responseArray.count > 0 {
                               let ip = IndexPath(row: self.responseArray.count - 1, section: 0)
                               self.chatTableView.scrollToRow(at: ip, at: .bottom, animated: false)
                           }
                       }
                   })
               }
           }) { (error) in
               alert("Nutshell", message: error.localizedDescription, view: self)
            AFWrapperClass.svprogressHudDismiss(view: self)
               //  AFWrapperClass.svprogressHudDismiss(view: self)
           }
       }
    
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        let trimmedString = messageTV.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if trimmedString == ""{
        }else{
          //  messageTV.resignFirstResponder()
            addMessageApi()
            messageTV.text = ""
        }
    }
}

extension ChatVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let respDict = responseArray[indexPath.row]
        let userid = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let senderId = respDict["sender_id"] as? String ?? ""
        let messageType = respDict["message_type"] as? String ?? ""
        if messageType == "0" {
            if  userid == senderId {
                var cell = tableView.dequeueReusableCell(withIdentifier: "identifier") as? RightMessageTableViewCell
                if cell == nil {
                    let arr = Bundle.main.loadNibNamed("RightMessageTableViewCell", owner: self, options: nil)
                    cell = arr?[0] as? RightMessageTableViewCell
                }
                let dateStr = respDict["creation_date"] as? String ?? ""
                let date = Date(timeIntervalSince1970:  dateStr.doubleValue)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
                _ = dateFormatter.string(from: date)
                cell?.timeLabel.text = dateStr
                cell?.messageLBL.text = respDict["message"] as? String ?? ""
                let photoStr = LoginUserImage
                cell?.userLoginIImage.sd_setImage(with: URL(string:photoStr ), placeholderImage:UIImage(named: "img1"))
                return cell!
                
            }
            else {
                var cell1 = tableView.dequeueReusableCell(withIdentifier: "identifier") as? LeftMessageTableViewCell
                if cell1 == nil {
                    let arr = Bundle.main.loadNibNamed("LeftMessageTableViewCell", owner: self, options: nil)
                    cell1 = arr?[0] as? LeftMessageTableViewCell
                }
                let respDict = responseArray[indexPath.row]
                let dateStr = respDict["creation_date"] as? String ?? ""
                _ = respDict["message_type"] as? Int ?? 0
                let date = Date(timeIntervalSince1970:  dateStr.doubleValue)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
                _ = dateFormatter.string(from: date)
                cell1?.timeLabel.text = dateStr
                cell1?.messageLBL.text = respDict["message"] as? String ?? ""
                let recieverDetailDict = self.responseDict["receiver_detail"] as? [String:AnyObject] ?? [:]
                cell1?.recieverUserNameLbl.text = "\(recieverDetailDict["username"] as? String ?? ""),"
                let photoStr = recieverDetailDict["photo"] as? String
                cell1?.senderTxtProfileImageView.sd_setImage(with: URL(string:photoStr ?? ""), placeholderImage:UIImage(named: "img1"))
                return cell1!
                
            }
        }else {
            if  userid == senderId {
                
                var cell = tableView.dequeueReusableCell(withIdentifier: "RightStatusTableVwCell") as? RightStatusTableVwCell
                if cell == nil {
                    let arr = Bundle.main.loadNibNamed("RightStatusTableVwCell", owner: self, options: nil)
                    cell = arr?[0] as? RightStatusTableVwCell
                }
                cell?.messageLbl.text = respDict["message"] as? String ?? ""
                let dateStr = respDict["creation_date"] as? String ?? ""
                let date = Date(timeIntervalSince1970:  dateStr.doubleValue)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
                _ = dateFormatter.string(from: date)
                cell?.timeLbl.text = dateStr
                let photoStr = LoginUserImage
                let statusStr = respDict["status_image"] as? String
                cell?.loginUserImage.sd_setImage(with: URL(string:photoStr ), placeholderImage:UIImage(named: "img1"))
                cell?.statusImage.sd_setImage(with: URL(string:statusStr ?? ""), placeholderImage:UIImage(named: "placeholderF"))
                cell?.statusBtn.tag = indexPath.row
                cell?.statusBtn.addTarget(self, action: #selector(statusImageOpenAction(_:)), for: .touchUpInside)
                return cell!
                
            }
            else {
                var cell1 = tableView.dequeueReusableCell(withIdentifier: "LeftStatusTableVwCell") as? LeftStatusTableVwCell
                if cell1 == nil {
                    let arr = Bundle.main.loadNibNamed("LeftStatusTableVwCell", owner: self, options: nil)
                    cell1 = arr?[0] as? LeftStatusTableVwCell
                }
                let respDict = responseArray[indexPath.row]
                cell1?.messageLbl.text = respDict["message"] as? String ?? ""
                let dateStr = respDict["creation_date"] as? String ?? ""
                let date = Date(timeIntervalSince1970:  dateStr.doubleValue)
                let dateFormatter = DateFormatter()
                dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
                dateFormatter.locale = NSLocale.current
                dateFormatter.dateFormat = "h:mm a" //Specify your format that you want
                _ = dateFormatter.string(from: date)
                cell1?.timeLbl.text = dateStr
                let photoStr = respDict["photo"] as? String ?? ""
                let statusImage = respDict["status_image"] as? String ?? ""
                cell1?.loginUserImage.sd_setImage(with: URL(string:photoStr), placeholderImage:UIImage(named: "img1"))
                cell1?.statusImage.sd_setImage(with: URL(string:statusImage), placeholderImage:UIImage(named: "placeholderF"))
                cell1?.otherUserNameLbl.text = "\(respDict["username"] as? String ?? ""),"
                cell1?.statusBtn.tag = indexPath.row
                cell1?.statusBtn.addTarget(self, action: #selector(statusImageOpenAction(_:)), for: .touchUpInside)
                return cell1!
            }
        }
    }
    @objc func statusImageOpenAction(_ sender: UIButton){
        
        if responseArray[sender.tag]["status_image"] as? String ?? "1" == "1" {
            print("stop")
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
        vc.imgString = responseArray[sender.tag]["status_image"] as? String ?? ""
        self.navigationController?.pushViewController(vc, animated: false)
        }
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension UIImageView {
    public func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}
extension String {
    
    /// the length of the string
    var length: Int {
        return self.count
    }
    
    /// Get substring, e.g. "ABCDE".substring(index: 2, length: 3) -> "CDE"
    ///
    /// - parameter index:  the start index
    /// - parameter length: the length of the substring
    ///
    /// - returns: the substring
    public func substring(index: Int, length: Int) -> String {
        if self.length <= index {
            return ""
        }
        let leftIndex = self.index(self.startIndex, offsetBy: index)
        if self.length <= index + length {
            return self.substring(from: leftIndex)
        }
        let rightIndex = self.index(self.endIndex, offsetBy: -(self.length - index - length))
        return self.substring(with: leftIndex..<rightIndex)
    }
    
    /// Get substring, e.g. -> "ABCDE".substring(left: 0, right: 2) -> "ABC"
    ///
    /// - parameter left:  the start index
    /// - parameter right: the end index
    ///
    /// - returns: the substring
    public func substring(left: Int, right: Int) -> String {
        if length <= left {
            return ""
        }
        let leftIndex = self.index(self.startIndex, offsetBy: left)
        if length <= right {
            return self.substring(from: leftIndex)
        }
        else {
            let rightIndex = self.index(self.endIndex, offsetBy: -self.length + right + 1)
            return self.substring(with: leftIndex..<rightIndex)
        }
    }
}
//MARK
extension String {
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
    func removeHTMLTag() -> String {
        let str = self.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)
        let replacedStr  = str.replacingOccurrences(of: "&nbsp;", with: "")
        return replacedStr
        
    }
}
