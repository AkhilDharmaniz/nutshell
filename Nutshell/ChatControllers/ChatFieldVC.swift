//
//  ChatFieldVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 16/02/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class ChatFieldVC: UIViewController {
    
    @IBOutlet weak var chatTableVw: UITableView!
    @IBOutlet weak var hideVw: UIView!
    var ChatLoginUser = String()
    var ChatLoginUserimage = String()
    var responseArray = [AnyObject]()
    lazy var globalRoomId = String()
    var pageCount = Int()
    var pushRoomId = String()
    var message = String()
    var respDict = [String:AnyObject]()
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideVw.isHidden = true
        pageCount = 1
        let refreshView = UIView(frame: CGRect(x: 0, y: 0, width: 55, height: 0))
        chatTableVw.insertSubview(refreshView, at: 0)
        refreshControl.addTarget(
            self,
            action: #selector(reloadtV),
            for: .valueChanged)
        refreshView.addSubview(refreshControl)
        chatTableVw.rowHeight = UITableView.automaticDimension
        chatTableVw.estimatedRowHeight = 70
    }
    override func viewWillAppear(_ animated: Bool) {
        pageCount = 1
         getListing()
    }
    @objc func reloadtV() {
        pageCount = 1
          getListing()
        refreshControl.endRefreshing()
       }
    
    // MARK: -- ChatListing
       open func getRoomIdApi(chatUserId:String){
           let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let chatData = Constant.shared.baseUrl + Constant.shared.requestChat
           let params:[String:Any] = [
               "user_id":idValue,
               "owner_id":chatUserId
           ]
           AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
               print(response)
               let status = response["status"] as? Int ?? 0
               print(status)
               let message = response["message"] as? String ?? ""
               if status == 1{
                
                   let resultDict = response as? [String:AnyObject] ?? [:]
                   self.globalRoomId = resultDict["room_id"] as? String ?? ""
                   
                   print("$$$$$$$$\(resultDict)")
                   
               }else{
                   alert("Nutshell", message: self.message, view: self)
               }
              self.chatTableVw.reloadData()
           })
           { (error) in
               print(error)
               alert("Nutshell", message: error.localizedDescription, view: self)
           }
       }
    
    func getListing() {
        AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
        //  let token = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let chatData = Constant.shared.baseUrl + Constant.shared.GetchatUser
        let parms : [String:Any] = ["user_id":idValue,"page_no":"\(pageCount)"]
        AFWrapperClass.requestPOSTURL(chatData, params: parms, success: { (response) in
            self.respDict = response as? [String : AnyObject] ?? [:]
            print(response)
            let status = response["status"] as? Int ?? 0
            print(status)
            let unreadcount = response["unread_message_count"] as? String ?? ""
            if unreadcount != ""{
                if unreadcount == "0" {
                    self.tabBarController?.tabBar.items![3].badgeValue = nil
                }else {
                    self.tabBarController?.tabBar.items![3].badgeValue = "\(unreadcount)"
                    self.tabBarController?.tabBar.items![3].badgeColor = .init(cgColor: #colorLiteral(red: 0.006663819775, green: 0.7878568769, blue: 0.1405803263, alpha: 1))
                    print(unreadcount,"opop")
                }
            }
            let message = response["message"] as? String ?? ""
            AFWrapperClass.svprogressHudDismiss(view: self)
            if status == 1{
                self.hideVw.isHidden = true
                let userName = response.value(forKeyPath: "user_data.user_id") as? String ?? "user"
                let userNames = response.value(forKeyPath: "user_data.username") as? String ?? "user"
                print(userNames,"userrrr")
                self.ChatLoginUser = userNames
                let UserImage = response.value(forKeyPath: "user_data.photo") as? String ?? ""
                self.ChatLoginUserimage = UserImage
                if ((response["all_users"] as? [[String:Any]]) != nil) {
                    
                    if Int(truncating: NSNumber(value: self.pageCount )) > 1 {
                        var firstPageArray = UserDefaults.standard.array(forKey: "FirstPageArray")
                        let pageDictArray = (response["all_users"] as? [[String:Any]])
                        //                                                        let reversedArray = ((pageDictArray as NSArray?)?.reverseObjectEnumerator())?.allObjects
                        
                        for obj in pageDictArray ?? [] {
                            guard let obj = obj as? [String : AnyObject] else {
                                continue
                            }
                            firstPageArray?.insert(obj, at: self.responseArray.count)
                            
                            self.responseArray = (firstPageArray as [AnyObject]?)!
                        }
                        
                        UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
                        UserDefaults.standard.synchronize()
                    } else {
                        
                        let reversedArr = (response["all_users"] as? [[String:Any]])
                        
                        //                                                            let reversedArray = ((reversedArr as NSArray?)?.reverseObjectEnumerator())?.allObjects
                        self.responseArray = reversedArr! as [AnyObject]
                        
                        UserDefaults.standard.set(self.responseArray, forKey: "FirstPageArray")
                        UserDefaults.standard.synchronize()
                    }
                }
                self.chatTableVw.reloadData()
                
            }else{
                self.hideVw.isHidden = false
                //                   alert("Nutshell", message: self.message, view: self)
            }
            
        })
        { (error) in
            AFWrapperClass.svprogressHudDismiss(view: self)
            print(error)
            alert("Nutshell", message: error.localizedDescription, view: self)
        }
    }
}
extension ChatFieldVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = chatTableVw.dequeueReusableCell(withIdentifier: "ChatfieldTableVwCell", for: indexPath) as! ChatfieldTableVwCell
        
        let rspDict = responseArray[indexPath.row] as? [String:AnyObject] ?? [:]
        cell.ChatuserImge.roundCorners([.topRight,.bottomLeft,.bottomRight], radius: 11)
        cell.ChatuserImge.sd_setImage(with: URL(string: rspDict["photo"] as? String ?? ""), placeholderImage: UIImage(named: "img1"))
        cell.selectionStyle = .none
        
        cell.lblChatName.text = rspDict["username"] as? String ?? ""
        // cell.userChatImage.image = UIImage(named: chatlistingArray[indexPath.row])
        cell.lblMessage.text = rspDict["message"] as? String ?? ""
       // cell.lblMessageCout.text = rspDict["unread_count"] as? String ?? ""
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rspDict = responseArray[indexPath.row] as? [String:AnyObject] ?? [:]
        
        let CMDVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
        
        let object = rspDict["room_no"] as? String ?? ""
        if object  == ""{
            let ownerId = UserDefaults.standard.object(forKey: "OwnerID") as? String ?? ""
            self.getRoomIdApi(chatUserId:ownerId)
            CMDVC?.roomId = globalRoomId
        }else{
            CMDVC?.roomId = "\(object)"
        }
        CMDVC?.senderId = rspDict["receiver_id"] as? String ?? ""
        
        CMDVC?.receiverName = rspDict["username"] as? String ?? ""
        CMDVC?.LoginUserName = self.ChatLoginUser
        CMDVC?.LoginUserImage = self.ChatLoginUserimage
                
        if let CMDVC = CMDVC {
            navigationController?.pushViewController(CMDVC, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == responseArray.count-1{
            
            if (respDict["last_page"] as? String ?? "" == "FALSE") {
                pageCount = pageCount + 1
                getListing()
                // self.NotificationList()
            }
        }
    }
}

