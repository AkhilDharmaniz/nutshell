//
//  SignInVC.swift
//  Nutshell
//
//  Created by Dharmani Apps on 12/06/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//

import UIKit
import CoreLocation
//import GoogleMaps
//import GooglePlaces
import MapKit

class SignInVC: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate  {
    var lat = String()
    var long = String()
    var lats = Double()
    var longs = Double()
    var placeLocationName = ""
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    @IBOutlet weak var selectedVwOne: UIView!
    @IBOutlet weak var selectedVwTwo: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    var message = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocation = nil
        setupLocationManager()
        self.locationManager.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }
    func setupLocationManager(){
           locationManager = CLLocationManager()
           locationManager.delegate = self
           self.locationManager.requestAlwaysAuthorization()
           locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
           locationManager.startUpdatingLocation()
       }
       func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
           if currentLocation == nil {
               currentLocation = locations.last
               locationManager.stopMonitoringSignificantLocationChanges()
               let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
               let latitude = String(format: "%.6f", locationValue.latitude)
            print(latitude,"lo")
            self.lats = locationValue.latitude
               let longitude = String(format: "%.6f", locationValue.longitude)
            print(longitude,"longitude")
            self.longs = locationValue.longitude
               UserDefaults.standard.set(latitude, forKey: "Latitude")
               UserDefaults.standard.set(longitude, forKey: "Longitude")
            convertLatLongToAddress(latitude:locationValue.latitude,longitude:locationValue.longitude)
               self.lat.append(latitude)
               self.long.append(longitude)
               //    print("locations = \(locationValue)")
               locationManager.stopUpdatingLocation()
           }
       }
    func convertLatLongToAddress(latitude:Double,longitude:Double){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:  lats, longitude: longs)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            // Location name
            if let locationName = placeMark?.location {
                print(locationName)
            }
            // Street address
            if let street = placeMark?.thoroughfare {
                print(street)
            }
            // City
            if let citys = placeMark?.subAdministrativeArea {
                print(citys)
                UserDefaults.standard.set(citys, forKey: "City")
                self.placeLocationName = citys
            }
            // City
            if let ppp = placeMark?.areasOfInterest {
                print(ppp,"lop")
            }
            // Zip code
            if let zip = placeMark?.isoCountryCode {
                print(zip)
            }
            // Country
            if let country = placeMark?.country {
                print(country)
            }
        })
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        //            setUpLocationPermissions()
        //                      convertLatLongToAddress(latitude: currentLattitude.doubleValue, longitude: currentLongitude.doubleValue)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            self.selectedVwOne.backgroundColor = #colorLiteral(red: 0.3454510868, green: 0.7843509316, blue: 0.759591043, alpha: 1)
        case txtPassword:
            self.selectedVwTwo.backgroundColor = #colorLiteral(red: 0.3412276506, green: 0.7920924425, blue: 0.7633551955, alpha: 1)
            
        default:break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            self.selectedVwOne.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtPassword:
            self.selectedVwTwo.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            
        default:break
        }
    }
    // MARK:--- InternetConnection
    override func viewDidAppear(_ animated: Bool) {
        if CheckInternet.Connection(){
        }
        else{
            alert("Nutshell", message: "Your Device is not connected with internet", view: self)
        }
    }
    func getData() {
        IJProgressView.shared.showProgressView()
        let signInUrl = Constant.shared.baseUrl + Constant.shared.signInUrl
        let deviceID = UserDefaults.standard.value(forKey: "deviceToken") as? String ?? "1234jfjjt"
        let userlatitude = UserDefaults.standard.value(forKey: "Latitude")
        let userlongitude = UserDefaults.standard.value(forKey: "Longitude")
        //            let userlatitudes = UserDefaults.standard.value(forKey: "Latitude")
        //                   let userlongitudes = UserDefaults.standard.value(forKey: "Longitude")
        //                   let userCitys = UserDefaults.standard.value(forKey: "City")
        let parms : [String:Any] = ["email": txtEmail.text!,"password": txtPassword.text!,"device_token":deviceID,"device_type":"1","latitude":userlatitude,"longitude":userlongitude]
        UserDefaults.standard.set(txtPassword.text, forKey: "oldPassword")
        AFWrapperClass.requestPOSTURL(signInUrl, params: parms, success: { (dict) in
            IJProgressView.shared.hideProgressView()
            if let result = dict as? [String:Any]{
                print(result)
                self.message = result["message"] as? String ?? ""
                let data = result["data"] as? [String:Any]
                print(data)
                let Id = data?["user_id"] as? String ?? ""
                UserDefaults.standard.set(Id, forKey: "Uid")
                print(Id)
                let username = data?["username"] as? String ?? ""
                print(username)
                UserDefaults.standard.set(username, forKey: "userName")
                let userImg = data?["profile_pic"] as? String ?? ""
                print(userImg)
                UserDefaults.standard.set(userImg, forKey: "userProfilePic")
                let email = data?["email"] as? String ?? ""
                print(email)
                UserDefaults.standard.set(email, forKey: "useremail")
                
                // self.usernameLabel.text = "\(username)"
                // UserDefaults.standard.set(id, forKey: "user_ids")
                let status = result["status"] as? Int ?? 0
                if status == 1{
                    UserDefaults.standard.set(true, forKey: "uid")
               //     UserDefaults.standard.set(true, forKey: "ISUSERLOGGEDIN")
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //   UserDefaults.standard.set(false, forKey: "uid")
                alert("Nutshell", message: self.message, view: self)
                }
            }
        }) { (error) in
        }
    }
    @IBAction func forgetPasswordBtnClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func signupBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func loginBtnCicked(_ sender: UIButton) {
        if (txtEmail.text?.isEmpty)!{
            UserDefaults.standard.set(false, forKey: "uid")
            ValidateData(strMessage: "Please enter email")
        }
        else if isValidEmail(testStr: (txtEmail.text)!) == false{
            ValidateData(strMessage: "Enter valid email")
        }
        else if (txtPassword.text?.isEmpty)!{
            UserDefaults.standard.set(false, forKey: "uid")
            ValidateData(strMessage: " Please enter password")
        }else{
            [self.txtEmail,self.txtPassword].map({$0?.resignFirstResponder()})
            //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
            //                self.navigationController?.pushViewController(vc, animated: true)
            self.getData()
        }
    }
}

