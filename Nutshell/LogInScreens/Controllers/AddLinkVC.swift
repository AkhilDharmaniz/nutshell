//
//  AddLinkVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 03/04/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit

class AddLinkVC: UIViewController,UITextFieldDelegate {
    var GetAddLinkArray = [GetAddLinkModel]()
    @IBOutlet weak var hideVw: UIView!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    var followerCount = ""
    var followingCount = ""
    @IBOutlet weak var linksAddVw: UIView!{
    didSet{
    linksAddVw.layer.cornerRadius = 20
    linksAddVw.clipsToBounds = true
    linksAddVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    }
    var phonepe = ""
    var rapido = ""
    var skype = ""
    var google = ""
    var googlepay = ""
    var instagram = ""
    var tiktok = ""
    var snackVideo = ""
    var movetoroot = Bool()
    var message = String()
    var CName = ""
    var CEmail = ""
    var C_ProfileImg = ""
    var LinkArray = [GetAddLinkModel]()
    var idArray = [String]()
    var linksArry = [String]()
   // var cellsArr = [AddLinkModel]()
    @IBOutlet weak var addLinksTableVw: UITableView!
    var LinksImg = ["phone-pay","rapido","skype","map","google-pay","instagram-tv","tiktok","snack-video"]
    var ProfileLinks = ["PhonePe","Rapido","Skype","Google","Google Pay","Instagram TV","Tiktok","snack video"]
    override func viewDidLoad() {
    super.viewDidLoad()
       print(followerCount,"poppp")
    self.lblUserName.text = followerCount
    self.lblRole.text = followingCount
   self.userProfileImage.sd_setImage(with: URL(string: C_ProfileImg), placeholderImage: UIImage(named: "img1"))
//    LinkArray = [AddLinkModel.init(id: "1", names: "PhonePe", image:#imageLiteral(resourceName: "phone-pay")),
//    AddLinkModel.init(id: "2", names: "Rapido", image: #imageLiteral(resourceName: "rapido")),
//    AddLinkModel.init(id: "3", names: "Google", image: #imageLiteral(resourceName: "map")),
//    AddLinkModel.init(id: "4", names: "Skype", image: #imageLiteral(resourceName: "skype")),
//    AddLinkModel.init(id: "5", names: "Google Pay", image: #imageLiteral(resourceName: "google-pay")),
//    AddLinkModel.init(id: "6", names: "Instagram TV", image: #imageLiteral(resourceName: "instagram-tv")),
//    AddLinkModel.init(id: "7", names: "Tiktok", image: #imageLiteral(resourceName: "tiktok")),
//    AddLinkModel.init(id: "8", names: "snack video", image: #imageLiteral(resourceName: "snack-video")),
//    AddLinkModel.init(id: "9", names: "snack", image: #imageLiteral(resourceName: "snack-video"))]
    addLinksTableVw.rowHeight = UITableView.automaticDimension
    addLinksTableVw.estimatedRowHeight = 100
    var tapGesture = UITapGestureRecognizer()
    // self.hideKeyboardTappedAround()
    tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(tap:)))
    tapGesture.numberOfTapsRequired = 1
    hideVw.addGestureRecognizer(tapGesture)
    }
    @objc func tapAction(tap:UITapGestureRecognizer){
    self.navigationController?.popViewController(animated: true)
       }
    @IBAction func saveBtnCLicked(_ sender: UIButton) {
        view.endEditing(true)
        linksArry.removeAll()
        idArray.removeAll()
    for i in 0..<GetAddLinkArray.count{
        if GetAddLinkArray[i].text == "https://" {
          
        }else{
            linksArry.append(GetAddLinkArray[i].text)
            idArray.append(GetAddLinkArray[i].social_id)
        }
    }
    if linksArry.count == 0 {
    alert("Nutshell", message: "Please add proper links", view: self)
    }else {
    AddLink()
    }
    }
    override func viewWillAppear(_ animated: Bool) {
        GetLinksAdd()
    }
    @IBAction func addLinkBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomLinkVC") as! CustomLinkVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
   func AddLink(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.AddLinks
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
//        let parms:[String:Any] = ["user_id":idValue,"phonepe":phonepe,"rapido":rapido,"skype":skype,"google":google,"googlepay":googlepay,"instagram":instagram,"tiktok":tiktok,"snackvideo":snackVideo]
    let parms:[String:Any] = ["user_id":idValue,"linkurl":linksArry,"social_id":idArray]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: "Links Added successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
            IJProgressView.shared.hideProgressView()
        }) { (Error) in
        }
    }
   // details[i]["id"] as? String ?? ""
    func GetLinksAdd() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetAddLinkApi
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            if status == 1{
                if let details = response["socialLinks"] as? [[String:Any]]{
                    print(details,"lll")
                    self.GetAddLinkArray.removeAll()
                    for i in 0..<details.count {
                        self.GetAddLinkArray.append(GetAddLinkModel(social_id: details[i]["social_id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", title: details[i]["title"] as? String ?? "", imageurl: details[i]["imageurl"] as? String ?? "", createDate: details[i]["createDate"] as? String ?? "", linkurl: details[i]["linkurl"] as? String ?? "", text: details[i]["linkurl"] as? String ?? "" == "" ? "https://" : details[i]["linkurl"] as? String ?? "" ))
                    }
                }
            }else{
                self.GetAddLinkArray.removeAll()
            }
            self.addLinksTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension AddLinkVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return GetAddLinkArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell2 = addLinksTableVw.dequeueReusableCell(withIdentifier: "LinkAddTableVwCell", for: indexPath) as? LinkAddTableVwCell {
            
            cell2.selectionStyle = .none
            cell2.txtLinks.attributedPlaceholder = NSAttributedString(string: GetAddLinkArray[indexPath.row].title, attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
           // cell2.linkImages.image = GetAddLinkArray[indexPath.row].
//            cell2.linkImages.sd_setImage(with: URL(string: GetAddLinkArray[indexPath.row].imageurl), placeholderImage: UIImage(named: ""))
            
            cell2.txtLinks.text = GetAddLinkArray[indexPath.row].text
            cell2.txtLinks.tag = indexPath.row
            cell2.txtLinks.addTarget(self, action: #selector(txtlink(sender:)), for: .editingDidEnd)
            let img = GetAddLinkArray[indexPath.row].imageurl
            if img == "" {
            cell2.lblLetterTitle.isHidden = false
                var firstLetter = self.GetAddLinkArray[indexPath.row].title.first?.description ?? ""
                print(firstLetter,"Data")
            cell2.lblLetterTitle.text = firstLetter
            cell2.lblLetterTitle.backgroundColor = UIColor.random
            }else {
            cell2.lblLetterTitle.isHidden = true
            cell2.linkImages.sd_setImage(with: URL(string: GetAddLinkArray[indexPath.row].imageurl), placeholderImage: UIImage(named: ""))
            }
            if indexPath.row == 3 {
                if cell2.txtLinks.text == "https://" {
                    cell2.txtLinks.text = ""
                }
                cell2.txtLinks.keyboardType = .phonePad
            }else {
                cell2.txtLinks.keyboardType = .asciiCapable
            }
//            if img == "" {
//                cell2.lblLetterTitle.isHidden = false
//            }else {
//                cell2.lblLetterTitle.isHidden = true
//            }
//            var firstLetter = self.GetAddLinkArray[indexPath.row].title.first?.description ?? ""
//            print(firstLetter,"Data")
//            cell2.lblLetterTitle.text = firstLetter
//            cell2.lblLetterTitle.backgroundColor = UIColor.random
//            cell2.delegate = self
//            cell2.txtLinks.delegate = self
//           linksArry.removeAll()
//           linksArry.append(cell2.txtLinks.text ?? "")
//            cell2.txtLinks.tag = indexPath.row
           // cellsArr.append(cell2)
            return cell2
        }
        return UITableViewCell()
    }
    @objc func txtlink(sender: UITextField){
        GetAddLinkArray[sender.tag].text = sender.text!
        addLinksTableVw.reloadData()
        }
//    func textFieldDidEndEditing(textField: UITextField) {
//           linksArry.append(textField.text!)
//           print(linksArry,"oooo")
//       }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

struct AddLinkModel {
    let id:String
    let names:String
    let image:UIImage
    init(id:String,names:String,image:UIImage) {
        self.id = id
        self.names = names
        self.image = image
    }
}
extension AddLinkVC:LinkProtocol{
    func stringpass(index1: String, index2: String) {
        print(index1)
        print(index2)
    }
}
