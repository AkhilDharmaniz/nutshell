//
//  AddPostVC.swift
//  Nutshell
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import IQKeyboardManagerSwift
import SDWebImage

class AddPostVC: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var addPostImgView: UIImageView!
    @IBOutlet weak var imgVideoView: UIView!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imgHeight: NSLayoutConstraint!
    
    var imgArray = [Data]()
    var thumbnailImg = Data()
    var addedData = 0
    var data = Data()
    var videoURLPath: URL!
    var keyboardHeight:CGFloat = 0.0
    
    @IBOutlet weak var categoryTV: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileData()
        categoryTV.delegate = self
        categoryTV.autocorrectionType = .no
        imgHeight.constant = 1
        imgVideoView.isHidden = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
                self.scrollViewBottom.constant = keyboardSize.height+70
                self.bottomConstraint.constant = keyboardSize.height
        }
    }
    //MARK:- viewWillDisappear
     override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.enable = true
    }
    // MARK:--- User Details
    func getProfileData(){
   //     IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.getUserDetails
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
        //    print(response)
            if let data = response["data"] as? [String:Any]{
                self.profileImg.sd_setImage(with: URL(string: data["photo"] as? String ?? ""), placeholderImage: UIImage(named: "img1"))
                self.nameLbl.text =  data["username"] as? String ?? ""
            }
            
        })
        { (error) in
            print(error)
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.bottomConstraint.constant = 0
        scrollViewBottom.constant = 70
    }
    open func choosePhoto() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.videoMaximumDuration = 40
        picker.videoQuality = .typeMedium
        picker.sourceType = .photoLibrary
        picker.mediaTypes = ["public.movie","public.image"]
        present(picker, animated: true)
    }
    
    @IBAction func closeImgVideoViewBtnAction(_ sender: UIButton) {
        addedData = 0
        imgHeight.constant = 1
        self.imgVideoView.isHidden = true
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true, completion: nil)
        imgVideoView.isHidden = false
        imgHeight.constant = UIScreen.main.bounds.size.height * 0.25
        if info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaType.rawValue)] as? String == "public.image" {
            
            if let imagedata = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage {
                addPostImgView.image = imagedata
                addPostImgView.contentMode = .scaleAspectFill
                addedData = 1
            }
            dismiss(animated: true, completion: nil)
            
        }else{
            if let videoURL = info[.mediaURL] as? URL {
                self.videoURLPath = videoURL
                play(url:videoURLPath)
                let urlString: String = videoURL.absoluteString
                let dispatchgroup = DispatchGroup()
                dispatchgroup.enter()
                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let outputurl = documentsURL.appendingPathComponent(urlString)
                var ur = outputurl
                IJProgressView.shared.showProgressView()
                self.convertVideo(toMPEG4FormatForVideo: videoURL as URL, outputURL: outputurl) { (session) in
                    ur = session.outputURL!
                    dispatchgroup.leave()
                }
                dispatchgroup.wait()
                data = NSData(contentsOf: videoURLPath as URL)! as Data
                addedData = 2
//                data = (NSData(contentsOf: ur as URL) ?? Data() as NSData) as Data
                do {
                    try data.write(to: URL(fileURLWithPath: "\(String(describing: videoURLPath))"), options: .atomic)
                    
                    IJProgressView.shared.hideProgressView()
                } catch {
                    IJProgressView.shared.hideProgressView()
                    print(error)
                }
            }
        }
        picker.dismiss(animated: true)
        
    }
    
    func play(url:URL)
    {
//        let player = AVPlayer(url: url)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        self.present(playerViewController, animated: true)
//        {
//            playerViewController.player!.play()
//        }
        
        do {
            let asset = AVURLAsset(url: url , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImageeee = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbNail = UIImage.init(cgImage: cgImageeee)
            addPostImgView.image = thumbNail
            addPostImgView.contentMode = .scaleAspectFill
            thumbnailImg = (addPostImgView.image?.jpegData(compressionQuality: 0.7))!
            print(thumbnailImg)
            
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
        }
    }
    
    @IBAction func playVideo(_ sender: Any) {
//        let player = AVPlayer(url:videoURLPath)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        self.present(playerViewController, animated: true) {
//            playerViewController.player?.play()
//        }
    }
    
    @IBAction func galleryBtnAction(_ sender: UIButton) {
        self.openGallary()
    }
    
    @IBAction func cameraBtnAction(_ sender: UIButton) {
        self.openCamera()
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title:Constant.shared.AppName, message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary()
    {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        picker.videoMaximumDuration = 40
        picker.videoQuality = .typeMedium
        picker.sourceType = .photoLibrary
        picker.mediaTypes = ["public.movie","public.image"]
        present(picker, animated: true)
    }
    
    @IBAction func addPostBtnAction(_ sender: Any) {
//        if addedData == 0 {
//            alert(Constant.shared.AppName, message: "Please select atleast one video or image for add post", view: self)
//        }else
        if categoryTV.text.count <= 0 {
            alert(Constant.shared.AppName, message: "Please add title for post", view: self)
        } else {
            addPostApi()
        }
    }
    
    func convertVideo(toMPEG4FormatForVideo inputURL: URL, outputURL: URL, handler: @escaping (AVAssetExportSession) -> Void) {
            let asset = AVURLAsset(url: inputURL as URL, options: nil)
            let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4
            exportSession.exportAsynchronously(completionHandler: {
                handler(exportSession)
            })
        }
    
    @IBAction func backBtnAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func addPostApi() {
        if addedData != 0{
        var compressedData = (addPostImgView.image?.jpegData(compressionQuality: 0.3))
   //     print(compressedData)
        if videoURLPath == nil {
            imgArray.removeAll()
            imgArray.append(compressedData!)
        }else{
            compressedData = data as Data?
            imgArray.removeAll()
            imgArray.append(compressedData!)
        }
        }
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let paramds = ["description":categoryTV.text ?? "","user_id":idValue , "type" : "1"] as [String : Any]
        self.view.endEditing(true)
        let strURL = Constant.shared.baseUrl + Constant.shared.addPost
        self.requestWith(endUrl: strURL , parameters: paramds)
    }
    
    func requestWith(endUrl: String, parameters: [AnyHashable : Any]){
        
        let url = endUrl /* your API url */

        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
            
        ]
        DispatchQueue.main.async {

        AFWrapperClass.svprogressHudShow(title:"Loading...", view:self)
        }

        AF.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as! String)
            }
            
            for i in 0..<self.imgArray.count{
                let imageData1 = self.imgArray[i]
                let imageData2 = self.thumbnailImg
                let ranStr = String.random(length: 7)
                if self.videoURLPath == nil {
                    multipartFormData.append(imageData1, withName: "uploads[\(i + 1)]" , fileName: ranStr + String(i + 1) + ".png", mimeType: "image/jpeg")
                    
                }else{
                    multipartFormData.append(imageData1, withName: "uploads[\(i + 1)]" , fileName: ranStr + String(i + 1) + ".mp4" , mimeType: "video/mp4")
                    multipartFormData.append(imageData2, withName: "video_thumbnail" , fileName: ranStr + String(i + 1) + ".png", mimeType: "image/jpeg")
                }
//                if imageData1.mimeType == "video/mp4"{
                    
//                }else{
//                    multipartFormData.append(imageData1, withName: "uploads[\(i + 1)]" , fileName: ranStr + String(i + 1) + ".jpg", mimeType: imageData1.mimeType)
//                }
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers, interceptor: nil, fileManager: .default)
        
        .uploadProgress(closure: { (progress) in
            print("Upload Progress: \(progress.fractionCompleted)")
            
        })
        .responseJSON { (response) in
            DispatchQueue.main.async {

            AFWrapperClass.svprogressHudDismiss(view: self)
            }

            print("Succesfully uploaded\(response)")
            let respDict =  response.value as? [String : AnyObject] ?? [:]
            if respDict.count != 0{
                self.navigationController?.popViewController(animated: true)

            }else{
                
            }
        }
    }
}

extension AVAsset {
    
    var videoThumbnail:UIImage? {
        let assetImageGenerator = AVAssetImageGenerator(asset: self)
        assetImageGenerator.appliesPreferredTrackTransform = true
        var time = self.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbNail = UIImage.init(cgImage: imageRef)
            print("Video Thumbnail genertated successfuly")
            return thumbNail
        } catch {
            print("error getting thumbnail video",error.localizedDescription)
            return nil
        }
    }
}
extension AddPostVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
     //   self.bottomConstraint.constant = keyboardHeight
    }
    func textViewDidChange(_ textView: UITextView) {
//        self.bottomConstraint.constant = keyboardHeight
//        scrollViewBottom.constant = keyboardHeight+70
    }
}
