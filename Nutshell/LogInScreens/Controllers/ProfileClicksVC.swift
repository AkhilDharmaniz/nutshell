//
//  ProfileClicksVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 23/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
class ProfileClicksVC: UIViewController,UITextFieldDelegate{
    @IBOutlet weak var hideVw: UIView!
    var phonepe = ""
    var rapido = ""
    var skype = ""
    var google = ""
    var googlepay = ""
    var instagram = ""
    var tiktok = ""
    var snackVideo = ""
    var movetoroot = Bool()
    var message = String()
    var Notificationimg = ["insta","facebook","twitter","snapchat","email","linkedin","whatsapp","youtube"]
    var NotificationuserName = ["Instagram","Facebook","Twitter","Snapchat","Email Address","Linkedin","Whatsapp","You tube"]
    var Clicks = ["0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks"]
    var LinksImg = ["phone-pay","rapido","skype","map","google-pay","instagram-tv","tiktok","snack-video"]
    var ProfileLinks = ["PhonePe","Rapido","Skype","Google","Google Pay","Instagram TV","Tiktok","snack video"]
    var CName = ""
    var CEmail = ""
    var C_ProfileImg = ""
    @IBOutlet weak var LinkProfileVw: UIView!
    @IBOutlet weak var ProfileClicksTableVw: UITableView!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtPhonePe: UITextField!
    @IBOutlet weak var txtRapido: UITextField!
    @IBOutlet weak var txtSkype: UITextField!
    @IBOutlet weak var txtGoogle: UITextField!
    @IBOutlet weak var txtGooglePay: UITextField!
    @IBOutlet weak var txtInstagramTv: UITextField!
    @IBOutlet weak var txtTikTok: UITextField!
    @IBOutlet weak var txtSnackVideo: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblUserName.text = CName
        self.lblUserEmail.text = CEmail
        self.userProfileImg.sd_setImage(with: URL(string: C_ProfileImg), placeholderImage: UIImage(named: "img1"))
        LinkProfileVw.layer.cornerRadius = 20
        LinkProfileVw.clipsToBounds = true
        LinkProfileVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        txtPhonePe.delegate = self
        txtRapido.delegate = self
        txtSkype.delegate = self
        txtGoogle.delegate = self
        txtGooglePay.delegate = self
        txtInstagramTv.delegate = self
        txtTikTok.delegate = self
        txtSnackVideo.delegate = self
        txtPhonePe.text = phonepe
        txtRapido.text = rapido
        txtSkype.text = skype
        txtGoogle.text = google
        txtGooglePay.text = googlepay
        txtInstagramTv.text = instagram
        txtTikTok.text = tiktok
        txtSnackVideo.text = snackVideo
        txtPhonePe.attributedPlaceholder = NSAttributedString(string: "PhonePe", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtRapido.attributedPlaceholder = NSAttributedString(string: "Rapido", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtSkype.attributedPlaceholder = NSAttributedString(string: "Skype", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtGoogle.attributedPlaceholder = NSAttributedString(string: "Google", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtGooglePay.attributedPlaceholder = NSAttributedString(string: "Google Pay", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtInstagramTv.attributedPlaceholder = NSAttributedString(string: "Instagram", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtTikTok.attributedPlaceholder = NSAttributedString(string: "Tiktok", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        txtSnackVideo.attributedPlaceholder = NSAttributedString(string: "snack video", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        var tapGesture = UITapGestureRecognizer()
        // self.hideKeyboardTappedAround()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(tap:)))
        tapGesture.numberOfTapsRequired = 1
        hideVw.addGestureRecognizer(tapGesture)
    }
    
    @objc func tapAction(tap:UITapGestureRecognizer){
        self.navigationController?.popViewController(animated: true)
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    @IBAction func saveBtnClicked(_ sender: UIButton) {
        //self.navigationController?.popViewController(animated: true)
        if txtPhonePe.text == ""  && txtRapido.text == "" && txtSkype.text == "" && txtGoogle.text == "" && txtGooglePay.text == "" && txtInstagramTv.text == "" && txtTikTok.text == "" && txtSnackVideo.text == "" {
          alert("Nutshell", message: "Please add link", view: self)
            print("empty")
        }
        else   if txtPhonePe.text != "" && verifyUrl(urlString:txtPhonePe.text) == false{
          alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtRapido.text != "" && verifyUrl(urlString:txtRapido.text) == false{
         alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtSkype.text != "" && verifyUrl(urlString:txtSkype.text) == false{
         alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtGoogle.text != "" && verifyUrl(urlString:txtGoogle.text) == false{
         alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtGooglePay.text != "" && verifyUrl(urlString:txtGooglePay.text) == false{
         alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtInstagramTv.text != "" && verifyUrl(urlString:txtInstagramTv.text) == false{
         alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtTikTok.text != "" && verifyUrl(urlString:txtTikTok.text) == false{
        alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else   if txtSnackVideo.text != "" && verifyUrl(urlString:txtSnackVideo.text) == false{
            alert("Nutshell", message: "Please add Valid link", view: self)
        }
        else{
            self.AddLink()
        }
    }    
    func AddLink(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.AddLinks
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"phonepe":txtPhonePe.text ?? "","rapido":txtRapido.text ?? "","skype":txtSkype.text ?? "","google":txtGoogle.text ?? "","googlepay":txtGooglePay.text ?? "","instagram":txtInstagramTv.text ?? "","tiktok":txtTikTok.text ?? "","snackvideo":txtSnackVideo.text ?? ""]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as! Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: "Links Added successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
            IJProgressView.shared.hideProgressView()
        }) { (Error) in
        }
    }
}
