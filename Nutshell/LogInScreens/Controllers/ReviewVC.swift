//
//  ReviewVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController {
    
    @IBOutlet weak var noDataVw: UIView!
    @IBOutlet weak var ReviewTableVw: UITableView!
    var ReviewImg = ["user","user-1","user-2","user-3","add-update"]
    var Reviewer = ["Jessica","Fazzel","Amen K","Priya J","Samira"]
    var ReviewName = ["Jiapel Ma liked your Profile","Mahmudal Husan added Facebook","Faxxel Kair send Follow Request","ulip cui added Instagram","Olha Bahularia accepted Request"]
    var ReviewRating = ["2.0","5.0","3.0","4.0","3.0"]
    var ReviewArray = [ReviewListModel]()
    var OtherUserId = String()
    var ProfileType = ""
    var message = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        ReviewTableVw.rowHeight = UITableView.automaticDimension
        ReviewTableVw.estimatedRowHeight = 100
        self.noDataVw.isHidden = true
        // Reset float rating view's background color
       
    }
    override func viewWillAppear(_ animated: Bool) {
         ReviewApi()
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addRatingBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BusinessAddReviewVC") as! BusinessAddReviewVC
        vc.OtherUserID = OtherUserId
        self.navigationController?.pushViewController(vc, animated: true)
//        if ProfileType == "1" {
//            
//        }else {
//            
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddRatingReviewVC") as! AddRatingReviewVC
//            vc.OtherUserID = OtherUserId
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    func ReviewApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let ReviewData = Constant.shared.baseUrl + Constant.shared.GetReviewApi
        let parms : [String:Any] = ["user_id":OtherUserId]
        AFWrapperClass.requestPOSTURL(ReviewData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message,"mm")
            if status == 1{
                self.noDataVw.isHidden = true
                if let details = response["reviwe_data"] as? [[String:Any]]{
                    print(details,"lll")
                    self.ReviewArray.removeAll()
                    for i in 0..<details.count {
                        self.ReviewArray.append(ReviewListModel(user_id: details[i]["user_id"] as? String ?? "", name: details[i]["username"] as? String ?? "", email: details[i]["email"] as? String ?? "", role: details[i]["role"] as? String ?? "", password: details[i]["password"] as? String ?? "", photo: details[i]["photo"] as? String ?? "", country: details[i]["country"] as? String ?? "", countryCode: details[i]["countryCode"] as? String ?? "", twitterId: details[i]["twitterId"] as? String ?? "", latitude: details[i]["latitude"] as? String ?? "", longitude: details[i]["longitude"] as? String ?? "", description: details[i]["description"] as? String ?? "", verified: details[i]["verified"] as? String ?? "", totalFollowing: details[i]["totalFollowing"] as? String ?? "", totalFollowers: details[i]["totalFollowers"] as? String ?? "", verificateCode: details[i]["verificateCode"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? "", disabled: details[i]["disabled"] as? String ?? "", allowPush: details[i]["allowPush"] as? String ?? "", device_type: details[i]["device_type"] as? String ?? "", device_token: details[i]["device_token"] as? String ?? "", status: details[i]["status"] as? String ?? "", changeStatus: details[i]["changeStatus"] as? String ?? "", review: details[i]["review"] as? String ?? "", id: details[i]["id"] as? String ?? "", ratingby: details[i]["ratingby"] as? String ?? "", ratingCount: details[i]["ratingCount"] as? String ?? "", createrreview: details[i]["createrreview"] as? String ?? "", rating: details[i]["rating"] as? String ?? "", profile_type: details[i]["profile_type"] as? String ?? ""))
                    }
                }
            }else{
                self.noDataVw.isHidden = false
             //   alert("Nutshell", message: self.message, view: self)
                self.ReviewArray.removeAll()
            }
            self.ReviewTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension ReviewVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReviewArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ReviewTableVw.dequeueReusableCell(withIdentifier: "ReviewTableVwCell", for: indexPath) as! ReviewTableVwCell
        cell.UserprofileImg.sd_setImage(with: URL(string: ReviewArray[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
        cell.selectionStyle = .none
        cell.lblUserName.text = ReviewArray[indexPath.row].name
        // cell.UserprofileImg.image = UIImage(named: ReviewImg[indexPath.row])
        cell.lblCount.text = ReviewArray[indexPath.row].ratingCount
        cell.lblReviewmsg.text = ReviewArray[indexPath.row].review
       
       // let profileType = ReviewArray[indexPath.row].profile_type
        if ProfileType == "1" {
            cell.normalUserRatingVw.isHidden = true
            cell.showRatingVw.isHidden = false
            cell.showRatingVw.backgroundColor = UIColor.clear
            cell.showRatingVw.delegate = self
            cell.showRatingVw.contentMode = UIView.ContentMode.scaleAspectFit
            cell.showRatingVw.type = .halfRatings
            
        }else {
           
            cell.normalUserRatingVw.isHidden = false
            cell.showRatingVw.isHidden = true
        }
               
               /** Note: With the exception of contentMode, type and delegate,
                all properties can be set directly in Interface Builder **/
        
       
        let starCount = ReviewArray[indexPath.row].ratingCount
        cell.showRatingVw.rating = Double(starCount) ?? 0.0
        cell.normalUserRatingVw.rating = Double(starCount) ?? 0.0
        cell.normalUserRatingVw.delegate = self
        cell.normalUserRatingVw.contentMode = UIView.ContentMode.scaleAspectFit
        cell.normalUserRatingVw.type = .halfRatings
        // let starTotal = starCount.rounded()
        //  let intValue = starData?[indexPath.row].b_id
        // let intStar = Int(starTotal)
//        if starCount >= "5"{
//            
//            //            cell.star1.isHidden = false
//            //            cell.star2.isHidden = false
//            //            cell.star3.isHidden = false
//            //            cell.star4.isHidden = false
//            //            cell.star5.isHidden = false
//            cell.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst"), for: .normal)
//        }
//        else if starCount  == "4"{
//            
//            //            cell.star1.isHidden = false
//            //            cell.star2.isHidden = false
//            //            cell.star3.isHidden = false
//            //            cell.star4.isHidden = false
//            //            cell.star5.isHidden = true
//            cell.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            
//        }
//        else if starCount == "3"{
//            //            cell.star1.isHidden = false
//            //            cell.star2.isHidden = false
//            //            cell.star3.isHidden = false
//            //            cell.star4.isHidden = true
//            //            cell.star5.isHidden = true
//            cell.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            
//        }
//        else if starCount == "2"{
//            
//            //            cell.star1.isHidden = false
//            //            cell.star2.isHidden = false
//            //            cell.star3.isHidden = true
//            //            cell.star4.isHidden = true
//            //            cell.star5.isHidden = true
//            cell.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//        }
//        else if starCount == "1"{
//            
//            //            cell.star1.isHidden = false
//            //            cell.star2.isHidden = true
//            //            cell.star3.isHidden = true
//            //            cell.star4.isHidden = true
//            //            cell.star5.isHidden = true
//            cell.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            
//        } else if starCount == "0"{
//            //            cell.star1.isHidden = true
//            //            cell.star2.isHidden = true
//            //            cell.star3.isHidden = true
//            //            cell.star4.isHidden = true
//            //            cell.star5.isHidden = true
//            cell.star1.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star2.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            cell.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
    
}

extension ReviewVC: FloatRatingViewDelegate {

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
       // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
      //  updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
         print(ratingView.rating)
    }
    
}


