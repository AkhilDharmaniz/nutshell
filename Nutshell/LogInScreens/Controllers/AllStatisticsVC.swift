//
//  AllStatisticsVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class AllStatisticsVC: UIViewController {

    @IBOutlet weak var allStatisticsTableVw: UITableView!
    var StaticsLbl = ["Link clicks","Unique clicks","Views","Daily","Weekly","Totals"]
    var HoursLbl = ["956","6798","","500","400","900"]
    var linkclicks = ""
    var uniqueclicks = ""
    var views = ""
    var daily = ""
    var weekly = ""
    var totals = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        GetAllStaticticsApi()
        allStatisticsTableVw.rowHeight = UITableView.automaticDimension
        allStatisticsTableVw.estimatedRowHeight = 100
    }
    // MARK:--- User Details
       func GetAllStaticticsApi(){
           IJProgressView.shared.showProgressView()
           let getUser = Constant.shared.baseUrl + Constant.shared.AllstatictisAPi
           let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["profile_id":idValue]
           AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
               print(response)
               IJProgressView.shared.hideProgressView()
               let result = response
               let hourscount = result.value(forKeyPath: "userprofileCount.linksclick") as? Int ?? 1
            print(hourscount,"kkkkk")
               self.linkclicks = "\(hourscount)"
               let days = result.value(forKeyPath: "userprofileCount.uniqueclicks") as? Int ?? 1
               print(days,"userrrr")
               self.uniqueclicks = "\(days)"
               let Month = result.value(forKeyPath: "userprofileCount.dailyCount") as? Int ?? 1
               self.daily = "\(Month)"
               let allcounts = result.value(forKeyPath: "userprofileCount.weeklyCount") as? Int ?? 1
            print(allcounts,"ooii")
               self.weekly = "\(allcounts)"
            print(self.weekly,"lll")
            let total = result.value(forKeyPath: "userprofileCount.totalCount") as? Int ?? 1
            self.totals = "\(total)"
        self.allStatisticsTableVw.reloadData()
           })
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
           }
       }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func subscriptionBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BoostProfileVC") as! BoostProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension AllStatisticsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = allStatisticsTableVw.dequeueReusableCell(withIdentifier: "AllStatisticsTableCell", for: indexPath) as! AllStatisticsTableCell
        cell.selectionStyle = .none
        cell.lblTopicTitle.text = StaticsLbl[indexPath.row]
        cell.lblCounts.text = HoursLbl[indexPath.row]
        if indexPath.row == 0 {
        cell.lblCounts.text = linkclicks
        }else if indexPath.row == 1 {
         cell.lblCounts.text = uniqueclicks
        }else if indexPath.row == 2 {
         cell.lblCounts.text = views
        cell.lblTopicTitle.font = UIFont.boldSystemFont(ofSize: 16.0)
        }else if indexPath.row == 3 {
         cell.lblCounts.text = weekly
        }else if indexPath.row == 4 {
        cell.lblCounts.text = daily
        }else if indexPath.row == 5 {
        cell.lblCounts.text = totals
        }
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
