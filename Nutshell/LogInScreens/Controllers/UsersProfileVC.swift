//
//  UsersProfileVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 22/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//
import UIKit
import SafariServices
class UsersProfileVC: UIViewController {
//  @IBOutlet weak var normanlUserRatingVw: FloatRatingView!
//  @IBOutlet weak var lblCountBusiness: UILabel!
//  //@IBOutlet weak var businessImage: UIImageView!
//  @IBOutlet weak var showRatingVw: FloatRatingView!
//  @IBOutlet weak var lblCount: UILabel!
//  @IBOutlet weak var star1: UIButton!
//  @IBOutlet weak var star2: UIButton!
//  @IBOutlet weak var star3: UIButton!
//  @IBOutlet weak var star4: UIButton!
//  @IBOutlet weak var star5: UIButton!
//  var Profile_type = ""
//  var fromAppDelegate: String?
//  @IBOutlet weak var userProfileCollectionVw: UICollectionView!
//var statusChange = String()
//    var LinkId = String()
//    var ratingAvg = String()
//    var ratingCounts = Int()
//    var GetLinksArray = [GetLinksModel]()
//    @IBOutlet weak var lblBio: UILabel!
//    var Notificationimg = ["insta","snapchat","twitter","facebook"]
//    var NotificationuserName = ["Instagram","Snapchat","Twitter","Facebook"]
//    var Clicks = ["0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks"]
//    var Envelope = ["FOOD ENVELOPE","SPORTS ENVELOPE","DANCE ENVELOPE"]
//    var EnvelopeProfileIMg = ["fooed-enve","sports-enve","dance-enve"]
//    var ImageCollection = ["insta","facebook","twitter","snapchat","email","linkedin","whatsapp","youtube","icon"]
//    var links = [("Instagram",#imageLiteral(resourceName: "insta"),0,false),("Facebook",#imageLiteral(resourceName: "facebook"),1,false),("Twitter",#imageLiteral(resourceName: "twitter"),2,false),("Snapchat",#imageLiteral(resourceName: "insta"),3,false),("Email",#imageLiteral(resourceName: "insta"),4,false),("Linkedin",#imageLiteral(resourceName: "insta"),5,false),("Whatsapp",#imageLiteral(resourceName: "insta"),6,false),("Youtube",#imageLiteral(resourceName: "insta"),7,false),("Envelope",#imageLiteral(resourceName: "insta"),8,true)]
//    var User_ids = String()
//    var ChangeStatus = String()
//    var message = String()
//    var EnvelopeListArray = [EnvelopebleEnaDetailModel]()
//    @IBOutlet weak var followBtn: UIButton!
//    @IBOutlet weak var envelopeDataVw: UIView!
//    @IBOutlet weak var userImages: UIImageView!
//    @IBOutlet weak var lblEnvelope: UILabel!
//    @IBOutlet weak var linksVw: UIView!
//    @IBOutlet weak var envelopeVw: UIView!
//    @IBOutlet weak var lblLinks: UILabel!
//    @IBOutlet weak var envelopeBtn: UIButton!
//    @IBOutlet weak var linksBtn: UIButton!
//    @IBOutlet weak var lblName: UILabel!
//    @IBOutlet weak var lblRole: UILabel!
//    @IBOutlet weak var lbllocation: UILabel!
//    var EnvelopeId = ""
//    var closure: (() -> Void)?
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.apiCall()
//        self.showRatingVw.isHidden = true
//        // Reset float rating view's background color
//        showRatingVw.backgroundColor = UIColor.clear
//        /** Note: With the exception of contentMode, type and delegate,
//         all properties can be set directly in Interface Builder **/
//        showRatingVw.delegate = self
//        showRatingVw.contentMode = UIView.ContentMode.scaleAspectFit
//        showRatingVw.type = .halfRatings
//    }
//    func apiCall(){
//        getData()
//        self.closure = {
//            self.GetLinksApi()
//        }
//        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        self.getData()
//    }
//    @objc func methodOfReceivedNotification(notification: Notification) {
//        self.User_ids = notification.object as? String ?? ""
//        self.apiCall()
//    }
//    @IBAction func addRatingBtnClicked(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
//        vc.OtherUserId = User_ids
//        vc.ProfileType = Profile_type
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//    func FollowApi() {
//        IJProgressView.shared.showProgressView()
//        let getUser = Constant.shared.baseUrl + Constant.shared.followApi
//        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
//        let parms:[String:Any] = ["user_id":idValue,"followUserID":User_ids]
//        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
//            print(response,"AddFollow")
//            self.message = response["message"] as? String ?? ""
//            print(self.message)
//            let status = response["status"] as? Int ?? 0
//            print(status)
//            if status == 1{
//                self.getData()
//            }else {
//                self.getData()
//            }
//            //            DispatchQueue.main.async {
//            //                let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
//            //
//            //                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//            //                    UIAlertAction in
//            //                    self.navigationController?.popViewController(animated: true)
//            //                }
//            //                alertController.addAction(okAction)
//            //                self.present(alertController, animated: true, completion: nil)
//            //            }
//
//        }) { (Error) in
//            IJProgressView.shared.hideProgressView()
//            print(Error)
//        }
//    }
//    func GetLinksApi() {
//        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
//        IJProgressView.shared.showProgressView()
//        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetLInk
//        let parms : [String:Any] = ["user_id":User_ids]
//        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
//            print(response,"GetLinks")
//            IJProgressView.shared.hideProgressView()
//            let status = response["status"] as? Int ?? 0
//            print(status)
//            if status == 1{
//                if let details = response["data"] as? [[String:Any]]{
//                    print(details,"lll")
//                    self.GetLinksArray.removeAll()
//                    for i in 0..<details.count {
//                        self.GetLinksArray.append(GetLinksModel(url: details[i]["url"] as? String ?? "", count: details[i]["count"] as? Int ?? 0, id: details[i]["id"] as? String ?? "", link_id: details[i]["link_id"] as? String ?? ""))
//                    }
//                }
//            }else{
//                self.GetLinksArray.removeAll()
//            }
//            self.userProfileCollectionVw.reloadData()
//        })
//        { (error) in
//            IJProgressView.shared.hideProgressView()
//            print(error)
//        }
//    }
//
//    func AddLinkCount(LinkId:String,clicksbyId:String,Link_number:String){
//        IJProgressView.shared.showProgressView()
//        let getUser = Constant.shared.baseUrl + Constant.shared.AddLinkCount
//        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
//        let parms:[String:Any] = ["link_id":Link_number,"user_id":idValue,"clickby":clicksbyId,"link_number":LinkId]
//        print(parms)
//        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
//            print(response,"Links")
//            IJProgressView.shared.hideProgressView()
//            let result = response
//            let status = result["status"] as! Int ?? 0
//            print(status)
//            self.message = result["message"] as? String ?? ""
//            print(self.message)
//            if status == 1{
//            }else {
//            }
//        })
//        { (error) in
//            IJProgressView.shared.hideProgressView()
//            print(error)
//        }
//    }
//    func GetSelectedEnvelope() {
//        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
//        IJProgressView.shared.showProgressView()
//        let CategoryData = Constant.shared.baseUrl + Constant.shared.EnvelopeEnableDetailsApi
//        let parms : [String:Any] = ["user_id":User_ids]
//        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
//            print(response,"GetEnvelope")
//            IJProgressView.shared.hideProgressView()
//            let status = response["status"] as? Int ?? 0
//            print(status)
//            if status == 1{
//                if let details = response["envelope_details"] as? [[String:Any]]{
//                    print(details,"lll")
//                    self.EnvelopeListArray.removeAll()
//
//                    for i in 0..<details.count {
//                        self.EnvelopeListArray.append(EnvelopebleEnaDetailModel(id: details[i]["id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", link: details[i]["link"] as? String ?? "", description: details[i]["description"] as? String ?? "", enable: details[i]["enable"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? ""))
//                    }
//                }
//            }else{
//                self.EnvelopeListArray.removeAll()
//            }
//            self.userProfileCollectionVw.reloadData()
//        })
//        { (error) in
//            IJProgressView.shared.hideProgressView()
//            print(error)
//        }
//    }
//
//    // MARK:--- User Details
//    func getData(){
//        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""; IJProgressView.shared.showProgressView()
//        let getUser = Constant.shared.baseUrl + Constant.shared.OtherUserApi
//        let parms:[String:Any] = ["user_id":user_ID,"followUserID":User_ids]
//        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
//            print(response,"GetProfiles")
//            IJProgressView.shared.hideProgressView()
//            let result = response
//            self.closure?()
//            let userName = result.value(forKeyPath: "user_data.name") as? String ?? "user"
//            let useremail = result.value(forKeyPath: "user_data.email") as? String ?? "user@gmail.com"
//            let profilePic = result.value(forKeyPath: "user_data.photo") as? String ?? ""
//            let desrp = result.value(forKeyPath: "user_data.description") as? String ?? ""
//            print(desrp)
//            UserDefaults.standard.set(profilePic, forKey: "userProfilePic")
//            let UserRole = result.value(forKeyPath: "user_data.role") as? String ?? ""
//            print(UserRole,"Roleeeee")
//            let ProfileType = result.value(forKeyPath: "user_data.profile_type") as? String ?? ""
//            print(ProfileType,"Typee")
//            self.Profile_type = ProfileType
//            if ProfileType == "1"{
////                self.normanlUserRatingVw.isHidden = false
//////                self.businessImage.image = UIImage(named: "F-Favrt")
//////                self.businessImage.isHidden = false
////               // self.lblCountBusiness.isHidden = false
////               // self.lblCount.isHidden = true
//                self.showRatingVw.isHidden = true
//                self.normanlUserRatingVw.isHidden = true
//                self.lblCount.isHidden = true
//            }
//            else {
//               // self.lblCount.isHidden = false
//                self.showRatingVw.isHidden = false
//                self.normanlUserRatingVw.isHidden = true
//               // self.lblCountBusiness.isHidden = true
//            }
//            let UserFollowStatus = result.value(forKeyPath: "user_data.changeStatus") as? String ?? ""
//            self.statusChange = UserFollowStatus
//            print(UserFollowStatus,"changeStatus")
//            if self.statusChange == "0" {
//                let title = "Follow"
//                self.followBtn.setTitle(title, for: .normal)
//            }else if self.statusChange == "2" {
//                let title = "Following"
//                self.followBtn.setTitle(title, for: .normal)
//            }else if self.statusChange == "1" {
//                let title = "Requested"
//                self.followBtn.setTitle(title, for: .normal)
//            }
//            let ratingAvg = result.value(forKeyPath: "user_data.ratingavg") as? String ?? ""
//            print(ratingAvg,"Avg")
//            self.ratingAvg = ratingAvg
//            self.showRatingVw.rating = Double(ratingAvg) ?? 0.0
//            self.normanlUserRatingVw.rating = Double(ratingAvg) ?? 0.0
//            let ratingCount = result.value(forKeyPath: "user_data.ratingcount") as? Int ?? 0
//            self.ratingCounts = ratingCount
//            print(ratingCount,"Count")
//
//            self.lblCount.text = self.ratingCounts == 0 ? "" : "\(self.ratingCounts)"
//           // self.lblCountBusiness.text = self.ratingCounts == 0 ? "" : "\(self.ratingCounts)"
//            //        if self.ratingAvg >= 5{
//            //            self.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst"), for: .normal)
//            //        }
//            //        else if self.ratingAvg  == 4{
//            //            self.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            //        }
//            //        else if self.ratingAvg == 3{
//            //            self.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            //        }
//            //        else if self.ratingAvg == 2{
//            //            self.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            //        }
//            //        else if self.ratingAvg == 1{
//            //            self.star1.setImage(UIImage(named: "Rst"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            //        } else if self.ratingAvg == 0{
//            //            self.star1.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star2.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star3.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star4.setImage(UIImage(named: "Rst1"), for: .normal)
//            //            self.star5.setImage(UIImage(named: "Rst1"), for: .normal)
//            //        }
//            print(userName,useremail,profilePic,desrp)
//            let passwords = UserDefaults.standard.value(forKey: "oldPassword") as? String ?? "1"
//            self.lblName.text = userName
//            self.lblRole.text = useremail
//            self.lbllocation.text = UserRole
//            self.lblBio.text = desrp
//            self.userImages.sd_setImage(with: URL(string: profilePic), placeholderImage: UIImage(named: "img1"))
//        })
//        { (error) in
//            IJProgressView.shared.hideProgressView()
//            print(error)
//        }
//    }
//    @IBAction func backBtnClicked(_ sender: UIButton) {
//        if fromAppDelegate == "YES"{
//            print("iiiii")
//            self.navigationController?.popViewController(animated: true)
//            let storyB = UIStoryboard(name: "Main", bundle: nil)
//            //  tabBarController?.tabBar.isHidden = false
//            let SWRC = storyB.instantiateViewController(withIdentifier: "Tab") as? TabBarClass
//            //  app.check = false;
//            if let SWRC = SWRC {
//                navigationController?.pushViewController(SWRC, animated: false)
//            }
//        }else{
//        self.navigationController?.popViewController(animated: true)
//        }
//    }
//    @IBAction func followBtnClicked(_ sender: UIButton) {
//        self.FollowApi()
//    }
//    @IBAction func linksBtnClicked(_ sender: UIButton) {
//        self.envelopeDataVw.isHidden = true
//        self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
//        self.lblLinks.textColor = .init(cgColor: #colorLiteral(red: 0.1456587911, green: 0.4564293027, blue: 0.4587423205, alpha: 1))
//        self.linksVw.backgroundColor = .init(cgColor: #colorLiteral(red: 0.1456587911, green: 0.4564293027, blue: 0.4587423205, alpha: 1))
//        self.envelopeVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
//    }
//    @IBAction func envelopeBtnClicked(_ sender: UIButton) {
//        self.envelopeDataVw.isHidden = false
//        self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.1456587911, green: 0.4564293027, blue: 0.4587423205, alpha: 1))
//        self.lblLinks.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
//        self.envelopeVw.backgroundColor = .init(cgColor: #colorLiteral(red: 0.1456587911, green: 0.4564293027, blue: 0.4587423205, alpha: 1))
//        self.linksVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
//    }
//}
//extension UsersProfileVC:UICollectionViewDelegate,UICollectionViewDataSource{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        return GetLinksArray.count+1
//    }
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersProfileCollectionVwCell", for: indexPath) as! UsersProfileCollectionVwCell
//        let totalRows = collectionView.numberOfItems(inSection: indexPath.section)
//        if indexPath.item == totalRows-1{
//            cell.userImg.image = #imageLiteral(resourceName: "Envelope")
//            cell.lblUser.text = "Envelope"
//        }else{
//            if GetLinksArray[indexPath.row].id == "1"{
//                cell.userImg.image = #imageLiteral(resourceName: "phone-pay")
//                cell.lblUser.text = "PhonePe"
//            }else if GetLinksArray[indexPath.row].id == "2"{
//                cell.userImg.image = #imageLiteral(resourceName: "rapido")
//                cell.lblUser.text = "Rapido"
//            }else if GetLinksArray[indexPath.row].id == "3"{
//                cell.userImg.image = #imageLiteral(resourceName: "map")
//                cell.lblUser.text = "Google"
//            }else if GetLinksArray[indexPath.row].id == "4"{
//                cell.userImg.image = #imageLiteral(resourceName: "skype")
//                cell.lblUser.text = "Skype"
//            }else if GetLinksArray[indexPath.row].id == "5"{
//                cell.userImg.image = #imageLiteral(resourceName: "google-pay")
//                cell.lblUser.text = "GooglePay"
//            }else if GetLinksArray[indexPath.row].id == "6"{
//                cell.userImg.image = #imageLiteral(resourceName: "insta")
//                cell.lblUser.text = "Instagram"
//            }else if GetLinksArray[indexPath.row].id == "7"{
//                cell.userImg.image = #imageLiteral(resourceName: "tiktok")
//                cell.lblUser.text = "Tiktok"
//            }else if GetLinksArray[indexPath.row].id == "8"{
//                cell.userImg.image = #imageLiteral(resourceName: "snack-video")
//                cell.lblUser.text = "snackVideo"
//            }
//        }
//        return cell
//    }
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let totalRows = collectionView.numberOfItems(inSection: indexPath.section)
//        if indexPath.item == totalRows-1{
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnvelopeVC") as! EnvelopeVC
//            vc.commingFromProfile = true
//            vc.Header = "Envelope Details"
//            vc.OtherUSerID = User_ids
//            self.navigationController?.pushViewController(vc, animated: true)
//        }else{
//            let links = GetLinksArray[indexPath.row].url
//            let LinkNumber = GetLinksArray[indexPath.row].link_id
//            if let url = URL(string:links)
//            {
//                let safariCC = SFSafariViewController(url: url)
//                present(safariCC, animated: true, completion: nil)
//            }
//            let linkId = GetLinksArray[indexPath.row].id
//            AddLinkCount(LinkId:linkId,clicksbyId:User_ids, Link_number: LinkNumber)
//        }
//    }
//}
//extension UsersProfileVC: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let bounds = collectionView.bounds
//        return CGSize(width: bounds.width/4.6, height: 138)
//    }
//}
//extension UsersProfileVC: FloatRatingViewDelegate {
//    // MARK: FloatRatingViewDelegate
//    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
//        // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
//        print(ratingView.rating)
//    }
//    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
//        //  updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
//        print(ratingView.rating)
//    }
}

