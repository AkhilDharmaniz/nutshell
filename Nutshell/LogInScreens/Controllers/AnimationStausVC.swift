//
//  AnimationStausVC.swift
//  InstagramStatusDemo
//
//  Created by Vivek Dharmani on 28/05/21.
//

import UIKit
import Gemini
import IQKeyboardManagerSwift
import IBAnimatable

class AnimationStausVC: UIViewController, UITextViewDelegate {
    @IBOutlet weak var commentBoxVw: UIView!
    var responseArray =  [[String:AnyObject]]()
    @IBOutlet weak var messageBottomVwConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageVwHeightCOnstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTvHeightConstrit: NSLayoutConstraint!
    @IBOutlet weak var messageTV: AnimatableTextView!
    var otherUserId = ""
    var onceOnly = false
    var StatusArray = [StatusModel]()
    var statusArrayOne = [StatusModel]()
    var statusArrayTwo = [StatusModel]()
    var currentPage = Int()
    let textViewMaxHeight: CGFloat = 120
    var message = String()
    var LoginUserName = ""
    var LoginUserImage = ""
    var Status_id = ""
    var message_Type = "1"
    lazy var globalRoomId = String()
    lazy var chatUserId = String()
    lazy var chatUsername = String()
    let photos = ["1","2","3","4","5","6"]
    let userPics = ["6","5","4","3","2","1"]
    let userName = ["Natural star Nani","Mahesh babu","Nithiin reddy","Pawan kalyan","Arjun Reddy","Ram Po"]
    let userTime = ["1 hr","2 hr","3 hr","4 hr","5 hr","6 hr"]
    var viewstatusID = String()
    @IBOutlet weak var geminiCollectionVw: GeminiCollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Looks for single or multiple taps.
            let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
           //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
           tap.cancelsTouchesInView = false
           view.addGestureRecognizer(tap)
       let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
       swipeRight.direction = .right
       self.view.addGestureRecognizer(swipeRight)
       let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
       swipeDown.direction = .down
       self.view.addGestureRecognizer(swipeDown)
        let swipeup = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .up
        self.view.addGestureRecognizer(swipeup)
        messageTV.backgroundColor = UIColor.clear
        getRoomIdApi(chatUserId:otherUserId)
        statusArrayOne = StatusArray
        ViewStatusApis()
     geminiCollectionVw.gemini
            .cubeAnimation().cubeDegree(90)
        print(currentPage,"oooo")
        DispatchQueue.main.async {
            for _ in 0..<self.currentPage{
                self.geminiCollectionVw.scrollToNextItem(animated: false)
            }
        }
        messageTV.delegate = self
        messageTV.textContainerInset = UIEdgeInsets(top: 12, left: 13, bottom: 15, right: 13)
        let taps: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(taps)
    let userid = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        if userid == otherUserId {
            self.commentBoxVw.isHidden = true
        }else {
        self.commentBoxVw.isHidden = false
        }
     //   self.geminiCollectionVw.isPagingEnabled = true
    }
   
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
            case .left:
                print("Swiped left")
            case .up:
                // Show keyboard by default
                  messageTV.becomeFirstResponder()
                print("Swiped up")
            default:
                break
            }
        }
    }
    @objc func didPressOnDoneButton() {
          messageTV.resignFirstResponder()
        self.geminiCollectionVw.alpha = 1
       }
    public func textViewDidBeginEditing(_ textView: UITextView) {
          let invocation = IQInvocation(self, #selector(didPressOnDoneButton))
          textView.keyboardToolbar.doneBarButton.invocation = invocation
        
       }
    func textViewDidChange(_ textView: UITextView) {
       
             // get the current height of your text from the content size
             var height = textView.contentSize.height
             // clamp your height to desired values
             if height > 90 {
                 height = 90
             } else if height < 50 {
                 height = 45
             }
             // update the constraint
             messageTvHeightConstrit.constant = height
             messageVwHeightCOnstraint.constant = height + 50
      //  self.geminiCollectionVw.alpha = 0.5
             // get the current height of your t
             self.view.layoutIfNeeded()
         }
    
    //Calls this function when the tap is recognized.
   
//    @objc  override func dismissKeyboard() {
//
//    view.endEditing(false)
//          }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(AnimationStausVC.self)
//        IQKeyboardManager.shared.disabledToolbarClasses = [AnimationStausVC.self]
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
//    }
//    @objc func handleKeyboardNotification(_ notification: Notification) {
//              if let userInfo = notification.userInfo {
//                  let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
//
//                  let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
//                  messageBottomVwConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
//                  UIView.animate(withDuration: 0.1, animations: { () -> Void in
//                      self.view.layoutIfNeeded()
//                  })
//              }
//          }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatFieldVC.self)
           IQKeyboardManager.shared.disabledToolbarClasses = [ChatFieldVC.self]
           NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
           
           NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
           if let userInfo = notification.userInfo {
               let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
               
               let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
               
            messageBottomVwConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
               
               DispatchQueue.main.async(execute: {

               })
               UIView.animate(withDuration: 0.1, animations: { () -> Void in
//                   self.view.layoutIfNeeded()

               })
           }
       }

    @objc override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        print("Tapppp")
        self.geminiCollectionVw.alpha = 1
        view.endEditing(true)
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.geminiCollectionVw.alpha = 0.5
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
       
        if textView.text.count == 0{
           // self.geminiCollectionVw.alpha = 0.5
                 // get the current height of your t
            messageTvHeightConstrit.constant = 45
            messageVwHeightCOnstraint.constant = 80
            
            //    messagePlaceholderLbl.isHidden = false
        }else{
            
            //  messagePlaceholderLbl.isHidden = true
        }
        
        return true
    }
    
    func addMessageApi(){
              let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
              let url = Constant.shared.baseUrl + Constant.shared.addMessageApi
              var params = [String:Any]()
              
        params = ["room_id":globalRoomId,"sender_id":userid,"message":messageTV.text!,"receiver_id" :otherUserId,"message_type":"1","status_id":Status_id]
               AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
              AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
                   AFWrapperClass.svprogressHudDismiss(view: self)
                  let result = dict as AnyObject
                  print(result)
                  let msg = result["message"] as? String ?? ""
                  let status = result["status"] as? Int ?? 0
                  if status == 1{
                    self.geminiCollectionVw.alpha = 1
                    let appendData = result["data"] as? [String:Any] ?? [:]
                    SocketManger.shared.socket.emit("newMessage",self.globalRoomId,appendData)
                    self.responseArray.append(appendData as [String : AnyObject])
                    self.messageTV.resignFirstResponder()
                    self.messageTV.text = ""
                    self.messageTvHeightConstrit.constant = 45
                    //  self.messagePlaceholderLbl.isHidden = false
                    self.messageVwHeightCOnstraint.constant = 80
//                    DispatchQueue.main.async {
//                        let alertController = UIAlertController(title: "Nutshell", message: "Comment sent successfully.", preferredStyle: .alert)
//                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
//                            UIAlertAction in
//                        }
//                        alertController.addAction(okAction)
//                        self.present(alertController, animated: true, completion: nil)
//                    }
                    
                  }else {
                    print("lll")
                }
              }) { (error) in
                  alert("Nutshell", message: error.localizedDescription, view: self)
               AFWrapperClass.svprogressHudDismiss(view: self)
                  //  AFWrapperClass.svprogressHudDismiss(view: self)
              }
          }
    
    
    open func getRoomIdApi(chatUserId:String){
           let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let chatData = Constant.shared.baseUrl + Constant.shared.requestChat
           let params:[String:Any] = [
               "user_id":idValue,
               "owner_id":chatUserId
           ]
          // IJProgressView.shared.showProgressView()
           AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
               print(response)
            //   IJProgressView.shared.hideProgressView()
               let status = response["status"] as! Int
               print(status)
               let message = response["message"] as? String ?? ""
               if status == 1{
                   let resultDict = response as? [String:AnyObject] ?? [:]
                   self.globalRoomId = resultDict["room_id"] as? String ?? ""
                print(self.globalRoomId,"ggggg")
                   print("$$$$$$$$\(resultDict)")
                self.chatUserId = resultDict["user_id"] as? String ?? ""
                print(chatUserId,"iiiiidddd")
                self.chatUsername = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                print(self.chatUsername,"llll")
                self.LoginUserName = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                print(self.LoginUserName,"llll")
                self.LoginUserImage = response.value(forKeyPath: "user_detail.photo") as? String ?? "user"
                print(self.LoginUserImage,"llll")
//                let CMDVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC
//                   CMDVC?.roomId = self.globalRoomId
//                   CMDVC?.senderId = self.OtherUserId
//                   CMDVC?.receiverName = self.chatUsername
//                   CMDVC?.Message_Type = self.message_Type
//                CMDVC?.LoginUserName = self.LoginUserName
//                CMDVC?.LoginUserImage = self.LoginUserImage
//                   print(self.chatUsername)
////                   CMDVC?.LoginUserName = self.ChatLoginUser
////                   CMDVC?.LoginUserImage = self.ChatLoginUserimage
//                   if let CMDVC = CMDVC {
//                self.navigationController?.pushViewController(CMDVC, animated: true)
//                   }
               }else{
            alert("Nutshell", message: self.message, view: self)
               }
           })
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
               alert("Nutshell", message: error.localizedDescription, view: self)
           }
       }
    
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        let trimmedString = messageTV.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if trimmedString == ""{
        }else{
            self.geminiCollectionVw.alpha = 1
            addMessageApi()
            messageTV.text = ""
            messageTV.resignFirstResponder()
//            self.messageTvHeightConstrit.constant = 45
//            //  self.messagePlaceholderLbl.isHidden = false
//            self.messageVwHeightCOnstraint.constant = 80
        }
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        let indexPath = IndexPath(item: currentPage, section: 0)
//        self.geminiCollectionVw.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
//        self.geminiCollectionVw.reloadData()
//    }
 
//    @IBAction func backBtnClicked(_ sender: UIButton) {
//
//    }
    
    func dateDiff(dateStr:String) -> String {
    let f:DateFormatter = DateFormatter()
    // f.timeZone = NSTimeZone.local
    f.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    let now = f.string(from: NSDate() as Date)


    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    print(toLongDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", fromdate: dateStr))
    let showDate = inputFormatter.date(from: toLongDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", fromdate: dateStr)!)
    //#colorLiteral(red: 0.1846026778, green: 0.1725854874, blue: 0.1682911813, alpha: 1)
    let date1:Date = showDate!
    let date2: Date = Date()
    let calendar: Calendar = Calendar.current

    let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1, to: date2)



    let weeks = Int(components.month!)
    let days = Int(components.day!)
    let hours = Int(components.hour!)
    let min = Int(components.minute!)
    let sec = Int(components.second!)
    let year = Int(components.year!)


    var timeAgo = ""
    if sec == 0 {
    timeAgo = "just now"
    }else if (sec > 0){
    if (sec >= 2) {
    timeAgo = "\(sec) secs ago"
    } else {
    timeAgo = "\(sec) sec ago"
    }
    }

    if (min > 0){
    if (min >= 2) {
    timeAgo = "\(min) mins ago"
    } else {
    timeAgo = "\(min) min ago"
    }
    }

    if(hours > 0){
    if (hours >= 2) {
    timeAgo = "\(hours) hrs ago"
    } else {
    timeAgo = "\(hours) hr ago"
    }
    }

    if (days > 0) {
    if (days >= 2) {
    timeAgo = "\(days) days ago"
    } else {
    timeAgo = "\(days) day ago"
    }
    }

    if(weeks > 0){
    if (weeks >= 2) {
    timeAgo = "\(weeks) months ago"
    } else {
    timeAgo = "\(weeks) month ago"
    }
    }

    if(year > 0){
    if (year >= 2) {
    timeAgo = "\(year) years ago"
    } else {
    timeAgo = "\(year) year ago"
    }
    }


    // print("timeAgo is===> \(timeAgo)")
    return timeAgo;
    }
    
    func formattedDateFromString(fromFormat:String,dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = fromFormat
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.locale = Locale(identifier: "en")
            //outputFormatter.timeZone = TimeZone(identifier: "UTC")
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    func toLongDate(withFormat format: String = "yyyy-MM-dd",fromdate:String)-> String?{
        let date = Date(timeIntervalSince1970: fromdate.doubleValue)
        let date1 = formattedDateFromString(fromFormat: "yyyy-MM-dd HH:mm:ssZ", dateString: "\(date)", withFormat: format)
        return date1
    }
    
    
}
extension AnimationStausVC:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return statusArrayOne.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = geminiCollectionVw.dequeueReusableCell(withReuseIdentifier: "AnimationCollectionCell", for: indexPath) as! AnimationCollectionCell
       // cell.setcell(imageName: StatusArray[indexPath.row].status_image)
        cell.userImg.sd_setImage(with: URL(string: statusArrayOne[indexPath.row].status_image), placeholderImage: UIImage(named: "placeholderF"))
       // cell.userImgProfile.image = UIImage(named: userPics[indexPath.row])
        cell.userImgProfile.sd_setImage(with: URL(string: statusArrayOne[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
        self.otherUserId = StatusArray[indexPath.row].user_id
        self.Status_id = StatusArray[indexPath.row].status_id
        cell.backBtn = {
        self.navigationController?.popViewController(animated: true)
        }
        cell.ProfileBtn = {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            vc.User_ids = self.statusArrayOne[indexPath.row].user_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.userNameLbl.text = statusArrayOne[indexPath.row].name
      //  cell.userTimeLbl.text = StatusArray[indexPath.row].created_at
        let unixtimeInterval = statusArrayOne[indexPath.row].created_at
        print(dateDiff(dateStr: "\(statusArrayOne[indexPath.row].statusdate)"))
        print(unixtimeInterval,"Timeee")
        let date = Date(timeIntervalSince1970:  unixtimeInterval.doubleValue)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "IST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        print(strDate,"date")
        cell.userTimeLbl.text = dateDiff(dateStr: "\(statusArrayOne[indexPath.row].statusdate)")
        self.geminiCollectionVw.animateCell(cell)
        self.viewstatusID = statusArrayOne[indexPath.row].status_id
        return cell
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       self.geminiCollectionVw.animateVisibleCells()
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        ViewStatusApis()
        getRoomIdApi(chatUserId:otherUserId)
    }
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if let cell = cell as? AnimationCollectionCell {
//            self.geminiCollectionVw.animateCell(cell)
//        }
//
//    }
    func scrollToItem(at indexPath: IndexPath,
          at scrollPosition: UICollectionView.ScrollPosition,
          animated: Bool){
    }
    func ViewStatusApis() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
       // IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.ViewStatusApi
        let parms : [String:Any] = ["user_id":user_ID,"status_id":viewstatusID]
        print(parms)
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
           // IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            if status == 1{
            }else{
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
  

//     internal func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//       if !onceOnly {
//         let indexToScrollTo = IndexPath(item: currentPage, section: 0)
//        self.geminiCollectionVw.scrollToItem(at: indexToScrollTo, at: .top, animated: true)
//         onceOnly = true
//       }
//     }
    
}
extension AnimationStausVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.geminiCollectionVw.frame.size.width)
        return .init(width: width, height: geminiCollectionVw.frame.size.height)

    }
}
extension UICollectionView {
    func scrollToNextItem(animated : Bool) {
        let contentOffset = CGFloat(floor(self.contentOffset.x + self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset, animated: animated)
    }
    
    func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(self.contentOffset.x - self.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset, animated: true)
    }
    
    func moveToFrame(contentOffset : CGFloat, animated : Bool) {
        self.setContentOffset(CGPoint(x: contentOffset, y: self.contentOffset.y), animated: animated)
    }
}
