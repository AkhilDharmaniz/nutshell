//
//  ChangePasswordVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController,UITextFieldDelegate {
    
    var movetoroot = Bool()
    var message = String()
    @IBOutlet weak var oneSelectVw: UIView!
    @IBOutlet weak var threeSelectVw: UIView!
    @IBOutlet weak var twoSelectVw: UIView!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtConfirmPassword.delegate = self
        txtNewPassword.delegate = self
        txtOldPassword.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtOldPassword:
            self.oneSelectVw.backgroundColor = Constant.shared.appcolor
        case txtNewPassword:
            self.twoSelectVw.backgroundColor = Constant.shared.appcolor
          case txtConfirmPassword:
            self.threeSelectVw.backgroundColor = Constant.shared.appcolor
       
        default:break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtOldPassword:
        self.oneSelectVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtNewPassword:
        self.twoSelectVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtConfirmPassword:
        self.threeSelectVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
       
        default:break
        }
    }
    
    
    
    func ChangePassword(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.ChangePasswordApi
        let idValue = UserDefaults.standard.value(forKey: "Uid")
             let parms:[String:Any] = ["user_id":idValue as Any,"old_password":txtOldPassword.text!,"new_password":txtNewPassword.text!]
                AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
                    print(response)
                 UserDefaults.standard.set(self.txtNewPassword.text, forKey: "oldPassword")
                    IJProgressView.shared.hideProgressView()
                    let result = response
                  let status = result["status"] as! Int
                   print(status)
                   self.message = result["message"] as? String ?? ""
                   print(self.message)
                 
                 if status == 1{
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                 }else {
                     alert("Nutshell", message: self.message, view: self)
                 }
                })
                { (error) in
                    IJProgressView.shared.hideProgressView()
                    print(error)
                }
            }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if movetoroot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }    }

    @IBAction func submitBtnCLicked(_ sender: UIButton) {
        if (txtOldPassword.text?.isEmpty)!{
            ValidateData(strMessage: "Please enter your old password")
        }
        else if (txtNewPassword.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter new password")
        }else if (txtConfirmPassword.text?.isEmpty)!{
            
            ValidateData(strMessage: " Please enter confirm password")
        }else if (txtNewPassword!.text!.count) < 4 || (txtNewPassword!.text!.count) > 15{
            ValidateData(strMessage: "Please enter minimum 4 digit password")
        }
        else
            if(txtNewPassword.text != txtConfirmPassword.text) {
                ValidateData(strMessage: "Sorry, your new password and confirm password is not matching")
            }
            else{
                self.ChangePassword()
        }
    }
}

