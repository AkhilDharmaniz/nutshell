//
//  EditProfileVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 06/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    //MARK:-- ImagePicker
    var imagePicker = UIImagePickerController()
    var imgData = [UIImage]()
    var movetoroot = Bool()
    var message = String()
    var closure: (() -> Void)?
    @IBOutlet weak var roleVw: UIView!
    @IBOutlet weak var emailVw: UIView!
    @IBOutlet weak var userVw: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var ProfileImg: UIImageView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblUsername: UITextField!
    @IBOutlet weak var txtVwBio: UITextView!
    @IBOutlet weak var txtRole: UITextField!
    var SName = ""
    var suserName = ""
    var SEmail = ""
    var S_ProfileImg = ""
    var S_Role = ""
    var descriptionTxt = ""
    var UserRole = ""
    var locationName = ""
    
    override func viewDidLoad() {
    super.viewDidLoad()
    txtVwBio.delegate = self
    txtName.delegate = self
    txtEmail.delegate = self
    txtRole.delegate = self
    imagePicker.delegate = self
    self.txtName.text = SName
    self.txtEmail.text = SEmail
    self.lblUsername.text = suserName
    self.ProfileImg.sd_setImage(with: URL(string: S_ProfileImg), placeholderImage: UIImage(named: "img1"))
    self.txtVwBio.text = descriptionTxt
    self.txtRole.text = locationName
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
      
    navigationController?.popViewController(animated: true)
    
    }
  //  name,email,role,photo,description,user_id
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
                 switch textField {
                 case txtName:
                    self.userVw.backgroundColor = Constant.shared.appcolor
                 case txtEmail:
                    self.emailVw.backgroundColor = Constant.shared.appcolor
                   case txtRole:
                    self.roleVw.backgroundColor = Constant.shared.appcolor
                
                 default:break
                 }
             }
             func textFieldDidEndEditing(_ textField: UITextField) {
                 switch textField {
                 case txtName:
                 self.userVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                 case txtEmail:
                 self.emailVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                 case txtRole:
                 self.roleVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
                
                 default:break
                 }
             }
    func textViewDidBeginEditing(_ textView: UITextView) {
           switch textView {
                case txtVwBio:
                    self.txtVwBio.borderColor = Constant.shared.appcolor
           
            default:break
            }
       }
       
       func textViewDidEndEditing(_ textView: UITextView) {
           switch textView {
           case txtVwBio:
               self.txtVwBio.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
           default:break
           }
       }
    func editProfile(){
          IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.EditProfileApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let img = ProfileImg.image
        let imageString = img?.jpegData(compressionQuality: 0.3)?.base64EncodedString() ?? ""
        let parms:[String:Any] = ["username":lblUsername.text ?? "","name":txtName.text ?? "","email":txtEmail.text ?? "","photo":imageString ,"description":txtVwBio.text ?? "","user_id":idValue,"role":"","location":txtRole.text ?? ""]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
        print(response)
        let status = response["status"] as? Int ?? 22
        self.message = response["message"] as? String ?? ""
            print(status,"LOL")
            if status == 1 {
            self.closure?()
            self.navigationController?.popViewController(animated: true)
            }else {
        alert("Nutshell", message: self.message, view: self)
            }
       
        IJProgressView.shared.hideProgressView()
          }) { (Error) in
          }
      }
    @IBAction func imagePickerBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Nutshell", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
                
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        ProfileImg.image  = tempImage
        imgData.append(tempImage)
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func saveBtnClicked(_ sender: UIButton) {
        if (lblUsername.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter username")
        } else if (txtRole.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter Location")
        } else if (txtVwBio.text?.isEmpty)!{
                ValidateData(strMessage: " Please enter Bio")
               }
        else {
            DispatchQueue.main.async {
                self.editProfile()
            }
        }
    }
}
