//
//  MapViewVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 16/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//


import UIKit
import GoogleMaps
import GooglePlaces

class MapViewVC: UIViewController,GMSMapViewDelegate {
    var isupdate:Bool = false
    //Mark ;-- map
    @IBOutlet weak var MapVw: GMSMapView!
    var locationManager = CLLocationManager()
    var lattiude : Double = 0.0
    var longitude : Double = 0.0
    var currentLattitude = NSString()
    var currentLongitude = NSString()
    var updateLocation = CLLocationCoordinate2D()
    var UserId = String()
    var UserDetails:NearByUser?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        self.MapVw.delegate = self
        self.setUpLocationPermissions()
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -  Determine My Current Location
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.desiredAccuracy = CLLocationAccuracy(kCLDistanceFilterNone)
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        let locationObj = locationManager.location
        let coordLat = locationObj?.coordinate.latitude
        let coordLong = locationObj?.coordinate.longitude
        self.lattiude = coordLat ?? 0.00
        self.longitude = coordLong ?? 0.00
        currentLattitude = NSString(format: "%.6f", coordLat ?? 0.00)
        // let userlatt = ["currentLattitude"] as? String ?? ""
        UserDefaults.standard.set(currentLattitude, forKey: "UpdateLatitude")
        //   print(userlatt)
        print(currentLattitude)
        currentLongitude = NSString(format: "%.6f", coordLong ?? 0.00)
        UserDefaults.standard.set(self.currentLongitude, forKey: "UpdateLongitude")
        print(currentLongitude)
        print("BEGIN:Sazile======> Current lattitude : \(currentLattitude)")
        //let email = ["Current lattitude"] as? String ?? ""
        print("BEGIN:Sazile======> Current longitude : \(currentLongitude)")
        print("BEGIN:Sazile======> Current lattitude : \(updateLocation.latitude)")
        print("BEGIN:Sazile======> Current longitude : \(updateLocation.longitude)")
        let location = CLLocationCoordinate2D(latitude:CLLocationDegrees(coordLat ?? 0.00), longitude:CLLocationDegrees(coordLong ?? 0.00))
        print("location: \(location)")
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(coordLat ?? 0.00), longitude: CLLocationDegrees(coordLong ?? 0.00), zoom: Float(16))
        MapVw.animate(to: camera)
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: coordLat ?? 0.0, longitude: coordLong ?? 0.0)
        //marker.icon = #imageLiteral(resourceName: "phone-pay")
        MapVw.padding = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 0)
        self.MapVw.delegate = self
        //        DispatchQueue.main.async {
        //
        //        }
          //getData()
    }
    func drawImageWithProfilePic(urlString:String, image: UIImage) -> UIImageView {
        let imgView = UIImageView(image: image)
        imgView.frame = CGRect(x: 0, y: 0, width: 90, height: 90)
        let picImgView = UIImageView()
        picImgView.sd_setImage(with:URL(string: urlString))
        picImgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        imgView.addSubview(picImgView)
        picImgView.center.x = imgView.center.x
        picImgView.center.y = imgView.center.y-10
        picImgView.layer.cornerRadius = picImgView.frame.width/2
        picImgView.clipsToBounds = true
        imgView.setNeedsLayout()
        picImgView.setNeedsLayout()
        //        let newImage = imageWithView(view: imgView)
        //        return newImage
        return imgView
    }
    //MARK: - GOOGLEMAPS PERMISSIONS
    func setUpLocationPermissions() {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        if CLLocationManager.locationServicesEnabled() {
            switch (CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                
                print("BEGIN:Sazile ======> No location service access")
                requestWhenInUseAuthorization()
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("BEGIN:Sazile ======> Location service access")
                
                determineMyCurrentLocation()
                
            @unknown default:
                
                fatalError()
            }
        } else {
            
            print("BEGIN:Sazile ======> Location services are not enabled")
            requestWhenInUseAuthorization()
        }
    }
    //MARK: -  RequestWhenInUseAuthorization
    func requestWhenInUseAuthorization() {
        let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        if status == .denied {
            var title: String
            title = (status == .denied) ? "Location Services Off" : ""
            let message = "Turn on Location Services in Settings > Privacy to allow Maps to determine your current location"
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: nil))
            alertController.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (action:UIAlertAction) in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("BEGIN:Sazile ======> Settings opened: \(success)")
                    })
                }
            }))
            self.present(alertController, animated: true, completion: nil)
        } else if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    // MARK:--- User Details
    func getData(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.NearByUser
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            do {
                let data = try JSONSerialization.data(withJSONObject: response, options: .prettyPrinted)
                let user = try JSONDecoder().decode(NearByUser.self, from: data)
                self.UserDetails = user
                print(user,"lopp")
                var indx = 0
                for x in self.UserDetails?.data ?? [] {
                    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                    imageView.backgroundColor = .clear
                    imageView.cornerView()
                    imageView.border(width: 2.0, color: .white)
                    imageView.sd_setImage(with: URL(string:x.photo.replacingOccurrences(of: "", with: "%20") ), placeholderImage:UIImage(named: "img1"))
                    let opponentMarker = GMSMarker()
                    print(self.lattiude,"oo")
                    print(self.longitude,"111")
                    opponentMarker.position = CLLocationCoordinate2DMake(Double(x.latitude) ?? 0.0, Double(x.longitude) ?? 0.0)
                    opponentMarker.appearAnimation = .none
                    opponentMarker.iconView = imageView
                    opponentMarker.map = self.MapVw
                    opponentMarker.title = x.name
                    opponentMarker.userData =  x
                    indx += 1
                    opponentMarker.zIndex = Int32(indx)
                    //                   opponentMarker.userData = self.UserDetails?.data?[x] ?? nil
                }
            } catch {
                print(error)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension MapViewVC : CLLocationManagerDelegate{
    //MARK: - CLLOCATIONMANAGER DELEGATE
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isupdate == false {
            setUpLocationPermissions()
            requestWhenInUseAuthorization()
            self.isupdate = true
        }else{
        }
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if let object = marker.userData as? NearUserDetails  {
            self.UserId = object.user_id
        }
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
        vc.User_ids = self.UserId
        self.navigationController?.pushViewController(vc, animated: true)
        return true
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        alert("Nutshell", message: "Network issue", view: self)
    }
}
//extension GMSMapView {
//    func mapStyle(withFilename name: String, andType type: String) {
//        do {
//            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
//                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
//            } else {
//                NSLog("Unable to find style.json")
//            }
//        } catch {
//            NSLog("One or more of the map styles failed to load. \(error)")
//        }
//    }
//}
