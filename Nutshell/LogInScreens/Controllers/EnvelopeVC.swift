//
//  EnvelopeVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 31/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import SafariServices

class EnvelopeVC: UIViewController {
    
    @IBOutlet weak var addMoreBtn: UIButton!
    @IBOutlet weak var staticticsBtn: UIButton!
    var Header = ""
    // var EnvelopeId = ""
    var OtherUSerID = ""
    var envelopeId = String()
    var LinkId = String()
    @IBOutlet weak var noDataFound: UIView!
    var message = String()
    var enableIDStatus = String()
    var commingFromProfile:Bool?
    @IBOutlet weak var staticStatusVw: UIView!
    @IBOutlet weak var privateorpublicBtn: UISwitch!
    @IBOutlet weak var hideVw: UIView!
    var EnvelopeArray = [EnvelopeDetailModel]()
    @IBOutlet weak var lblHeader: UILabel!
    var Title = ["LOREAM IPSUM","LOREAM IPSUM","LOREAM IPSUM"]
    var Url = ["https://youtube/z-EJXPQGgBi","https://youtube/z-EJXPQGgBi","https://youtube/z-EJXPQGgBi"]
    var Description = ["Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. the passage is attributed to ab unknow typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.","Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. the passage is attributed to ab unknow typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.","Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. the passage is attributed to ab unknow typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book."]
    @IBOutlet weak var envelopeTableVw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noDataFound.isHidden = true
        staticStatusVw.layer.cornerRadius = 30
        staticStatusVw.clipsToBounds = true
        staticStatusVw.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        envelopeTableVw.rowHeight = UITableView.automaticDimension
        envelopeTableVw.estimatedRowHeight = 100
        self.staticStatusVw.isHidden = true
        self.hideVw.isHidden = true
        var tapGesture = UITapGestureRecognizer()
        // self.hideKeyboardTappedAround()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(tap:)))
        tapGesture.numberOfTapsRequired = 1
        hideVw.addGestureRecognizer(tapGesture)
        if commingFromProfile == true {
        self.staticticsBtn.isHidden = true
        self.addMoreBtn.isHidden = true
        } else {
        self.staticticsBtn.isHidden = false
        self.addMoreBtn.isHidden = false
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        GetEnvelopeApi()
//        if commingFromProfile == true {
//            GetSelectedEnvelope()
//        }else {
//            GetEnvelopeApi()
//        }
    }
    @objc func tapAction(tap:UITapGestureRecognizer){
        hideVw.isHidden = true
        staticStatusVw.isHidden = true
    }
    @IBAction func totalStaticBtn(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsVC") as! StatisticsVC
        vc.commingFromEnvelope = true
        vc.EnvelopeIDD = envelopeId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func addMoreLinkBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEnvelopeVC") as! AddEnvelopeVC
        vc.EnvelopeIds = envelopeId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func statisticsBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsVC") as! StatisticsVC
        vc.LinkID = envelopeId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func privateOrPublicBtnClicked(_ sender: UISwitch) {
        if (sender.isOn == true) {
            EnvelopeEnable(status:"1",EnvelopeId:envelopeId)
            //PrivacyAccount(status:"1")
        }else {
            EnvelopeEnable(status:"0",EnvelopeId:envelopeId)
            // PrivacyAccount(status:"0")
        }
    }
    func GetEnvelopeApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetEnvelopeApi
        let parms : [String:Any] = ["user_id":user_ID,"envelope_id":envelopeId,"userprofileid":OtherUSerID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            //            let Counts = response["count"] as? Int ?? 0
            //            print(Counts,"lll")
            //  let message = response["message"] as? String ?? ""
            
            if status == 1{
                self.noDataFound.isHidden = true
                if let details = response["envelope_details"] as? [[String:Any]]{
                    print(details,"lll")
                    self.EnvelopeArray.removeAll()
                    for i in 0..<details.count {
                        self.EnvelopeArray.append(EnvelopeDetailModel(id: details[i]["id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", link: details[i]["link"] as? String ?? "", description: details[i]["description"] as? String ?? "", enable: details[i]["enable"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? "", envelopeid: details[i]["envelopeid"] as? String ?? ""))
                    }
                }
            }else{
                self.noDataFound.isHidden = false
                self.EnvelopeArray.removeAll()
            }
            self.envelopeTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
//    func GetSelectedEnvelope() {
//        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
//        IJProgressView.shared.showProgressView()
//        let CategoryData = Constant.shared.baseUrl + Constant.shared.EnvelopeEnableDetailsApi
//        let parms : [String:Any] = ["user_id":OtherUSerID]
//        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
//            print(response)
//            IJProgressView.shared.hideProgressView()
//            let status = response["status"] as? Int ?? 0
//            print(status)
//            if status == 1{
//
//                if let details = response["envelope_details"] as? [[String:Any]]{
//                    print(details,"lll")
//                    self.EnvelopeArray.removeAll()
//
//                    for i in 0..<details.count {
//                        self.EnvelopeArray.append(EnvelopeDetailModel(id: details[i]["id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", link: details[i]["link"] as? String ?? "", description: details[i]["description"] as? String ?? "", enable: details[i]["enable"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? ""))
//                    }
//                }
//            }else{
//                self.noDataFound.isHidden = false
//                self.EnvelopeArray.removeAll()
//            }
//            self.envelopeTableVw.reloadData()
//        })
//        { (error) in
//            IJProgressView.shared.hideProgressView()
//            print(error)
//        }
//    }
    func EnvelopeEnable(status:String,EnvelopeId:String){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.enableDisableApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"disable":status,"id":LinkId]
        print(parms)
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let result = response
            let status = result["status"] as! Int ?? 0
            print(status)
            self.message = result["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.GetEnvelopeApi()
//                        if self.commingFromProfile == true {
//                            self.GetSelectedEnvelope()
//                        }else {
//                            self.GetEnvelopeApi()
//                        }
                        //  self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                // alert("Nutshell", message: self.message, view: self)
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func EnvelopeClicksCount(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.EnvelopeClicksCount
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["link_id":LinkId,"user_id":idValue,"clickble_id":OtherUSerID]
        print(parms)
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let result = response
            let status = result["status"] as! Int ?? 0
            print(status)
            self.message = result["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                // alert("Nutshell", message: self.message, view: self)
            }else {
                //  alert("Nutshell", message: self.message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}

extension EnvelopeVC: UITableViewDelegate,UITableViewDataSource {
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
return EnvelopeArray.count
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
let cell = envelopeTableVw.dequeueReusableCell(withIdentifier: "EnvelopeTableVwCell", for: indexPath) as! EnvelopeTableVwCell
        // cell.notificationImg.sd_setImage(with: URL(string: NotificationListArray[indexPath.row].image), placeholderImage: UIImage(named: "img1"))
if commingFromProfile == true {
cell.threedotsImg.isHidden = true
cell.threeDotsBtn.isHidden = true
cell.switchBtn.isHidden = true
} else {
cell.threeDotsBtn.isHidden = false
cell.threedotsImg.isHidden = false
cell.switchBtn.isHidden = false
}
        cell.selectionStyle = .none
        cell.lblTitle.text = EnvelopeArray[indexPath.row].name
        cell.lblUrl.text = EnvelopeArray[indexPath.row].link
        cell.lblDescription.text = EnvelopeArray[indexPath.row].description
        let enableStatus = EnvelopeArray[indexPath.row].enable
        print(enableStatus,"STATUS")
        if enableStatus == "0" {
            print("true")
        }else {
            print("false")
        }
        cell.DotBTN = {
            let envelopeid = self.EnvelopeArray[indexPath.row].id
            self.LinkId = envelopeid
            self.staticStatusVw.isHidden = false
            self.hideVw.isHidden = false
            if self.EnvelopeArray[indexPath.row].enable == "0"{
                self.privateorpublicBtn.isOn = false
            }else{
                self.privateorpublicBtn.isOn = true
            }
        }
        let enableID = self.EnvelopeArray[indexPath.row].enable
        self.enableIDStatus = enableID
        print(enableID,"IIIII")        
        if enableIDStatus == "0"{
            cell.onOffSiwtch.isOn = false
        }else{
            cell.onOffSiwtch.isOn = true
        }
        cell.LInkBTN = {
            let envelopeid = self.EnvelopeArray[indexPath.row].envelopeid
            self.LinkId = envelopeid
            let links = self.EnvelopeArray[indexPath.row].link
            if let url = URL(string:links)
            {
                if self.commingFromProfile == true {
                  self.EnvelopeClicksCount()
                }else {
                   print("STop")
                }
                let safariCC = SFSafariViewController(url: url)
                self.present(safariCC, animated: true, completion: nil)
            }
        }
        //        cell.switchAction = { (bool) in
        //            if bool {
        //                print("switch on state")
        //                //call your switch on api here
        //
        //
        //                self.EnvelopeEnable(status:"0", EnvelopeId: envelopeid)
        //            }else {
        //                print("switch off state")
        //                // call your switch off api here
        //                let envelopeid = self.EnvelopeArray[indexPath.row].id
        //                self.EnvelopeEnable(status:"1", EnvelopeId: envelopeid)
        //            }
        //        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // EnvelopeClicksCount()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
