//
//  OtherUserProfilesVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit
import SDWebImage
import SafariServices

class OtherUserProfilesVC: UIViewController {
    
    @IBOutlet weak var privateVw: UIView!
    @IBOutlet weak var ratingVwHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var followersBtn: UIButton!
    @IBOutlet weak var followingBtn: UIButton!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var stackHeight: NSLayoutConstraint!
    @IBOutlet weak var ratingBtn: UIButton!
    var accountStatus = ""
    var followersCount = ""
    var followingCount = ""
    var fromAppDelegate: String?
    var ChangeStatus = String()
    var statusChange = String()
    var ratingAvg = String()
    var ratingCounts = Int()
    var Profile_type = ""
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var folowBtn: UIButton!
    @IBOutlet weak var businessRatingVw: FloatRatingView!
    @IBOutlet weak var LblNoData: UILabel!
    var User_ids = String()
  //  @IBOutlet weak var boostProfileBtn: UIButton!
    var linkarray = [String]()
   // @IBOutlet weak var noDataVw: UIView!
    @IBOutlet weak var linkNoDataFound: UIView!
    @IBOutlet weak var lblBio: UILabel!
   // @IBOutlet weak var linkaddBtn: UIButton!
    @IBOutlet weak var lblNameOnProfileTab: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var UserImage: UIImageView!
  //  @IBOutlet weak var envelopeAddBtn: UIButton!
    @IBOutlet weak var envelopeVw: UIView!
    @IBOutlet weak var messageBtn: UIButton!
    
    var phonepe = ""
    var rapido = ""
    var skype = ""
    var google = ""
    var googlepay = ""
    var instagram = ""
    var tiktok = ""
    var snackVideo = ""
    var message = String()
    var EnvelopeSelectedArray = [EnvelopebleEnaDetailModel]()
    var GetLinksArray = [GetLinksModel]()
    var Name = String()
    var idArrays = [String]()
    var MainArrys = [String]()
    var ProfileImage = String()
    var Header = ""
    var SName = ""
    var SEmail = ""
    var S_ProfileImg = ""
    var S_Role = ""
    var descriptionTxt = ""
    var UserRole = ""
    var LinkIds = String()
    var User_id = ""
    var Notificationimg = ["insta","snapchat","twitter","facebook"]
    var NotificationuserName = ["Instagram","Snapchat","Twitter","Facebook"]
    // var Clicks = ["0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks"]
    var Envelope = ["FOOD ENVELOPE","SPORTS ENVELOPE","DANCE ENVELOPE"]
    var EnvelopeProfileIMg = ["fooed-enve","sports-enve","dance-enve","dance-enve"]
    var result = [String:Any]()
    @IBOutlet weak var linkTableVw: UITableView!
   // @IBOutlet weak var addLinksHideVw: UIView!
   // @IBOutlet weak var addLinksVw: UIView!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblEnvelope: UILabel!
    @IBOutlet weak var linkSelectedVw: UIView!
    @IBOutlet weak var envelopeselectedVw: UIView!
    @IBOutlet weak var envelopeTableVw: UITableView!
    @IBOutlet weak var linkBtn: UIButton!
    @IBOutlet weak var enveplopeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.privateVw.isHidden = true
        UserDefaults.standard.set("111", forKey: "ENO")
        self.businessRatingVw.isHidden = true
        // Reset float rating view's background color
        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        businessRatingVw.delegate = self
        businessRatingVw.contentMode = UIView.ContentMode.scaleAspectFit
        businessRatingVw.type = .halfRatings
        businessRatingVw.backgroundColor = UIColor.clear
        linkTableVw.rowHeight = UITableView.automaticDimension
        linkTableVw.estimatedRowHeight = 100
        envelopeTableVw.rowHeight = UITableView.automaticDimension
        envelopeTableVw.estimatedRowHeight = 75
    }
    
    override func viewWillAppear(_ animated: Bool) {
       // self.GetSelectedEnvelope()
        self.getData()
        let Check = UserDefaults.standard.value(forKey: "ENO") as? String ?? ""
        if Check == "222" {
            self.GetSelectedEnvelope()
            self.envelopeVw.isHidden = false
            self.lblEnvelope.textColor = Constant.shared.appcolor
            self.lblLink.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
            self.envelopeselectedVw.backgroundColor = Constant.shared.appcolor
            self.linkSelectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        }else {
            self.GetLinksApi()
            self.envelopeVw.isHidden = true
            self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
            self.lblLink.textColor = Constant.shared.appcolor
            self.linkSelectedVw.backgroundColor = Constant.shared.appcolor
            self.envelopeselectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func addRatingBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        vc.OtherUserId = User_ids
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func FollowApi() {
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.followApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"followUserID":User_ids]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response,"AddFollow")
            self.message = response["message"] as? String ?? ""
            print(self.message)
            let status = response["status"] as? Int ?? 0
            print(status)
            if status == 1{
                self.getData()
            }else {
                self.getData()
            }
            
        }) { (Error) in
            IJProgressView.shared.hideProgressView()
            print(Error)
        }
    }
    
    // MARK:--- User Details
    func getData(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""; IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.OtherUserApi
        let parms:[String:Any] = ["user_id":user_ID,"followUserID":User_ids]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response,"GetProfiles")
            IJProgressView.shared.hideProgressView()
            self.lblFollowers.text =  response["Followers"] as? String ?? ""
            self.followersCount = response["Followers"] as? String ?? ""
            self.lblFollowing.text = response["Followings"] as? String ?? ""
            self.followingCount = response["Followings"] as? String ?? ""
            if let userData = response["user_data"] as? [String:Any]{
                self.result = userData
    //            self.lblNameOnProfileTab.text = self.result["name"] as? String ?? ""
                self.lblNameOnProfileTab.text = self.result["username"] as? String ?? ""
                UserDefaults.standard.set(self.result["photo"] as? String ?? "", forKey: "userProfilePic")
                self.lblBio.text = self.result["description"] as? String ?? ""
                self.lblRole.text = self.result["location"] as? String ?? ""
                self.accountStatus = self.result["status"] as? String ?? ""
                if  self.accountStatus == "0" {
                    self.privateVw.isHidden = true
                    self.followingBtn.isUserInteractionEnabled = true
                    self.followersBtn.isUserInteractionEnabled = true
                }else {
                    self.privateVw.isHidden = false
                }
                self.Profile_type = self.result["profile_type"] as? String ?? ""
                if self.Profile_type == "1"{
                     self.businessRatingVw.isHidden = false
                }
                else {
                    self.lblCount.isHidden = true
                    self.ratingBtn.isUserInteractionEnabled = false
                    self.ratingVwHeightConstraint.constant = 0
                }
                self.statusChange = self.result["changeStatus"] as? String ?? ""
                if self.statusChange == "0" {
                    let title = "Follow"
                    if self.accountStatus == "0" {
                        self.followingBtn.isUserInteractionEnabled = true
                        self.followersBtn.isUserInteractionEnabled = true
                    }else {
                        self.followingBtn.isUserInteractionEnabled = false
                        self.followersBtn.isUserInteractionEnabled = false
                    }
    //
                    self.folowBtn.setTitle(title, for: .normal)
                }else if self.statusChange == "2" {
                    let title = "Following"
                    self.privateVw.isHidden = true
                    self.folowBtn.setTitle(title, for: .normal)
                    self.followingBtn.isUserInteractionEnabled = true
                    self.followersBtn.isUserInteractionEnabled = true
                }else if self.statusChange == "1" {
                    let title = "Requested"
                    self.followingBtn.isUserInteractionEnabled = false
                    self.followersBtn.isUserInteractionEnabled = false
                    self.folowBtn.setTitle(title, for: .normal)
                }
                self.ratingAvg = self.result["ratingavg"] as? String ?? ""
                self.businessRatingVw.rating = Double(self.ratingAvg) ?? 0.0
                self.ratingCounts = self.result["ratingcount"] as? Int ?? 0
                self.lblCount.text = self.ratingCounts == 0 ? "" : "\(self.ratingCounts)"
                self.UserImage.sd_setImage(with: URL(string: self.result["photo"] as? String ?? ""), placeholderImage: UIImage(named: "img1"))
                if let chatStatus = self.result["chatStatus"] as? Int{
                    self.messageBtn.isHidden = chatStatus == 1 ? false : true
                }
            }
            
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
  
    func GetSelectedEnvelope() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.EnvelopeEnableDetailsApi
        let parms : [String:Any] = ["user_id":user_ID,"otheruserid":User_ids]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
           // self.GetLinksApi()
            //            let Counts = response["count"] as? Int ?? 0
            //            print(Counts,"lll")
            //  let message = response["message"] as? String ?? ""
            if status == 1{
              //  self.noDataVw.isHidden = true
                self.linkNoDataFound.isHidden = true
                if let details = response["envelope_details"] as? [[String:Any]]{
                    print(details,"lll")
                    self.EnvelopeSelectedArray.removeAll()
                    for i in 0..<details.count {
                        self.EnvelopeSelectedArray.append(EnvelopebleEnaDetailModel(id: details[i]["id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", link: details[i]["link"] as? String ?? "", description: details[i]["description"] as? String ?? "", enable: details[i]["enable"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? "", expirylink: details[i]["expirylink"] as? String ?? ""))
                    }
                }
            }else{
                self.EnvelopeSelectedArray.removeAll()
                self.linkNoDataFound.isHidden = false
                self.LblNoData.text = "No Envelopes Found"
               // self.noDataVw.isHidden = false
            }
            self.envelopeTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func GetLinksApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetLinksListing
        let parms : [String:Any] = ["user_id":User_ids]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            
            //            let Counts = response["count"] as? Int ?? 0
            //            print(Counts,"lll")
            //  let message = response["message"] as? String ?? ""
            if status == 1{
                self.linkNoDataFound.isHidden = true
                
             //   self.noDataVw.isHidden = true
            
                if let details = response["data"] as? [[String:Any]]{
                    print(details,"lll")
                    self.GetLinksArray.removeAll()
                   
                    for i in 0..<details.count {
                        self.GetLinksArray.append(GetLinksModel(social_id: details[i]["social_id"] as? String ?? "", title: details[i]["title"] as? String ?? "", imageurl: details[i]["imageurl"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", sort_id: details[i]["sort_id"] as? String ?? "", linkurl: details[i]["linkurl"] as? String ?? "", create_date: details[i]["create_date"] as? String ?? "", linkcount: details[i]["linkcount"] as? String ?? "", link_id: details[i]["link_id"] as? String ?? ""))
                    }
                   
                }
            }else{
                self.linkNoDataFound.isHidden = false
                self.LblNoData.text = "No Links Found"
                self.GetLinksArray.removeAll()
            }
            self.linkTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    @IBAction func followBtnClicked(_ sender: UIButton) {
        self.FollowApi()
    }
    @IBAction func linksBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set("111", forKey: "ENO")
        self.GetLinksApi()
        self.envelopeVw.isHidden = true
        self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        self.lblLink.textColor = Constant.shared.appcolor
        self.linkSelectedVw.backgroundColor = Constant.shared.appcolor
        self.envelopeselectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    
    @IBAction func followingBtnClicked(_ sender: UIButton) {
        if followingCount == "0" {
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendsListVC") as! FriendsListVC
            vc.followType = "2"
            vc.OtherProfileId = User_ids
            vc.navTitle = "Following"
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
        
    @IBAction func folowersBtnClicked(_ sender: UIButton) {
        if followersCount == "0" {
            
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendsListVC") as! FriendsListVC
            vc.followType = "1"
            vc.OtherProfileId = User_ids
            vc.navTitle = "Followers"
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
    @IBAction func envelopeBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set("222", forKey: "ENO")
        self.GetSelectedEnvelope()
        self.envelopeVw.isHidden = false
        self.lblEnvelope.textColor = Constant.shared.appcolor
        self.lblLink.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        self.envelopeselectedVw.backgroundColor = Constant.shared.appcolor
        self.linkSelectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if fromAppDelegate == "YES"{
            print("iiiii")
            self.navigationController?.popViewController(animated: true)
            let storyB = UIStoryboard(name: "Main", bundle: nil)
            //  tabBarController?.tabBar.isHidden = false
            let SWRC = storyB.instantiateViewController(withIdentifier: "Tab") as? TabBarClass
            //  app.check = false;
            if let SWRC = SWRC {
                navigationController?.pushViewController(SWRC, animated: false)
            }
        }else {
            AFWrapperClass.redirectToTabNavRVC(currentVC: self)
        }
    }
    
    func AddLinkCount(LinkId:String,clicksbyId:String,Link_number:String){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.AddLinkCount
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["link_id":LinkId,"user_id":idValue,"clickby":clicksbyId,"link_number":Link_number]
        print(parms)
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response,"Links")
            IJProgressView.shared.hideProgressView()
            let result = response
            let status = result["status"] as! Int ?? 0
            print(status)
            self.message = result["message"] as? String ?? ""
            print(self.message)
            if status == 1{
            }else {
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    @IBAction func messageBtnAction(_ sender: UIButton) {
        getRoomIdApi(chatUserId: result["user_id"] as? String ?? "")
    }
    open func getRoomIdApi(chatUserId:String){
           let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let chatData = Constant.shared.baseUrl + Constant.shared.requestChat
           let params:[String:Any] = [
               "user_id":idValue,
               "owner_id":chatUserId
           ]
           IJProgressView.shared.showProgressView()
           AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
               print(response)
               IJProgressView.shared.hideProgressView()
               let status = response["status"] as! Int
               let message = response["message"] as? String ?? ""
            if status == 1{
                let chatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                chatVC.roomId = response["room_id"] as? String ?? ""
                chatVC.LoginUserImage = response["loginProfile"] as? String ?? ""
                if let userDetail = response["user_detail"] as? [String:Any]{
                    chatVC.senderId = userDetail["user_id"] as? String ?? ""
                    chatVC.receiverName = userDetail["username"] as? String ?? ""
//                    chatVC.LoginUserName = self.ChatLoginUser
//                    chatVC.LoginUserImage = self.ChatLoginUserimage
                }
                self.navigationController?.pushViewController(chatVC, animated: true)
                
                
                
                
                //                print(self.globalRoomId,"ggggg")
                //                   print("$$$$$$$$\(resultDict)")
                //                self.chatUserId = resultDict["user_id"] as? String ?? ""
                //                print(chatUserId,"iiiiidddd")
                //                self.chatUsername = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                //                print(self.chatUsername,"llll")
                //                self.LoginUserName = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                //                print(self.LoginUserName,"llll")
                //                self.LoginUserImage = response.value(forKeyPath: "user_detail.photo") as? String ?? "user"
                //                print(self.LoginUserImage,"llll")
            }else{
            alert(Constant.shared.AppName, message: self.message, view: self)
               }
           })
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
            alert(Constant.shared.AppName, message: error.localizedDescription, view: self)
           }
       }
}
extension OtherUserProfilesVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == linkTableVw {
            return GetLinksArray.count
        } else {
            print(EnvelopeSelectedArray.count,"ee")
            return EnvelopeSelectedArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == linkTableVw {
            if let cell2 = linkTableVw.dequeueReusableCell(withIdentifier: "OtherUserLinkTableCell", for: indexPath) as? OtherUserLinkTableCell {
                if let spacer = tableView.reorder.spacerCell(for: indexPath) {
                    return spacer
                }
                cell2.lblLinkProfileName.text = GetLinksArray[indexPath.row].title
              //  cell2.linkProfileImg.image = UIImage(named: "map")
                cell2.linkProfileImg.sd_setImage(with: URL(string: GetLinksArray[indexPath.row].imageurl), placeholderImage: UIImage(named: ""))
                let img = GetLinksArray[indexPath.row].imageurl
               
                if img == "" {
                    cell2.lblFirstLetter.isHidden = false
                }else {
                    cell2.lblFirstLetter.isHidden = true
                }
                var firstLetter = self.GetLinksArray[indexPath.row].title.first?.description ?? ""
                print(firstLetter,"Data")
                cell2.lblFirstLetter.text = firstLetter
                cell2.lblFirstLetter.backgroundColor = UIColor.random
                cell2.lblLinkClicks.text = GetLinksArray[indexPath.row].linkcount
                // cell2.lblReason.text = (cancelArr[indexPath.row] as! String)
                cell2.selectionStyle = .none
                // cell2.lblLinkProfileName.text = GetLinksArray[indexPath.row].url
                
//                let Id = GetLinksArray[indexPath.row].id
//                let COunts = GetLinksArray[indexPath.row].count
//                print(COunts,"ppppppp")
//                print(Id,"ooo")
//                self.LinkIds = Id
//                if LinkIds == "1" {
//                    print("Phonepe")
//                    cell2.linkProfileImg.image = UIImage(named: "phone-pay")
//                    cell2.lblLinkProfileName.text = "PhonePe"
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.phonepe = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "2" {
//                    print("rapidoDetails")
//                    cell2.lblLinkProfileName.text = "Rapido"
//                    cell2.linkProfileImg.image = UIImage(named: "rapido")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.rapido = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "3" {
//                    print("googleDetails")
//                    cell2.lblLinkProfileName.text = "Google"
//                    cell2.linkProfileImg.image = UIImage(named: "map")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.google = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "4" {
//                    print("skypeDetails")
//                    cell2.lblLinkProfileName.text = "Skype"
//                    cell2.linkProfileImg.image = UIImage(named: "skype")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.skype = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "5" {
//                    print("googlepayCountDetails")
//                    cell2.lblLinkProfileName.text = "Google Pay"
//                    cell2.linkProfileImg.image = UIImage(named: "google-pay")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.googlepay = GetLinksArray[indexPath.row].url
//
//                }else if LinkIds == "6" {
//                    print("instagramCountDetails")
//                    cell2.lblLinkProfileName.text = "Instagram TV"
//                    cell2.linkProfileImg.image = UIImage(named: "instagram-tv")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.instagram = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "7"{
//                    print("tiktokCountDetails")
//                    cell2.lblLinkProfileName.text = "Tiktok"
//                    cell2.linkProfileImg.image = UIImage(named: "tiktok")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.tiktok = GetLinksArray[indexPath.row].url
//                }else if LinkIds == "8" {
//                    print("snackvideoCountDetails")
//                    cell2.lblLinkProfileName.text = "snack video"
//                    cell2.linkProfileImg.image = UIImage(named: "snack-video")
//                    cell2.lblLinkClicks.text = "\(COunts) Clicks"
//                    self.snackVideo = GetLinksArray[indexPath.row].url
//                }
                //cell2.linkProfileImg.image = UIImage(named: Notificationimg[indexPath.row])
                //cell2.lblLinkClicks.text = GetLinksArray[indexPath.row].url
                return cell2
            }
        } else {
            let cell = envelopeTableVw.dequeueReusableCell(withIdentifier: "OtherUserEnvelopeTableCell", for: indexPath) as! OtherUserEnvelopeTableCell
            cell.selectionStyle = .none
            cell.lblEnvelopeTitle.text = EnvelopeSelectedArray[indexPath.row].name
            var firstLetter = self.EnvelopeSelectedArray[indexPath.row].name.first?.description ?? ""
            print(firstLetter,"Data")
            cell.LblCornerTitle.text = firstLetter
            cell.LblCornerTitle.backgroundColor = UIColor.random
            //  cell.envelopeImg.image = UIImage(named: EnvelopeProfileIMg[indexPath.row] )
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == linkTableVw {
            let links = GetLinksArray[indexPath.row].linkurl
            if links.contains("https") {
                if let url = URL(string:links)
                {
                    let safariCC = SFSafariViewController(url: url)
                    present(safariCC, animated: true, completion: nil)
                }
            }else {
                let links = GetLinksArray[indexPath.row].linkurl
                let wtsap = "https://api.whatsapp.com/send?phone="
        UIApplication.shared.openURL(URL(string:wtsap + links)!)
            }
            let linkId = GetLinksArray[indexPath.row].social_id
            AddLinkCount(LinkId:linkId,clicksbyId:User_ids, Link_number: "")
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileClicksVC") as! ProfileClicksVC
            //            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnvelopeVC") as! EnvelopeVC
            vc.commingFromProfile = true
            vc.Header = "Envelope Details"
            vc.OtherUSerID = User_ids
            vc.envelopeId = EnvelopeSelectedArray[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == linkTableVw {
            return UITableView.automaticDimension
        }else {
            return UITableView.automaticDimension
            // return 70
        }
    }
}

extension OtherUserProfilesVC: FloatRatingViewDelegate {
    // MARK: FloatRatingViewDelegate
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        //  updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
}
