//
//  ExploreVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 21/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class ExploreVC: UIViewController,UISearchBarDelegate {
    @IBOutlet weak var noDataFound: UIView!
    var Notificationimg = ["user-2","user-1","P3","user-3"]
    var HomeuserName = ["Jiapel Ma","Jorde Guevara","Bryanna R","Nick eischens"]
    var HomeMessage = ["CEO/President","Chief Marketing Officer","Chief Operating Officer","Chief Product Development Officer"]
    var searchArray = [SearchDetailsModel]()
    @IBOutlet weak var searchBr: UISearchBar!
    @IBOutlet weak var exploreTableVw: UITableView!
    var User_ids = String()
    var message = String()
    var ratingAvarge = Int()
    var ratingCount = Int()
    var ChnageStaus = String()
    override func viewDidLoad() {
    super.viewDidLoad()
    self.noDataFound.isHidden = true
       // send(searchText:"")
        exploreUsers(searchText: "")
        self.hideKeyboardTappedAround()
        searchBr.delegate = self
        exploreTableVw.rowHeight = UITableView.automaticDimension
        exploreTableVw.estimatedRowHeight = 100
        searchBr.layer.borderWidth = 5
        searchBr.layer.borderColor = UIColor.white.cgColor
        let textFieldInsideUISearchBar = searchBr.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = UIColor.black
        textFieldInsideUISearchBar?.font = textFieldInsideUISearchBar?.font?.withSize(16)
        searchBr.compatibleSearchTextField.textColor = UIColor.black
        searchBr.compatibleSearchTextField.backgroundColor = UIColor.init(cgColor: #colorLiteral(red: 0.9370767474, green: 0.9373400807, blue: 0.9412035346, alpha: 1))
        searchBr.compatibleSearchTextField.font = UIFont(name: "Poppins-Medium", size: 16.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        exploreUsers(searchText: "")
        searchBr.text = ""
    }
    @IBAction func nearbyUserBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewVC") as! MapViewVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
  
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBr.resignFirstResponder()
        searchBr.text = ""
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBr.resignFirstResponder()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBr.resignFirstResponder()
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        guard let firstSubview = searchBr.subviews.first else { return }
        firstSubview.subviews.forEach {
            ($0 as? UITextField)?.clearButtonMode = .never
        }
    }
    
    // MARK : -- SearchBar Function
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
           if searchText.isEmpty {
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                   self.exploreUsers(searchText: searchText)
                   //   self.productList(searchText: "")
                   self.searchBr.resignFirstResponder()
               }
           }
           if searchText.count != 0{
               //  isAccSearch = true
               exploreUsers(searchText: searchText)
               let filtered = searchArray.filter {
                   return $0.name.range(of: searchText, options: .caseInsensitive) != nil
               }
               DispatchQueue.main.async {
                   self.exploreTableVw.reloadData()
               }
           }
       }
   
    // MARK:--- User Details
       func exploreUsers(searchText: String){
           IJProgressView.shared.showProgressView()
           let getUser = Constant.shared.baseUrl + Constant.shared.SearchApi
           let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"search":searchText,"lastUserId":"","perPage":""]
           AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
               IJProgressView.shared.hideProgressView()
               let result = response
            print(response,"ppp")
               self.message = result["message"] as? String ?? ""
               let status = result["status"] as? Int ?? 0
               if status == 1{
                self.noDataFound.isHidden = true
                   if let details = response["data"] as? [[String:Any]]{
                       print(details)
                       self.searchArray.removeAll()
                       for i in 0..<details.count {
                           self.searchArray.removeAll()
                           for s in 0..<details.count{
                            self.searchArray.append(SearchDetailsModel(user_id: details[s]["user_id"] as? String ?? "", name: details[s]["name"] as? String ?? "", email: details[s]["email"] as? String ?? "", role: details[s]["role"] as? String ?? "", password: details[s]["password"] as? String ?? "", photo: details[s]["photo"] as? String ?? "", country: details[s]["country"] as? String ?? "", countryCode: details[s]["countryCode"] as? String ?? "", twitterId: details[s]["twitterId"] as? String ?? "", latitude: details[s]["latitude"] as? String ?? "", longitude: details[s]["longitude"] as? String ?? "", description: details[s]["description"] as? String ?? "", verified: details[s]["verified"] as? String ?? "", totalFollowing: details[s]["totalFollowing"] as? String ?? "", totalFollowers: details[s]["totalFollowers"] as? String ?? "", verificateCode: details[s]["verificateCode"] as? String ?? "", created_at: details[s]["created_at"] as? String ?? "", disabled: details[s]["disabled"] as? String ?? "", allowPush: details[s]["allowPush"] as? String ?? "", device_type: details[s]["device_type"] as? String ?? "", device_token: details[s]["device_token"] as? String ?? "", status: details[s]["status"] as? String ?? "", changeStatus: details[s]["changeStatus"] as? String ?? "", ratingavg: details[i]["ratingavg"] as? Int ?? 0, ratingcount: details[i]["ratingcount"] as? Int ?? 0, sponsored_key: details[s]["sponsored_key"] as? String ?? "", username: details[s]["username"] as? String ?? ""))
                           }
                       }
                   }
                    self.exploreTableVw.reloadData()
               }else {
                self.noDataFound.isHidden = false
                 self.searchArray.removeAll()
               }
               self.exploreTableVw.reloadData()
           })
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
           }
       }
}
extension ExploreVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = exploreTableVw.dequeueReusableCell(withIdentifier: "ExploreTableVwCell", for: indexPath) as! ExploreTableVwCell
        cell.userProfile.sd_setImage(with: URL(string: searchArray[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
       
        cell.selectionStyle = .none
        cell.nameLbl.text = searchArray[indexPath.row].name
        cell.LblUserName.text = "@\(searchArray[indexPath.row].username)"
        //  cell.userProfile.image = UIImage(named: Notificationimg[indexPath.row])
//        if searchArray[indexPath.row].role == "" {
//            cell.lblDescriptions.text = "  "
//        }else {
//        cell.lblDescriptions.text = searchArray[indexPath.row].role
//        }
       
        let sponseredUser = searchArray[indexPath.row].sponsored_key
        print(sponseredUser,"lopop")
        if sponseredUser == "1" {
            cell.sponserImg.isHidden = false
        }else {
            cell.sponserImg.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
        self.User_ids = searchArray[indexPath.row].user_id
        vc.User_ids = User_ids
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
extension UISearchBar {
    
    // Due to searchTextField property who available iOS 13 only, extend this property for iOS 13 previous version compatibility
    
    var compatibleSearchTextField: UITextField {
        guard #available(iOS 13.0, *) else { return legacySearchField }
        return self.searchTextField
    }
    
    private var legacySearchField: UITextField {
        if let textField = self.subviews.first?.subviews.last as? UITextField {
            // Xcode 11 previous environment
            return textField
        } else if let textField = self.value(forKey: "searchField") as? UITextField {
            // Xcode 11 run in iOS 13 previous devices
            return textField
        } else {
            // exception condition or error handler in here
            return UITextField()
        }
    }
}
