//
//  FriendsListVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 25/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class FriendsListVC: UIViewController {
    @IBOutlet weak var lblTitleNav: UILabel!
    var FriendsArray = [FollowingListingModel]()
    var followType = ""
    var OtherProfileId = ""
    var navTitle = ""
    @IBOutlet weak var friendsTableVw: UITableView!
    @IBOutlet weak var noFollowersLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.FriendsList()
        friendsTableVw.rowHeight = UITableView.automaticDimension
        friendsTableVw.estimatedRowHeight = 100
        self.lblTitleNav.text = navTitle
    }
      
    func FriendsList() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.FriendsListApi
        let parms : [String:Any] = ["user_id":user_ID,"followType":followType,"profileUserId":OtherProfileId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as! Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                //    self.notificationArr = response["data"] as? [[String:AnyObject]] ?? [[:]]
                if let details = response["userfollowlist"] as? [[String:Any]]{
                    self.FriendsArray.removeAll()
                    for i in 0..<details.count {
                        self.FriendsArray.append(FollowingListingModel(user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", email: details[i]["email"] as? String ?? "", role: details[i]["role"] as? String ?? "", password: details[i]["password"] as? String ?? "", photo: details[i]["photo"] as? String ?? "", location: details[i]["location"] as? String ?? "", countryCode: details[i]["countryCode"] as? String ?? "", twitterId: details[i]["twitterId"] as? String ?? "", latitude: details[i]["latitude"] as? String ?? "", longitude: details[i]["longitude"] as? String ?? "", description: details[i]["description"] as? String ?? "", verified: details[i]["verified"] as? String ?? "", totalFollowing: details[i]["totalFollowing"] as? String ?? "", totalFollowers: details[i]["totalFollowers"] as? String ?? "", verificateCode: details[i]["verificateCode"] as? String ?? "", created_at: details[i]["notification_id"] as? String ?? "", disabled: details[i]["notification_id"] as? String ?? "", allowPush: details[i]["notification_id"] as? String ?? "", device_type: details[i]["notification_id"] as? String ?? "", device_token: details[i]["notification_id"] as? String ?? "", status: details[i]["notification_id"] as? String ?? "", changeStatus: details[i]["notification_id"] as? String ?? "", profile_type: details[i]["notification_id"] as? String ?? "", sponsored_key: details[i]["notification_id"] as? String ?? "", ratingavg: details[i]["notification_id"] as? String ?? "", ratingcount: details[i]["notification_id"] as? String ?? "", isfollow: details[i]["notification_id"] as? String ?? "", followID: details[i]["followID"] as? String ?? "", username: details[i]["username"] as? String ?? ""))
                    }
                    self.friendsTableVw.reloadData()
                }
            }else{

            }
            self.noFollowersLbl.isHidden = self.FriendsArray.count == 0 ? false : true
            self.noFollowersLbl.text = self.FriendsArray.count == 0 ? self.navTitle == "Followers" ? "No Followers List Found" : "No Following List Found" : ""
            self.friendsTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension FriendsListVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FriendsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = friendsTableVw.dequeueReusableCell(withIdentifier: "FriendTableVwCell", for: indexPath) as! FriendTableVwCell
        cell.friendsUserImg.sd_setImage(with: URL(string: FriendsArray[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
        cell.selectionStyle = .none
        cell.lblUserName.text = "@\(FriendsArray[indexPath.row].username)"
        cell.nameLbl.text = FriendsArray[indexPath.row].name
    //    cell.lblRole.text = FriendsArray[indexPath.row].role
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        if user_ID == FriendsArray[indexPath.row].user_id {
            print("44444")
        } else {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
    vc.User_ids = FriendsArray[indexPath.row].user_id
    self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
    
}
