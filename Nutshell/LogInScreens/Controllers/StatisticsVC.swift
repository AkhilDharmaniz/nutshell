//
//  StatisticsVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 27/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class StatisticsVC: UIViewController {
    
    @IBOutlet weak var statisticsTableVw: UITableView!
    var StaticsLbl = ["Past 24 Hours","Past 7 Days","Past 30 Days","All Time"]
    var HoursLbl = ["0","0","0","0"]
    var hours = ""
    var dayscount = ""
    var monthCount = ""
    var allcount = ""
    var LinkID = ""
    var EnvelopeIDD = ""
    var commingFromEnvelope:Bool?
    override func viewDidLoad() {
    super.viewDidLoad()
    statisticsTableVw.rowHeight = UITableView.automaticDimension
    statisticsTableVw.estimatedRowHeight = 100
       
        if commingFromEnvelope == true {
            GetAllStaticticsList()
        }else {
            GetStaticticsApi()
        }
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:--- User Details
       func GetStaticticsApi(){
           IJProgressView.shared.showProgressView()
           let getUser = Constant.shared.baseUrl + Constant.shared.StaticticsApi
           let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"link_id":LinkID]
           AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
               print(response)
               IJProgressView.shared.hideProgressView()
               let result = response
               let hourscount = result.value(forKeyPath: "envelopecount_details.hourCount") as? Int ?? 1
            print(hourscount,"kkkkk")
               self.hours = "\(hourscount)"
               let days = result.value(forKeyPath: "envelopecount_details.dayCount") as? Int ?? 1
               print(days,"userrrr")
               self.dayscount = "\(days)"
               let Month = result.value(forKeyPath: "envelopecount_details.monthCount") as? Int ?? 1
               self.monthCount = "\(Month)"
               let allcounts = result.value(forKeyPath: "envelopecount_details.allCount") as? Int ?? 1
            print(allcounts,"ooii")
               self.allcount = "\(allcounts)"
            print(self.allcount,"lll")
        self.statisticsTableVw.reloadData()
           })
        
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
           }
       }
    
    // MARK:--- User Details
       func GetAllStaticticsList(){
           IJProgressView.shared.showProgressView()
           let getUser = Constant.shared.baseUrl + Constant.shared.AllStaticsLinks
           let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"envelope_id":EnvelopeIDD]
           AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
               print(response)
               IJProgressView.shared.hideProgressView()
               let result = response
               let hourscount = result.value(forKeyPath: "envelopecount_details.hourCount") as? Int ?? 1
            print(hourscount,"kkkkk")
               self.hours = "\(hourscount)"
               let days = result.value(forKeyPath: "envelopecount_details.dayCount") as? Int ?? 1
               print(days,"userrrr")
               self.dayscount = "\(days)"
               let Month = result.value(forKeyPath: "envelopecount_details.monthCount") as? Int ?? 1
               self.monthCount = "\(Month)"
               let allcounts = result.value(forKeyPath: "envelopecount_details.allCount") as? Int ?? 1
            print(allcounts,"ooii")
               self.allcount = "\(allcounts)"
            print(self.allcount,"lll")
        self.statisticsTableVw.reloadData()
           })
        
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
           }
       }
}

extension StatisticsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = statisticsTableVw.dequeueReusableCell(withIdentifier: "StatisticsTableVwCell", for: indexPath) as! StatisticsTableVwCell
        cell.selectionStyle = .none
        cell.lblPastHours.text = StaticsLbl[indexPath.row]
        if indexPath.row == 0 {
        cell.lblHours.text = hours
        }else if indexPath.row == 1 {
         cell.lblHours.text = dayscount
        }else if indexPath.row == 2 {
         cell.lblHours.text = monthCount
        }else if indexPath.row == 3 {
         cell.lblHours.text = allcount
        }
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
