//
//  AddEnvelopeVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 06/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit

class AddEnvelopeVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    var EnvelopeIds = ""
    @IBOutlet weak var threeSelectVw: UIView!
    @IBOutlet weak var txtExpire: UITextField!
    @IBOutlet weak var oneSelectVw: UIView!
    @IBOutlet weak var twoSelectVw: UIView!
    var movetoroot = Bool()
    var message = String()
    @IBOutlet weak var txtNameEnvelope: UITextField!
    @IBOutlet weak var txtLinkEnvelope: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtNameEnvelope.delegate = self
        txtLinkEnvelope.delegate = self
        txtDescription.delegate = self
        txtExpire.delegate = self
        txtExpire.addTarget(self, action: #selector(didEndediting(_:)), for: .allEvents)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtNameEnvelope:
            self.oneSelectVw.backgroundColor = #colorLiteral(red: 0.3454510868, green: 0.7843509316, blue: 0.759591043, alpha: 1)
        case txtLinkEnvelope:
            self.twoSelectVw.backgroundColor = #colorLiteral(red: 0.3412276506, green: 0.7920924425, blue: 0.7633551955, alpha: 1)
        case txtDescription:
            self.txtDescription.backgroundColor = #colorLiteral(red: 0.3412276506, green: 0.7920924425, blue: 0.7633551955, alpha: 1)
        default:break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtNameEnvelope:
            self.oneSelectVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtLinkEnvelope:
            self.twoSelectVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtDescription:
            self.txtDescription.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        default:break
        }
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        switch textView {
        case txtDescription:
            self.txtDescription.borderColor = #colorLiteral(red: 0.3412276506, green: 0.7920924425, blue: 0.7633551955, alpha: 1)
            
        default:break
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        switch textView {
        case txtDescription:
            self.txtDescription.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        default:break
        }
    }
    
    @objc func didEndediting(_ textField :UITextField!){
        if textField == txtExpire {
            RPicker.selectDate(title: "Select Date & Time", cancelText: "Cancel", datePickerMode: .dateAndTime, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: {[weak self] (selectedDate) in
                guard let self = self else {return}
                self.txtExpire.text = selectedDate.dateString()
            })
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtNameEnvelope{
            if range.location == 0 && string == " "
            {
                return false
            }
            return true
        }
        else if textField == txtLinkEnvelope{
            if textField.text?.last == " " || string == " "{
                return false
            }
        }
        return true
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if movetoroot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    func AddEnvelope(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.AppEnvelopeApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["name":txtNameEnvelope.text ?? "","link":txtLinkEnvelope.text ?? "","description":txtDescription.text ?? "","user_id":idValue,"expiryDate":txtExpire.text ?? "","envelope_id":EnvelopeIds]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: "Envelope Added successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
            IJProgressView.shared.hideProgressView()
        }) { (Error) in
        }
    }
    @IBAction func addBtnClicked(_ sender: UIButton) {
        
        if  (txtNameEnvelope.text?.isEmpty)!{
            ValidateData(strMessage: "Please enter Envelope Name")
        }
        else if txtLinkEnvelope.text == "" {
            alert("Nutshell", message: "Please add link", view: self)
            print("empty")
        }
            
        else  if txtLinkEnvelope.text != "" && verifyUrl(urlString:txtLinkEnvelope.text) == false{
            alert("Nutshell", message: "Please add Valid link", view: self)
        }else if (txtExpire.text?.isEmpty)!{
            ValidateData(strMessage: " Please Add Expiry Date")
        }
        else if (txtDescription.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter description")
        }
        else{
            self.AddEnvelope()
        }
    }
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
}
