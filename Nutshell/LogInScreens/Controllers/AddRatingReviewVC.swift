//
//  AddRatingReviewVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class AddRatingReviewVC: UIViewController {
    @IBOutlet weak var starRatingVw: FloatRatingView!
    
    var starsRating = 0
    var message = String()
    var OtherUserID = String()
    @IBOutlet weak var txtVwdescription: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Reset float rating view's background color
        starRatingVw.backgroundColor = UIColor.clear

        /** Note: With the exception of contentMode, type and delegate,
         all properties can be set directly in Interface Builder **/
        starRatingVw.delegate = self
        starRatingVw.contentMode = UIView.ContentMode.scaleAspectFit
        starRatingVw.type = .halfRatings
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if starRatingVw.rating == 0 {
         ValidateData(strMessage: "Please Give rating")
        }else if (txtVwdescription.text?.isEmpty)!{
            UserDefaults.standard.set(false, forKey: "uid")
            ValidateData(strMessage: "Please add Review")
        }else {
        AddRating()
        }
    }
    
    func AddRating(){
        IJProgressView.shared.showProgressView()
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
         let RatingData = Constant.shared.baseUrl + Constant.shared.AddRating
        let parms : [String:Any] = ["user_id":user_ID,"ratingby":OtherUserID,"review":txtVwdescription.text as? String ?? "","rating":starRatingVw.rating]
       
        AFWrapperClass.requestPOSTURL(RatingData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            //    self.commentTF.text = ""
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object:self.OtherUserID ?? "", userInfo:nil)
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else{
                alert("Nutshell", message: self.message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension AddRatingReviewVC: FloatRatingViewDelegate {

    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
       // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        print(ratingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
      //  updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
         print(ratingView.rating)
    }
    
}
