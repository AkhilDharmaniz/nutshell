//
//  StatusShowVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 06/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit
import ImageSlideshow
import SDWebImage
import IBAnimatable
import IQKeyboardManagerSwift
import SocketIO

class StatusShowVC: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var lblStatusCount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    // @IBOutlet weak var commentBoxVw: UIView!
    var responseArray =  [[String:AnyObject]]()
//    @IBOutlet weak var messageBottomVwConstraint: NSLayoutConstraint!
//    @IBOutlet weak var messageVwHeightCOnstraint: NSLayoutConstraint!
//    @IBOutlet weak var messageTvHeightConstrit: NSLayoutConstraint!
//    @IBOutlet weak var messageTV: AnimatableTextView!
    let textViewMaxHeight: CGFloat = 120
    var message = String()
    var LoginUserName = ""
    var LoginUserImage = ""
    var Status_id = ""
    var message_Type = "1"
    lazy var globalRoomId = String()
    lazy var chatUserId = String()
    lazy var chatUsername = String()
    //@IBOutlet weak var replyBtn: UIButton!
    var imgArr = [String]()
    var statusImg = ""
    var OtherUserId = String()
    var ViewstatusID = String()
    var OtheruserstatusArray = [UserStatusModel]()
    @IBOutlet weak var slideVw: ImageSlideshow!
    @IBOutlet weak var statusImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetStatusApi()
//        messageTV.delegate = self
//    messageTV.textContainerInset = UIEdgeInsets(top: 7, left: 13, bottom: 11, right: 13)
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        view.addGestureRecognizer(tap)
//    let userid = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
//        if userid == OtherUserId {
//            self.commentBoxVw.isHidden = true
//        }else {
//        self.commentBoxVw.isHidden = false
//        }
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//             // get the current height of your text from the content size
//             var height = textView.contentSize.height
//             // clamp your height to desired values
//             if height > 90 {
//                 height = 90
//             } else if height < 50 {
//                 height = 33
//             }
//             // update the constraint
//             messageTvHeightConstrit.constant = height
//             messageVwHeightCOnstraint.constant = height + 30
//             self.view.layoutIfNeeded()
//         }
//    @objc  override func dismissKeyboard() {
//              view.endEditing(false)
//          }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatFieldVC.self)
//        IQKeyboardManager.shared.disabledToolbarClasses = [ChatFieldVC.self]
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
//    }
    
//    @objc func handleKeyboardNotification(_ notification: Notification) {
//              if let userInfo = notification.userInfo {
//                  let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
//
//                  let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
//
//                  messageBottomVwConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
//                  UIView.animate(withDuration: 0.1, animations: { () -> Void in
//                      self.view.layoutIfNeeded()
//
//                  })
//              }
//          }
//
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//
//        if textView.text.count == 0{
//            messageTvHeightConstrit.constant = 33
//            messageVwHeightCOnstraint.constant = 60
//
//            //    messagePlaceholderLbl.isHidden = false
//        }else{
//            //  messagePlaceholderLbl.isHidden = true
//        }
//
//        return true
//    }
    
//    func addMessageApi(){
//              let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
//              let url = Constant.shared.baseUrl + Constant.shared.addMessageApi
//              var params = [String:Any]()
//
//        params = ["room_id":globalRoomId,"sender_id":userid,"message":messageTV.text!,"receiver_id" :OtherUserId,"message_type":"1","status_id":Status_id]
//               AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
//              AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
//                   AFWrapperClass.svprogressHudDismiss(view: self)
//                  let result = dict as AnyObject
//                  print(result)
//                  let msg = result["message"] as? String ?? ""
//                  let status = result["status"] as? Int ?? 0
//                  if status == 1{
//                    let appendData = result["data"] as? [String:Any] ?? [:]
//                    SocketManger.shared.socket.emit("newMessage",self.globalRoomId,appendData)
//                    self.responseArray.append(appendData as [String : AnyObject])
//                    self.messageTV.resignFirstResponder()
//                    self.messageTV.text = ""
//                    self.messageTvHeightConstrit.constant = 33
//                    //  self.messagePlaceholderLbl.isHidden = false
//                    self.messageVwHeightCOnstraint.constant = 60
////                    DispatchQueue.main.async {
////                        let alertController = UIAlertController(title: "Nutshell", message: "Comment sent successfully.", preferredStyle: .alert)
////                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
////                            UIAlertAction in
////                        }
////                        alertController.addAction(okAction)
////                        self.present(alertController, animated: true, completion: nil)
////                    }
//
//                  }else {
//                    print("lll")
//                }
//              }) { (error) in
//                  alert("Nutshell", message: error.localizedDescription, view: self)
//               AFWrapperClass.svprogressHudDismiss(view: self)
//                  //  AFWrapperClass.svprogressHudDismiss(view: self)
//              }
//          }
//
    
    func GetStatusApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetStatusApi
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let result = response
            print(status)
            self.getRoomIdApi(chatUserId:self.OtherUserId)
            if status == 1{
                let User_ID = result.value(forKeyPath: "userstatus.user_id") as? String ?? "user"
                print(User_ID,"userrrr")
                let userNames = result.value(forKeyPath: "userstatus.name") as? String ?? "user"
                print(userNames,"userrrr")
                self.lblUserName.text = userNames
                let Status_id = result.value(forKeyPath: "userstatus.status_id") as? String ?? "user"
                print(Status_id,"Statusssss")
                self.Status_id = Status_id
                let useremail = result.value(forKeyPath: "userstatus.email") as? String ?? "user@gmail.com"
                let profilePic = result.value(forKeyPath: "userstatus.photo") as? String ?? ""
               
                self.userImg.sd_setImage(with: URL(string: profilePic), placeholderImage: UIImage(named: "img1"))
                let img = result.value(forKeyPath: "userstatus.status_image") as? String ?? ""
                print(img)
                self.imgArr.append(img)
                print(self.imgArr,"showww")
                let statusCount = result.value(forKeyPath: "userstatus.statuscount") as? String ?? ""
                print(statusCount,"countttt")
                self.lblStatusCount.text = "Seen by \(statusCount)"
                let statusDate = result.value(forKeyPath: "userstatus.statusdate") as? String ?? ""
                print(statusDate)
                self.lblDate.text = self.dateDiff(dateStr:statusDate)
                self.ViewStatusApis()
                var sdWebImageSource = [InputSource]()
                for i in 0..<self.imgArr.count{
                    let userImage = self.imgArr[i].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
                    sdWebImageSource.append(SDWebImageSource(urlString:userImage)!)
                }
                self.slideVw.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
                self.slideVw.contentScaleMode = UIView.ContentMode.scaleAspectFit
                let pageControl = UIPageControl()
                pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 0.9960784314, green: 0.3411764706, blue: 0.4666666667, alpha: 1)
                pageControl.pageIndicatorTintColor = UIColor.lightGray
                self.slideVw.pageIndicator = pageControl
                // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
                self.slideVw.activityIndicator = DefaultActivityIndicator()
                // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
                self.slideVw.setImageInputs(sdWebImageSource)
            }else{
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    func dateDiff(dateStr:String) -> String {
    let f:DateFormatter = DateFormatter()
    // f.timeZone = NSTimeZone.local
    f.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

    let now = f.string(from: NSDate() as Date)


    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    print(toLongDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", fromdate: dateStr))
    let showDate = inputFormatter.date(from: toLongDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSZ", fromdate: dateStr)!)
    //#colorLiteral(red: 0.1846026778, green: 0.1725854874, blue: 0.1682911813, alpha: 1)
    let date1:Date = showDate!
    let date2: Date = Date()
    let calendar: Calendar = Calendar.current

    let components: DateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1, to: date2)



    let weeks = Int(components.month!)
    let days = Int(components.day!)
    let hours = Int(components.hour!)
    let min = Int(components.minute!)
    let sec = Int(components.second!)
    let year = Int(components.year!)


    var timeAgo = ""
    if sec == 0 {
    timeAgo = "just now"
    }else if (sec > 0){
    if (sec >= 2) {
    timeAgo = "\(sec) secs ago"
    } else {
    timeAgo = "\(sec) sec ago"
    }
    }

    if (min > 0){
    if (min >= 2) {
    timeAgo = "\(min) mins ago"
    } else {
    timeAgo = "\(min) min ago"
    }
    }

    if(hours > 0){
    if (hours >= 2) {
    timeAgo = "\(hours) hrs ago"
    } else {
    timeAgo = "\(hours) hr ago"
    }
    }

    if (days > 0) {
    if (days >= 2) {
    timeAgo = "\(days) days ago"
    } else {
    timeAgo = "\(days) day ago"
    }
    }

    if(weeks > 0){
    if (weeks >= 2) {
    timeAgo = "\(weeks) months ago"
    } else {
    timeAgo = "\(weeks) month ago"
    }
    }

    if(year > 0){
    if (year >= 2) {
    timeAgo = "\(year) years ago"
    } else {
    timeAgo = "\(year) year ago"
    }
    }


    // print("timeAgo is===> \(timeAgo)")
    return timeAgo;
    }
    
    func formattedDateFromString(fromFormat:String,dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = fromFormat
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.locale = Locale(identifier: "en")
            //outputFormatter.timeZone = TimeZone(identifier: "UTC")
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    func toLongDate(withFormat format: String = "yyyy-MM-dd",fromdate:String)-> String?{
        let date = Date(timeIntervalSince1970: fromdate.doubleValue)
        let date1 = formattedDateFromString(fromFormat: "yyyy-MM-dd HH:mm:ssZ", dateString: "\(date)", withFormat: format)
        return date1
    }
    
    func ViewStatusApis() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.ViewStatusApi
        let parms : [String:Any] = ["user_id":user_ID,"status_id":ViewstatusID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            if status == 1{
            }else{
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    open func getRoomIdApi(chatUserId:String){
           let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
           let chatData = Constant.shared.baseUrl + Constant.shared.requestChat
           let params:[String:Any] = [
               "user_id":idValue,
               "owner_id":chatUserId
           ]
           IJProgressView.shared.showProgressView()
           AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
               print(response)
               IJProgressView.shared.hideProgressView()
               let status = response["status"] as! Int
               print(status)
               let message = response["message"] as? String ?? ""
               if status == 1{
                   let resultDict = response as? [String:AnyObject] ?? [:]
                   self.globalRoomId = resultDict["room_id"] as? String ?? ""
                print(self.globalRoomId,"ggggg")
                   print("$$$$$$$$\(resultDict)")
                self.chatUserId = resultDict["user_id"] as? String ?? ""
                print(chatUserId,"iiiiidddd")
                self.chatUsername = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                print(self.chatUsername,"llll")
                self.LoginUserName = response.value(forKeyPath: "user_detail.name") as? String ?? "user"
                print(self.LoginUserName,"llll")
                self.LoginUserImage = response.value(forKeyPath: "user_detail.photo") as? String ?? "user"
                print(self.LoginUserImage,"llll")
               }else{
            alert("Nutshell", message: self.message, view: self)
               }
           })
           { (error) in
               IJProgressView.shared.hideProgressView()
               print(error)
               alert("Nutshell", message: error.localizedDescription, view: self)
           }
       }
//    @IBAction func sendBtnClicked(_ sender: UIButton) {
//        let trimmedString = messageTV.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
//        
//        if trimmedString == ""{
//        }else{
//            messageTV.resignFirstResponder()
//            addMessageApi()
//            messageTV.text = ""
//        }
//    }
    
    
    @IBAction func replyBtnClicked(_ sender: UIButton) {
   //  getRoomIdApi(chatUserId:OtherUserId)
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
}
