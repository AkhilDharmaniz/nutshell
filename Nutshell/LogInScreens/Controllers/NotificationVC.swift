//
//  NotificationVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 23/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
class NotificationVC: UIViewController {
    var message = String()
    @IBOutlet weak var notificationTableVw: UITableView!
    var OtherUserId = String()
    @IBOutlet weak var noDataVw: UIView!
    var Notificationimg = ["user","user-1","user-2","user-3","add-update"]
    var NotificationuserName = ["Jiapel Ma liked your Profile","Mahmudal Husan added Facebook","Faxxel Kair send Follow Request","ulip cui added Instagram","Olha Bahularia accepted Request"]
    var Clicks = ["Nov 1 at 22:14","Nov 15 at 20:14","Oct 1 at 16:10","Jan 5 at 12:14","Mar 7 at 07:16"]
    var NotificationArray = [NotificationModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTableVw.rowHeight = UITableView.automaticDimension
        notificationTableVw.estimatedRowHeight = 100
        self.noDataVw.isHidden = true
        // tabBarController?.tabBar.items![2].badgeValue = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationList()
        RemoveNotifcaion()
        self.noDataVw.isHidden = true
    }
    func NotificationList() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.NotificationApi
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            self.NotificationArray.removeAll()
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            let count = response["count"] as? Int ?? 0
            if count == 0 {
                self.tabBarController?.tabBar.items![2].badgeValue = nil
            }else {
                self.tabBarController?.tabBar.items![2].badgeValue = "\(count)"
                self.tabBarController?.tabBar.items![2].badgeColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7430018783, blue: 0, alpha: 1))
                
                print(count,"opop")
            }
            if status == 1{
                self.noDataVw.isHidden = true
                //    self.notificationArr = response["data"] as? [[String:AnyObject]] ?? [[:]]
                if let details = response["data"] as? [[String:Any]]{
                    for i in 0..<details.count {
                        self.NotificationArray.append(NotificationModel(notification_id: details[i]["notification_id"] as? String ?? "", message: details[i]["message"] as? String ?? "", notification_type: details[i]["notification_type"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", otherID: details[i]["otherID"] as? String ?? "", room_id: details[i]["room_id"] as? String ?? "", followID: details[i]["followID"] as? String ?? "", creation_date: details[i]["creation_date"] as? String ?? "", viewed_status: details[i]["viewed_status"] as? String ?? "", notification_read_status: details[i]["notification_read_status"] as? String ?? "", creation_date_timestamp: details[i]["creation_date_timestamp"] as? String ?? "", name: details[i]["username"] as? String ?? "", image: details[i]["image"] as? String ?? "", title_message: details[i]["title_message"] as? String ?? ""))
                    }
                }
            }else{
                self.noDataVw.isHidden = false
            }
            self.notificationTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func RemoveNotifcaion() {
        //IJProgressView.shared.showProgressView()
        let token = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let itemdelete = Constant.shared.baseUrl + Constant.shared.updateNotification
        let parms : [String:Any] = ["user_id":token]
        
        AFWrapperClass.requestPOSTURL(itemdelete, params: parms, success: { (response) in
            // IJProgressView.shared.hideProgressView()
            print(response)
        })
        { (error) in
            //    IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func AcceptREquest(Type:String,OtherID:String,FollowID:String) {
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        
        IJProgressView.shared.showProgressView()
        let ContactUs = Constant.shared.baseUrl + Constant.shared.approveRejectApi
        
        let parms : [String:Any] = ["userID":idValue,"followUserID":OtherID,"followID":FollowID,"appoveRejectFollow":Type]
        AFWrapperClass.requestPOSTURL(ContactUs, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            let status = response["status"] as? String ?? ""
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == "1" {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        //   self.navigationController?.popViewController(animated: true)
                        self.NotificationList()
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension NotificationVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotificationArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = notificationTableVw.dequeueReusableCell(withIdentifier: "NotificationTableVwCell", for: indexPath) as! NotificationTableVwCell
        cell.notificationImg.sd_setImage(with: URL(string: NotificationArray[indexPath.row].image), placeholderImage: UIImage(named: "img1"))
        cell.selectionStyle = .none
        cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
        cell.lblNotificationMessage.text = NotificationArray[indexPath.row].creation_date
        let N_TYpe = NotificationArray[indexPath.row].notification_type
        
        cell.acceptedBTN = {
            print("LLL")
            let OtherUserId = self.NotificationArray[indexPath.row].otherID
            let FollowId = self.NotificationArray[indexPath.row].followID
            let Accept = "1"
            self.AcceptREquest(Type:Accept,OtherID:OtherUserId,FollowID:FollowId)

        }
        cell.rejectedBTN = {
            print("MMM")
            let OtherUserId = self.NotificationArray[indexPath.row].otherID
            let FollowId = self.NotificationArray[indexPath.row].followID
            let Accept = "2"
            self.AcceptREquest(Type:Accept,OtherID:OtherUserId,FollowID:FollowId)
        }
        
        if N_TYpe == "0" {
            cell.lblNotificationMessage.isHidden = false
            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
            cell.lblNotificationMessage.text = NotificationArray[indexPath.row].creation_date
            cell.followViw.isHidden = true
            cell.heightCOnstraintforChat.constant = 0
            
        } else if N_TYpe == "1"{
            cell.lblNotificationMessage.isHidden = true
            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
            cell.followViw.isHidden = false
            cell.heightCOnstraintforChat.constant = 0
        }else if N_TYpe == "2" {
            cell.lblNotificationMessage.isHidden = false
            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
            cell.lblNotificationMessage.text = NotificationArray[indexPath.row].creation_date
            cell.followViw.isHidden = true
            cell.heightCOnstraintforChat.constant = 0
        }else if N_TYpe == "7"{
            cell.lblNotificationMessage.isHidden = false
            cell.lblMessage.isHidden = false
            cell.lblMessage.text = NotificationArray[indexPath.row].title_message
            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
            cell.followViw.isHidden = true
            cell.heightCOnstraintforChat.constant = 20
        }
        else if N_TYpe == "3" || N_TYpe == "4" || N_TYpe == "5" || N_TYpe == "6"{
            cell.lblNotificationMessage.isHidden = false
            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
            cell.lblNotificationMessage.text = NotificationArray[indexPath.row].creation_date
            cell.followViw.isHidden = true
            cell.heightCOnstraintforChat.constant = 0
        }else {
            cell.heightCOnstraintforChat.constant = 0
        }
        
        //        else if N_TYpe == "4" {
        //            cell.lblNotificationMessage.isHidden = false
        //            cell.lblNotificationTitle.text = NotificationArray[indexPath.row].message
        //            cell.lblNotificationMessage.text = NotificationArray[indexPath.row].creation_date
        //            cell.followViw.isHidden = true
        //        } else if N_TYpe == "5" {
        //
        //        }else {
        //
        //        }
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let N_TYpe = NotificationArray[indexPath.row].notification_type
        if N_TYpe == "0" {
            let userprofile = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            self.OtherUserId = NotificationArray[indexPath.row].otherID
            userprofile.User_ids = OtherUserId
            self.navigationController?.pushViewController(userprofile, animated: true)
            
        } else if N_TYpe == "1"{
            
        }else if N_TYpe == "2" {
            let userprofile = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            self.OtherUserId = NotificationArray[indexPath.row].otherID
            userprofile.User_ids = OtherUserId
            self.navigationController?.pushViewController(userprofile, animated: true)
            
        }else if N_TYpe == "3" {
            let userprofile = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            self.OtherUserId = NotificationArray[indexPath.row].otherID
            userprofile.User_ids = OtherUserId
            self.navigationController?.pushViewController(userprofile, animated: true)
        }
        else if N_TYpe == "4" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
            vc.selectedIndex = 0
            self.navigationController?.pushViewController(vc, animated: false)
        }else if N_TYpe == "5" {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let rootVc = storyBoard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            rootVc.senderId = NotificationArray[indexPath.row].otherID
            rootVc.receiverName = NotificationArray[indexPath.row].name
            rootVc.roomId = NotificationArray[indexPath.row].room_id
            self.navigationController?.pushViewController(rootVc, animated: true)
        }else if N_TYpe == "6" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
            vc.postId = NotificationArray[indexPath.row].otherID
            self.navigationController?.pushViewController(vc, animated: false)
        }else if N_TYpe == "7" {
            
            let vc = self.tabBarController?.viewControllers?[0] as? HomeFeedVC
            vc?.postId = NotificationArray[indexPath.row].otherID
            self.tabBarController?.selectedIndex = 0
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
            //            vc.selectedIndex = 0
            //            let homeVc = vc.viewControllers?[0] as? HomeFeedVC
            //            homeVc?.postId = NotificationArray[indexPath.row].otherID
            //            self.navigationController?.pushViewController(vc, animated: false)
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
//            controller.postId = NotificationArray[indexPath.row].otherID
//            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
    
}
