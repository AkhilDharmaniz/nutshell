//
//  CustomLinkVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 17/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
class CustomLinkVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate  {
    var imagePicker = UIImagePickerController()
    var imgData = [UIImage]()
    var movetoroot = Bool()
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var userImgs: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userImgs.image = #imageLiteral(resourceName: "img1")
        txtName.delegate = self
        imagePicker.delegate = self
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if (txtName.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter name")
        }
        else {
            DispatchQueue.main.async {
                self.AddCustomLink()
            }
        }
    }
    @IBAction func imagePickerBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Nutshell", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        userImgs.image  = tempImage
        imgData.append(tempImage)
        picker.dismiss(animated: true, completion: nil)
    }
     
    func AddCustomLink(){
          IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.customLinks
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let img = userImgs.image
        let imageString = userImgs.image == #imageLiteral(resourceName: "img1") ? "" : img?.jpegData(compressionQuality: 0.3)?.base64EncodedString() ?? ""
//        let img = userImgs.image
//        let imageString = img?.jpegData(compressionQuality: 0.3)?.base64EncodedString() ?? ""
        let parms:[String:Any] = ["user_id":idValue,"title":txtName.text ?? "","image":imageString]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
        print(response)
        self.navigationController?.popViewController(animated: true)
        IJProgressView.shared.hideProgressView()
          }) { (Error) in
          }
      }
    
    }
   
    

