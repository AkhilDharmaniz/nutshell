//
//  UserQRCodeVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 01/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class UserQRCodeVC: UIViewController {
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var qrCodeImage: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
//        alert("idddd", message: id, view: self)
        // Do any additional setup after loading the view.
        getData()
    }
    @IBAction func saveQrCodeBtnaclicked(_ sender: UIButton) {
    }
    
    // MARK:--- User Details
    func getData(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.ScanQrProfileAPi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":id]
        AFWrapperClass.requestUrlEncodedPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            //  let result = response
            IJProgressView.shared.hideProgressView()
            if let details = response["userprofileDetails"] as? [[String:Any]]{
                print(details,"mmm")
                
                details.forEach { (result) in
                    self.lblUserName.text = result["username"] as? String ?? ""
                    self.lblRole.text = result["email"] as? String ?? ""
                    self.userImage.sd_setImage(with: URL(string:result["photo"] as? String ?? ""), placeholderImage: UIImage(named: "img1"))
                    let image = (result["QrCode"] as? String ?? "").base64ToImage()
                    self.qrCodeImage.image = image
                }
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }   
    @IBAction func crossBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
