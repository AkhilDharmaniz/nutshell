//
//  EnvelopeOtherUserTableVw.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 23/02/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class EnvelopeOtherUserTableVw: UITableViewCell {

    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblEnvelopeName: UILabel!    
    
    @IBOutlet weak var lblCornerTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
