//
//  ChatfieldTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 16/02/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class ChatfieldTableVwCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblChatName: UILabel!
    @IBOutlet weak var ChatuserImge: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
