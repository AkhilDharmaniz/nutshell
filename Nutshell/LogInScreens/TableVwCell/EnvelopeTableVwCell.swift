//
//  EnvelopeTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 31/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class EnvelopeTableVwCell: UITableViewCell {
     var DotBTN:(()->Void)?
     var LInkBTN:(()->Void)?
    
    @IBOutlet weak var switchBtn: UISwitch!
    var switchAction:((Bool)->())?
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblUrl: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var threedotsImg: UIImageView!
    
    @IBOutlet weak var threeDotsBtn: UIButton!
    @IBOutlet weak var onOffSiwtch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func linksCLickesBtnClicked(_ sender: UIButton) {
        LInkBTN!()
    }
    @IBAction func dotBtnClicked(_ sender: UIButton) {
         DotBTN!()
    }
    
    @IBAction func EnvelopeSwitch(_ sender: UISwitch) {
         if sender.isOn {
                   if let action = switchAction {
                       action(true)
                   }
               }else {
                   if let action = switchAction {
                       action(false)
                   }
               }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
