//
//  StatisticsTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 27/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit
class StatisticsTableVwCell: UITableViewCell {
    @IBOutlet weak var lblHours: UILabel!
    @IBOutlet weak var lblPastHours: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
