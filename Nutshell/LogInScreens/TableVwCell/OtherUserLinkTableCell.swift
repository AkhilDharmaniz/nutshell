//
//  OtherUserLinkTableCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class OtherUserLinkTableCell: UITableViewCell {
    @IBOutlet weak var lblFirstLetter: UILabel!
    @IBOutlet weak var lblLinkClicks: UILabel!
    @IBOutlet weak var lblLinkProfileName: UILabel!
    @IBOutlet weak var linkProfileImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
