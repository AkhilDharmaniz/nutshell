//
//  AllStatisticsTableCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class AllStatisticsTableCell: UITableViewCell {

    @IBOutlet weak var lblCounts: UILabel!
    @IBOutlet weak var lblTopicTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
