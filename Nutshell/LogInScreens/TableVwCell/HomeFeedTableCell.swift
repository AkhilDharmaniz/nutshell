//
//  HomeFeedTableCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 29/06/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import ASPVideoPlayer

class HomeFeedTableCell: UITableViewCell {

    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var videoVwHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtVw: UITextView!
    @IBOutlet weak var videoPlayerVw: ASPVideoPlayer!
    @IBOutlet weak var lblFavCount: UILabel!
    @IBOutlet weak var lblcommentCount: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    @IBOutlet weak var thumbNailImgView: UIImageView!
    
    @IBOutlet weak var videoPlayBtn: UIButton!
    
    @IBOutlet weak var likeImg: UIImageView!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var commentBtn: UIButton!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var deleteImg: UIImageView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var threeDotImg: UIImageView!
    @IBOutlet weak var threeDotBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
