//
//  LinkAddTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 03/04/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
protocol LinkProtocol {
    func stringpass(index1:String,index2:String)
}

class LinkAddTableVwCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var lblLetterTitle: UILabel!
    
    var index1 = ""
    var index2 = ""
    var allData = [String]()
    var delegate:LinkProtocol?
    
    @IBOutlet weak var txtLinks: UITextField!
       @IBOutlet weak var linkImages: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtLinks.delegate = self
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch txtLinks.tag {
        case 0:
            index1 = txtLinks.text ?? ""
        case 1:
            index2 = txtLinks.text ?? ""
        default:
            break
        }
        return true
    }
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        switch txtLinks.tag {
//        case 0:
//             delegate?.stringpass(index1: index1, index2: index2)
//        case 1:
//            delegate?.stringpass(index1: index1, index2: index2)
//        default:
//            break
//        }
//      //  delegate?.stringpass(index1: index1, index2: index2)
//    }
//    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
