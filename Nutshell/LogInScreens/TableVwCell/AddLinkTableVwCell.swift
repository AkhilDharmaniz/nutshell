//
//  AddLinkTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 06/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class AddLinkTableVwCell: UITableViewCell {

    @IBOutlet weak var lblAddLinkName: UILabel!
    @IBOutlet weak var addLinkImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
