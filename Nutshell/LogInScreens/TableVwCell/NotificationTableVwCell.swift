//
//  NotificationTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/09/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class NotificationTableVwCell: UITableViewCell {
    
    var acceptedBTN:(()->Void)?
    var rejectedBTN:(()->Void)?
    
     
    @IBOutlet weak var heightCOnstraintforChat: NSLayoutConstraint!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var followViw: UIView!
    @IBOutlet weak var lblNotificationMessage: UILabel!
    @IBOutlet weak var notificationImg: UIImageView!
    @IBOutlet weak var lblNotificationTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func acceptBtnClicked(_ sender: UIButton) {
        acceptedBTN!()
    }
    
    @IBAction func BtnRejectAction(_ sender: UIButton) {
       rejectedBTN!()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
