//
//  FriendTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 25/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class FriendTableVwCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var friendsUserImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
