//
//  ExploreTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 21/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class ExploreTableVwCell: UITableViewCell {
    @IBOutlet weak var sponserImg: UIImageView!
   // @IBOutlet weak var lblDescriptions: UILabel!
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var LblUserName: UILabel!
    @IBOutlet weak var userProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
