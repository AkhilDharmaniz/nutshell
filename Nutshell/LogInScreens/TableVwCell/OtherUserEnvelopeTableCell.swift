//
//  OtherUserEnvelopeTableCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class OtherUserEnvelopeTableCell: UITableViewCell {
    @IBOutlet weak var lblEnvelopeTitle: UILabel!
    @IBOutlet weak var envelopeImg: UIImageView!
    @IBOutlet weak var LblCornerTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
