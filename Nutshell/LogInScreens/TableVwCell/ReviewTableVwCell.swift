//
//  ReviewTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/03/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class ReviewTableVwCell: UITableViewCell {

    @IBOutlet weak var showRatingVw: FloatRatingView!
    
    @IBOutlet weak var normalUserRatingVw: FloatRatingView!
    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star5: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var lblReviewmsg: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var UserprofileImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
