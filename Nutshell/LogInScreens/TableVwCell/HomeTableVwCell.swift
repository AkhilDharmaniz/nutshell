//
//  HomeTableVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 21/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit
import GoogleMobileAds
class HomeTableVwCell: UITableViewCell {
    @IBOutlet weak var adsHightConstVw: NSLayoutConstraint!
    @IBOutlet weak var adsView: GADBannerView!
    @IBOutlet weak var Lblsponser: UILabel!
    @IBOutlet weak var lbldescription: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var profileImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     //   self.Lblsponser.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
