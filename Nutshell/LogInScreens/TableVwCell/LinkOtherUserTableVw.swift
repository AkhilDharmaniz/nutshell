//
//  LinkOtherUserTableVw.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 23/02/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class LinkOtherUserTableVw: UITableViewCell {

    @IBOutlet weak var lbllinkname: UILabel!
    
    @IBOutlet weak var userimageProfiles: UIImageView!
    @IBOutlet weak var lblLinkCounts: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
