//
//  ReportUserVC.swift
//  Nutshell
//
//  Created by apple on 04/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
class ReportTableViewCell: UITableViewCell{
    
    @IBOutlet weak var reportLbl: UILabel!
    @IBOutlet weak var selectImg: UIImageView!
}
class ReportUserVC: UIViewController {
    @IBOutlet weak var reportUserTableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var reportsArr = [ReportsModel]()
    var reportId = ""
    var reportType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        reportUserTableView.dataSource = self
        reportUserTableView.delegate = self
        getAllReportResponse()
        // Do any additional setup after loading the view.
    }
    func getAllReportResponse(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetAllReportReasons
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let msg = response["message"] as? String ?? ""
            print(status)
            self.reportsArr.removeAll()
            if status == 1{
                if let data = response["data"] as? [[String:Any]]{
                    for i in 0..<data.count{
                        self.reportsArr.append(ReportsModel(reasonId: data[i]["reasonId"] as? String ?? "", reportReasons: data[i]["reportReasons"] as? String ?? "",selected: false))
                    }
                }
            }else{
                alert(Constant.shared.AppName, message: msg, view: self)
            }
            self.reportUserTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }

    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButton(_ sender: Any) {
        let selectedElement = reportsArr.filter({$0.selected == true})
        if selectedElement.count == 0{
            alert(Constant.shared.AppName, message: "Please select your report reason", view: self)
        }else{
            addReport()
        }
        
    }
    func addReport() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        //UserDefaults.standard.set(selectedName, forKey: "Reort")
        IJProgressView.shared.showProgressView()
        let ReportType = Constant.shared.baseUrl + Constant.shared.AddReport
        let selectedElement = reportsArr.filter({$0.selected == true})
        let parms : [String:Any] = ["reasonId":selectedElement[0].reasonId,"reportedId":reportId,"reportType":reportType,"reportedBy":user_ID]
        print(parms)
        AFWrapperClass.requestPOSTURL(ReportType, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                showAlertMessage(title: Constant.shared.AppName, message: message, okButton: "OK", controller: self) {
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                alert(Constant.shared.AppName, message: message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
            
        }
    }
}
extension ReportUserVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reportsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ReportTableViewCell
        cell.reportLbl.text = reportsArr[indexPath.row].reportReasons
        cell.selectImg.image = reportsArr[indexPath.row].selected == true ? #imageLiteral(resourceName: "radioGreen") : #imageLiteral(resourceName: "radio1Green")
        DispatchQueue.main.async {
            self.heightConstraint.constant = self.reportUserTableView.contentSize.height
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.reportsArr = self.reportsArr.map({ obj in
            var changeObj = obj
            changeObj.selected = false
            return changeObj
        })
        self.reportsArr[indexPath.row].selected = true
        self.reportUserTableView.reloadData()
    }

}
