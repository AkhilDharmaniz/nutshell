//
//  SignUpVC.swift
//  Nutshell
//
//  Created by Dharmani Apps on 12/06/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//
import UIKit

import SafariServices
//import GoogleMaps
//import GooglePlaces
import CoreLocation

class SignUpVC: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate {
    var lat = String()
    var long = String()
    var lats = Double()
    var longs = Double()
    var placeLocationName = ""
    var profileSelection = ""
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    @IBOutlet weak var selectedVwTwo: UIView!
    @IBOutlet weak var selectedVwOne: UIView!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRole: FloatinLabelInput!
    @IBOutlet weak var doBTF: FloatinLabelInput!
    @IBOutlet weak var genderTF: FloatinLabelInput!
    @IBOutlet weak var selectedVwThree: UIView!
    @IBOutlet weak var selectedVwFour: UIView!
    private var datePicker: UIDatePicker?
    var selectedAgree = false
    var selectedbusinessAgree = false
    var message = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentLocation = nil
        setupLocationManager()
        self.locationManager.delegate = self
        //        setupLocationManager()
        self.navigationController?.isNavigationBarHidden = true
        txtname.delegate = self
        txtEmail.delegate = self
        txtPassword.delegate = self
        txtRole.delegate = self
        txtname.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        txtRole.attributedPlaceholder = NSAttributedString(string: "Location", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
    }
    func setupLocationManager(){
    locationManager = CLLocationManager()
    locationManager.delegate = self
    self.locationManager.requestAlwaysAuthorization()
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if currentLocation == nil {
            currentLocation = locations.last
            locationManager.stopMonitoringSignificantLocationChanges()
            let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
            let latitude = String(format: "%.6f", locationValue.latitude)
            print(latitude,"lo")
            self.lats = locationValue.latitude
            let longitude = String(format: "%.6f", locationValue.longitude)
            print(longitude,"longitude")
            self.longs = locationValue.longitude
            convertLatLongToAddress(latitude:locationValue.latitude,longitude:locationValue.longitude)
            UserDefaults.standard.set(latitude, forKey: "Latitude")
            UserDefaults.standard.set(longitude, forKey: "Longitude")
            self.lat.append(latitude)
            self.long.append(longitude)
            //    print("locations = \(locationValue)")
            locationManager.stopUpdatingLocation()
        }
    }
    
    func convertLatLongToAddress(latitude:Double,longitude:Double){
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:  lats, longitude: longs)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Location name
            if let locationName = placeMark?.location {
                print(locationName)
            }
            // Street address
            if let street = placeMark?.thoroughfare {
                print(street)
            }
            // City
            if let citys = placeMark?.subAdministrativeArea {
                print(citys)
                UserDefaults.standard.set(citys, forKey: "City")
                self.placeLocationName = citys
                self.txtRole.text = self.placeLocationName
            }
            // City
            if let ppp = placeMark?.areasOfInterest {
                print(ppp,"lop")
            }
            // Zip code
            if let zip = placeMark?.isoCountryCode {
                print(zip)
            }
            // Country
            if let country = placeMark?.country {
                print(country)
            }
        })
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtname || textField == txtRole {
            if range.location == 0 && string == " "
            {
                return false
            }
            return true
        }
        else if textField == txtEmail || textField == txtPassword {
            if textField.text?.last == " " || string == " "{
                return false
            }
        }
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtname:
            self.selectedVwOne.backgroundColor = Constant.shared.appcolor
        case txtEmail:
            self.selectedVwTwo.backgroundColor = Constant.shared.appcolor
        case txtPassword:
            self.selectedVwThree.backgroundColor = Constant.shared.appcolor
        case txtRole:
            self.selectedVwFour.backgroundColor = Constant.shared.appcolor
        default:break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtname:
            self.selectedVwOne.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtEmail:
            self.selectedVwTwo.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtPassword:
            self.selectedVwThree.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        case txtRole:
            self.selectedVwFour.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        default:break
        }
    }
    //    func setupLocationManager(){
    //        locationManager = CLLocationManager()
    //       // locationManager.delegate = self
    //        self.locationManager.requestAlwaysAuthorization()
    //        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    //        locationManager.startUpdatingLocation()
    //    }
    //    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //
    //        if currentLocation == nil {
    //            currentLocation = locations.last
    //            locationManager.stopMonitoringSignificantLocationChanges()
    //            let locationValue:CLLocationCoordinate2D = manager.location!.coordinate
    //            let latitude = String(format: "%.6f", locationValue.latitude)
    //            let longitude = String(format: "%.6f", locationValue.longitude)
    //            UserDefaults.standard.set(latitude, forKey: "Latitude")
    //            UserDefaults.standard.set(longitude, forKey: "Longitude")
    //            self.lat.append(latitude)
    //            self.long.append(longitude)
    //            getAddressFromLatLon(pdblLatitude: lat, withLongitude: long)
    //            //    print("locations = \(locationValue)")
    //            locationManager.stopUpdatingLocation()
    //            self.getCategory()
    //            self.UpdateUserLocation()
    //            self.GetProductListing()
    //        }
    //    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // MARK:--- InternetConnection
    override func viewDidAppear(_ animated: Bool) {
        if CheckInternet.Connection(){
        }
        else{
            alert("Nutshell", message: "Your Device is not connected with internet", view: self)
            //self.Alert(Message: "Your Device is not connected with internet")
        }
    }
    @IBAction func businessAccountBtnClicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
       selectedbusinessAgree = sender.isSelected
        print(selectedbusinessAgree)
        print(selectedbusinessAgree,"pppp")
        if selectedbusinessAgree == false {
        print("NormalBusin")
        let accountUpdate = "0"
        self.profileSelection = accountUpdate
        }else {
        print("Business")
        let accountUpdate = "1"
        self.profileSelection = accountUpdate
        }
    }
    @IBAction func agreeBtnClicked(_ sender: UIButton) {
    sender.isSelected = !sender.isSelected
        selectedAgree = !selectedAgree
   // selectedAgree = sender.isSelected
    print(selectedAgree)
    }
    @IBAction func termsCondtionBtnClicked(_ sender: UIButton) {
        if let url = URL(string:Constant.shared.websiteUrl+Constant.shared.TermsConditionsLink)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
        }
    }
    @IBAction func signinBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func signUpBtnClicked(_ sender: UIButton) {
        if (txtname.text?.isEmpty)!{
        ValidateData(strMessage: " Please enter username")
        }
        else if (txtEmail.text?.isEmpty)!{
        ValidateData(strMessage: " Please enter email address")
        }
        else if isValidEmail(testStr: (txtEmail.text)!) == false{
        ValidateData(strMessage: "Enter valid email")
        }
        else if (txtPassword.text?.isEmpty)!{
        ValidateData(strMessage: " Please enter password")
        }else if (txtPassword!.text!.count) < 4 || (txtPassword!.text!.count) > 150{
        ValidateData(strMessage: "Please enter minimum 4 digit password")
        UserDefaults.standard.set(txtPassword.text, forKey: "password")
        UserDefaults.standard.string(forKey: "password")
        }else if (txtRole.text?.isEmpty)! {
        ValidateData(strMessage: "Please enter Location")
        }
        else if selectedAgree == false{
        ValidateData(strMessage: "Please agree terms and conditions")
        }
//        else if selectedAgree == false{
//        ValidateData(strMessage: "Please agree terms and conditions")
//        }
        else{
        signUp()
        }
        }
    func signUp(){
        IJProgressView.shared.showProgressView()
        let signUpUrl = Constant.shared.baseUrl + Constant.shared.signUpUrl
        let deviceID = UserDefaults.standard.value(forKey: "deviceToken") as? String ?? ""
        let userlatitude = UserDefaults.standard.value(forKey: "Latitude")
        let userlongitude = UserDefaults.standard.value(forKey: "Longitude")
        let userCity = UserDefaults.standard.value(forKey: "City")
        let userlatitudes = UserDefaults.standard.value(forKey: "Latitude")
        let userlongitudes = UserDefaults.standard.value(forKey: "Longitude")
        let parms : [String:Any] = ["email": txtEmail.text!,"password": txtPassword.text!,"username": txtname.text!,"device_token":deviceID,"device_type":"1","role":"","latitude":userlatitudes,"longitude":userlongitudes,"profile_type":profileSelection,"location":txtRole.text ?? ""]
        UserDefaults.standard.set(txtPassword.text, forKey: "oldPassword")
        UserDefaults.standard.set(txtname.text, forKey: "userName")
        UserDefaults.standard.set(txtEmail.text, forKey: "useremail")
        // print(parms)
        AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (dict) in
            IJProgressView.shared.hideProgressView()
            print(dict)
            let userID = dict.value(forKey: "id") as? Int ?? 0
            print(userID)
            UserDefaults.standard.set(userID, forKey: "Uid")
            if let result = dict as? [String:Any]{
                print(result)
                let detail = result["user_detail"] as?  [[String:Any]]
                print(detail)
                
                let data = detail?[0]
                print(data)
                
                let googileID = data?["google_id"] as? String ?? ""
                UserDefaults.standard.set(googileID, forKey: "googleId")
                self.message = result["message"] as? String ?? ""
                let status = result["status"] as? Int ?? 0
                if status == 0{
                    //                    let token = result["username"] as? String
                    //                    UserDefaults.standard.set(token, forKey: "tokenFString")
                    //                    print(token)
                    alert("Nutshell", message: self.message, view: self)
                }else{
                    DispatchQueue.main.async {
                        let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    //                    UserDefaults.standard.set(true, forKey: "ISUSERLOGGEDIN")
                    //                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                    //                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }) { (error) in
        }
    }
}
extension UIViewController {
    func isValidUsername(testStr:String) -> Bool
    {
        let emailRegEx = ".*[^A-Za-z].*"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func isValiddate(testStr:String)->Bool
    {
        let dateRegEx = "^(0[1-9]|[12][0-9]|3[01])[- \\.](0[1-9]|1[012])[- \\.](19|20)\\d\\d$"
        let dateTest=NSPredicate(format:"SELF MATCHES %@", dateRegEx)
        return dateTest.evaluate(with:testStr)
    }
}
extension UIViewController {
    func ValidateData(strMessage: String)
    {
        let alert = UIAlertController(title: "Nutshell", message: strMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

func validateURL(urlString: String) -> Bool {
    let urlRegEx = "(https?://(www.)?)?[-a-zA-Z0-9@:%._+~#=]{2,256}.[a-z]{2,6}b([-a-zA-Z0-9@:%_+.~#?&//=]*)"
    let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
    return urlTest.evaluate(with: urlString)
}
extension String {
    var isBlankByTrimming: Bool {
        let trimmed = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    func trimWhitespacesAndNewlines() -> String{
        return self.trimmingCharacters(in:
            CharacterSet.whitespacesAndNewlines)
    }
    
}
