//
//  CommentsVC.swift
//  Nutshell
//
//  Created by apple on 03/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
class CommentsTableViewCell: UITableViewCell{
    @IBOutlet weak var threedotsBtn: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var profileDetailsBtn: UIButton!
    @IBOutlet weak var threeDotImg: UIImageView!
    
}
class CommentsVC: UIViewController {
    var postId = ""
    var isFrom = ""
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var commentTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var noCommentsFoundLbl: UILabel!
    
    var commentsArr = [CommentsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentsTableView.delegate = self
        self.commentsTableView.dataSource = self
        self.commentTextView.delegate = self
        commentsList()
        
    }
    func commentsList(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetAllComments
        let parms : [String:Any] = ["user_id":user_ID, "post_id": postId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let img = response["photo"] as? String ?? ""
            self.profilePic.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "img1"))
            print(status)
            self.commentsArr.removeAll()
            if status == 1{
                if let commentData = response["comment_data"] as? [[String:Any]]{
                    for i in 0..<commentData.count{
                        self.commentsArr.append(CommentsModel(comment: commentData[i]["comment"] as? String ?? "", comment_by: commentData[i]["comment_by"] as? String ?? "", comment_id: commentData[i]["comment_id"] as? String ?? "", created_at: commentData[i]["created_at"] as? String ?? "", name: commentData[i]["username"] as? String ?? "", photo: commentData[i]["photo"] as? String ?? "", post_id: commentData[i]["post_id"] as? String ?? ""))
                    }
                }
            }else{
               // alert(Constant.shared.AppName, message: msg, view: self)
            }
            self.noCommentsFoundLbl.isHidden = self.commentsArr.count == 0 ? false : true
            self.commentsTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        if isFrom == "push"{
            AFWrapperClass.redirectToTabNavRVC(currentVC: self)
            
        }else{
        self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func postBtnAction(_ sender: UIButton) {
        let trimmedString = commentTextView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString != ""{
            commentTextView.resignFirstResponder()
            self.addComment()
            commentTextView.text = ""
            self.commentTextViewHeight.constant = 45
        }
        
    }
    // MARK: --- AddComment
    func addComment(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.AddComment
        let parms : [String:Any] = ["comment_by":user_ID,"post_id":postId,"comment":commentTextView.text ?? ""]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                self.commentsList()
               
            }else{
                alert(Constant.shared.AppName, message: message, view: self)
            }
            self.commentsTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
extension CommentsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CommentsTableViewCell
        cell.userImg.sd_setImage(with: URL(string: commentsArr[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
        cell.selectionStyle = .none
        cell.lblUserName.text = commentsArr[indexPath.row].name
        cell.lblComment.text = commentsArr[indexPath.row].comment
        
        let unixtimeInterval = commentsArr[indexPath.row].created_at
        let date = Date(timeIntervalSince1970:  unixtimeInterval.doubleValue)
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = NSLocale.current
        if compareDate(date1: now, date2: date){
            dateFormatter.dateFormat = "hh:mm a"
        }else{
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
        }
        let strDate = dateFormatter.string(from: date)
        print(strDate,"date")
        cell.lblTime.text = strDate
        cell.threedotsBtn.tag = indexPath.row
        cell.threedotsBtn.addTarget(self, action: #selector(threedotsBtnAction(sender:)), for: .touchUpInside)
        cell.profileDetailsBtn.tag = indexPath.row
        cell.profileDetailsBtn.addTarget(self, action: #selector(profileDetailsBtnAction(sender:)), for: .touchUpInside)
            
        return cell
    }
    func compareDate(date1:Date, date2:Date) -> Bool {
        let order = NSCalendar.current.compare(date1, to: date2, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    @objc func threedotsBtnAction(sender : UIButton) {
        let alert = UIAlertController(title: Constant.shared.AppName, message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7959716916, blue: 0.0269425828, alpha: 1))
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        if user_ID == commentsArr[sender.tag].comment_by{
            alert.addAction(UIAlertAction(title: "Delete this Comment", style: .default, handler: { _ in
                let alert = UIAlertController(title: Constant.shared.AppName, message: "Are you sure want to Delete this Comment?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                    self.deleteCpmmentMethod(commentId: self.commentsArr[sender.tag].comment_id)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in
                }))
                self.present(alert, animated: true)
            }))
        }else{
            alert.addAction(UIAlertAction(title: "Report this Comment", style: .default, handler: { _ in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportUserVC") as! ReportUserVC
                vc.reportId = self.commentsArr[sender.tag].comment_id
                vc.reportType = "1"
                self.navigationController?.pushViewController(vc, animated: true)
                
            }))
            alert.addAction(UIAlertAction(title: "Block the user", style: .default, handler: { _ in
                let alert = UIAlertController(title: "Block", message: "Are you sure want to Block this User?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                    self.blockUserMethod(blockedId: self.commentsArr[sender.tag].comment_by)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in
                }))
                self.present(alert, animated: true)
            }))
        }
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func blockUserMethod(blockedId : String){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.BlockUser
        let parms : [String:Any] = ["user_id":user_ID,"blocked_id":blockedId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                showAlertMessage(title: Constant.shared.AppName, message: message, okButton: "OK", controller: self) {
                    self.commentsList()
                }
            }else{
                alert(Constant.shared.AppName, message: message, view: self)
            }
            self.commentsTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
        
    }
    func deleteCpmmentMethod(commentId : String){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.DeleteComment
        let parms : [String:Any] = ["user_id":user_ID,"comment_id":commentId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                self.commentsList()
            }else{
                alert(Constant.shared.AppName, message: message, view: self)
            }
            self.commentsTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
        
    }
    @objc func profileDetailsBtnAction(sender : UIButton) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        if user_ID != commentsArr[sender.tag].comment_by{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            vc.User_ids = commentsArr[sender.tag].comment_by
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension CommentsVC : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height <= 45 {
            commentTextViewHeight.constant = 45
        }else if textView.contentSize.height <= 100 && textView.contentSize.height > 45 {
            commentTextViewHeight.constant = textView.contentSize.height
        }else{
            commentTextViewHeight.constant = 100
        }
    }
}
