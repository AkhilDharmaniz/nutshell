//
//  NotificationTableVwCell.swift
//  Chips App
//
//  Created by Vivek Dharmani on 02/09/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import UIKit

class NotificationTableVwCell: UITableViewCell {
  
     var AcceptBTN:(()->Void)?
     var RejectBTN:(()->Void)?
    @IBOutlet weak var followVw: UIView!
    @IBOutlet weak var NtfctionMessage: UILabel!
    @IBOutlet weak var NtfctionTitle: UILabel!
    @IBOutlet weak var NtfctionImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func followBtnClicked(_ sender: UIButton) {
         AcceptBTN!()
    }
    
    @IBAction func rejectBtnClicked(_ sender: UIButton) {
        RejectBTN!()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
