//
//  ScanScreenVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.

import UIKit

import MercariQRScanner

class ScanScreenVC: UIViewController {
    
    var urlTxt = ""
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblScan: UILabel!
    @IBOutlet weak var lblMyCode: UILabel!
    @IBOutlet weak var mycodeVw: UIView!
    @IBOutlet weak var qrScanVw: QRScannerView!
    @IBOutlet weak var scanVw: UIView!
    @IBOutlet weak var ImgQrCode: UIImageView!
    var screenUserProfile = [ScanUserProfileModel]()
    var imagePicker =  UIImagePickerController()
    var qrImageData: UIImage = UIImage()
    
    override func viewDidLoad() {
    super.viewDidLoad()
    }
    override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    qrScanVw.stopRunning()
    }
    
    @IBAction func save_Btn(_ sender :UIButton){
    let layer = ImgQrCode.toImage(layer: ImgQrCode)
    UIImageWriteToSavedPhotosAlbum(layer, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    DispatchQueue.main.async {
    self.qrScanVw.focusImage = UIImage(named: "scanoutline")
    self.qrScanVw.focusImagePadding = 8.0
    self.qrScanVw.animationDuration = 0.5
    self.qrScanVw.configure(delegate: self)
    self.qrScanVw.startRunning()
    }
    self.imagePicker.delegate = self
        
    }
    
    // MARK:--- User Details
    func getData(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.ScanQrProfileAPi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue]
        AFWrapperClass.requestUrlEncodedPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            //  let result = response
            IJProgressView.shared.hideProgressView()
            if let details = response["userprofileDetails"] as? [[String:Any]]{
                print(details,"mmm")
                    details.forEach { (result) in
                    self.lblUserName.text = result["username"] as? String ?? ""
                    self.lblUserEmail.text = result["email"] as? String ?? ""
                    self.userImage.sd_setImage(with: URL(string:result["photo"] as? String ?? ""), placeholderImage: UIImage(named: "img1"))
                    let base64String = result["QrCode"] as? String ?? ""
                    print(base64String,"lop")
                        self.qrImageData = self.base64Convert(base64String: base64String)
                        print(self.qrImageData,"lopp")
                        self.ImgQrCode.image = self.qrImageData
                        print(base64String.toBase64(),"opop")
                      
//                    if base64String != "" {
//                        let decodedData = NSData(base64Encoded: base64String, options: [])
//                        if let data = decodedData {
//                            let decodedimage = UIImage(data: data as Data)
//                            print(decodedimage,"iopop")
//                            self.ImgQrCode.image = decodedimage
//                        } else {
//                            print("error with decodedData")
//                        }
//                    } else {
//                        print("error with base64String")
//                    }
                    //  self.ImgQrCode.image = self.convertBase64StringToImage(imageBase64String: image)
                }
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    func base64Convert(base64String: String?) -> UIImage{
       if (base64String?.isEmpty)! {
           return #imageLiteral(resourceName: "P2")
       }else {
           // !!! Separation part is optional, depends on your Base64String !!!
           let temp = base64String?.components(separatedBy: ",")
        let dataDecoded : Data = Data(base64Encoded: temp![1], options: .ignoreUnknownCharacters)!
           let decodedimage = UIImage(data: dataDecoded)
           return decodedimage!
       }
     }
    
//    func convertBase64StringToImage (imageBase64String:String) -> UIImage {
//        let imageData = Data.init(base64Encoded: imageBase64String, options: .init(rawValue: 0))
//        print(imageData,"lip")
//        let image = UIImage(data: imageData!)
//        return image!
//    }
    
    @IBAction func scanBtnClicked(_ sender: UIButton) {
        
    self.qrScanVw.startRunning()
    self.qrScanVw.isHidden = false
    self.lblScan.textColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
    self.lblMyCode.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
    self.scanVw.backgroundColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
    self.mycodeVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        
    }
    
    @IBAction func mycodeBtnClicked(_ sender: UIButton) {
    getData()
    self.qrScanVw.isHidden = true
    qrScanVw.stopRunning()
    self.lblScan.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
    self.lblMyCode.textColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
    self.mycodeVw.backgroundColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
    self.scanVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
}
// MARK: - QRScannerViewDelegate
extension ScanScreenVC: QRScannerViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        // self.navigationController?.popViewController(animated: true)
    }
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
        } else {
        let alert = UIAlertController(title: "Saved!", message: "QR saved successfully", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true)
        }
    }
    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        if code.contains("Nutshell") {
            self.urlTxt = code.replacingOccurrences(of: "Nutshell-", with: "")
            qrScannerView.stopRunning()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            vc.User_ids =  self.urlTxt
            self.navigationController?.pushViewController(vc, animated: true)
            //        if let urls = URL(string: code), (urls.scheme == "http" || urls.scheme == "https") {
            
            //            let alert = UIAlertController(title: self.urlTxt, message: "Sucess", preferredStyle: .alert)
            //                 let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            //                    let qrScannerView = QRScannerView(frame: self.view.bounds)
            //                           // Customize focusImage, focusImagePadding, animationDuration
            //                           qrScannerView.focusImage = UIImage(named: "scanoutline")
            //                        //   qrScannerView.focusImagePadding = 8.0
            //                          // qrScannerView.animationDuration = 0.5
            //                           qrScannerView.configure(delegate: self)
            //                    self.qrScanVw.addSubview(qrScannerView)
            //                           qrScannerView.startRunning()
            //
            //                 })
            //                 alert.addAction(ok)
            //                 let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            //                    self.navigationController?.popViewController(animated: true)
            //
            //                 })
            //                 alert.addAction(cancel)
            //                 DispatchQueue.main.async(execute: {
            //                    self.present(alert, animated: true)
            //            })
            
        } else {
        self.qrScanVw.rescan()
        Timer.scheduledTimer(withTimeInterval: 0, repeats: false) { (_) in
        self.qrScanVw.stopRunning()
            }
        DispatchQueue.main.async {
        let alertController = UIAlertController(title: "Nutshell", message:"Seems like this code is not valid. Try scanning another code", preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    self.qrScanVw.startRunning()
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
           
            //            self.qrScanVw.focusImage = UIImage(named: "scanoutline")
            //            self.qrScanVw.focusImagePadding = 8.0
            //            self.qrScanVw.animationDuration = 0.5
            //            self.qrScanVw.configure(delegate: self)
            //            //        qrScanVw.addSubview(qrScannerView)
            
            //            let qrScannerView = QRScannerView(frame: view.bounds)
            
        }
    }
}


extension ScanScreenVC{
    
    func showAlert(code: String) {
        let alertController = UIAlertController(title: code, message: nil, preferredStyle: .actionSheet)
        let copyAction = UIAlertAction(title: "Copy", style: .default) { [weak self] _ in
            UIPasteboard.general.string = code
            self?.navigationController?.popViewController(animated: true)
        }
        
        alertController.addAction(copyAction)
        let searchWebAction = UIAlertAction(title: "Search Web", style: .default) { [weak self] _ in
            UIApplication.shared.open(URL(string: "https://www.google.com/search?q=\(code)")!, options: [:], completionHandler: nil)
        }
        alertController.addAction(searchWebAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}
extension String {
    func base64ToImage() -> UIImage? {
        if let url = URL(string: self),let data = try? Data(contentsOf: url),let image = UIImage(data: data) {
            return image
        }
        return nil
    }
}
extension String {
    func fromBase64() -> String? {
                guard let data = Data(base64Encoded: self) else {
                        return nil
                }
                return String(data: data, encoding: .utf8)
        }
    func toBase64() -> String {
                return Data(self.utf8).base64EncodedString()
        }
}

//UIView extension which converts the UIView into an image.
extension UIView {
    func toImage(layer: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, 3)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}
