//
//  SettingsVC.swift
//  Nutshell
//
//  Created by Dharmani Apps on 12/06/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//

import UIKit
import SafariServices

class SettingsVC: UIViewController {
    var message = String()
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var privateOrPublicSwitch: UISwitch!
    var movetoroot = Bool()
    var SName = ""
    var SEmail = ""
    var S_ProfileImg = ""
    var S_Role = ""
    var descriptionTxt = ""
    var statusPublic = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //  self.getData()
        self.lblUserName.text = SName
        self.lblUserEmail.text = SEmail
        self.userProfileImage.sd_setImage(with: URL(string: S_ProfileImg), placeholderImage: UIImage(named: "img1"))
    }
    @IBAction func editBtnClicked(_ sender: UIButton) {
        //    let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        //    self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        //  self.getData()
        getData()
    }
    // MARK:--- User Details
    func getData(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.getUserDetails
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let result = response
            let UserStatus = result.value(forKeyPath: "data.status") as? String ?? ""
            print(UserStatus,"Roleeeee")
            if UserStatus == "0"{
                self.privateOrPublicSwitch.isOn = true
            }else{
                self.privateOrPublicSwitch.isOn = false
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if movetoroot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func changePasswordBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func aboutBtnClicked(_ sender: UIButton) {
        if let url = URL(string:Constant.shared.websiteUrl)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
        }
    }
    @IBAction func termsandCondtionBtnClicked(_ sender: UIButton) {
        if let url = URL(string:Constant.shared.websiteUrl + Constant.shared.TermsConditionsLink)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
        }
    }
    @IBAction func privacyPolicyBtnClicked(_ sender: UIButton) {
        if let url = URL(string:Constant.shared.websiteUrl + Constant.shared.PrivacyLink)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
        }
    }
    
    @IBAction func switchAccountBtnClicked(_ sender: UISwitch) {
        if (sender.isOn == true) {
            PrivacyAccount(status:"0")
        }else {
            PrivacyAccount(status:"1")
        }
    }
    @IBAction func contactUsBtnClicked(_ sender: UIButton) {
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func logOutBtnClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Nutshell", message: "Are you sure you want to logout?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            self.LogOutApi()
            //  UserDefaults.standard.set(false, forKey: "ISUSERLOGGEDIN")
            // var profilePic = ""
            //  UserDefaults.standard.set(profilePic, forKey: "userProfilePic")
            // let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
            // self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
            
        }))
        //        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        //        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func PrivacyAccount(status:String){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.PrivatePublic
        let idValue = UserDefaults.standard.value(forKey: "Uid") ?? ""
        let parms:[String:Any] = ["user_id":idValue,"status":status]
        print(parms)
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let result = response
            let status = result["status"] as! Int ?? 0
            print(status)
            self.message = result["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                alert("Nutshell", message: self.message, view: self)
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    // MARK: --- LogOut
    func LogOutApi(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let forumData = Constant.shared.baseUrl + Constant.shared.logOutApi
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(forumData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                appDelegate().logOutSuccess()
            }else{
                alert("Nutshell", message: self.message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
}
