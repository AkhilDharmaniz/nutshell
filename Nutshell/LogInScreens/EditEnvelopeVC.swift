//
//  EditEnvelopeVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 12/05/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class EditEnvelopeVC: UIViewController,UITextFieldDelegate,UITextViewDelegate {
    
    var envelopeid = ""
    var name = ""
    var links = ""
    var dates = ""
    var descrp = ""
    @IBOutlet weak var txtVwDescp: UITextView!
    @IBOutlet weak var txtExpireDate: UITextField!
    @IBOutlet weak var txtLinks: UITextField!
    @IBOutlet weak var txtName: UITextField!
    var movetoroot = Bool()
    var message = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.delegate = self
        txtName.text = name
        txtLinks.text = links
        txtVwDescp.text = descrp
        txtExpireDate.text = dates
        txtExpireDate.delegate = self
        txtExpireDate.addTarget(self, action: #selector(didEndediting(_:)), for: .allEvents)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtName{
            if range.location == 0 && string == " "
            {
                return false
            }
            return true
        }
        else{
        }
        return true
    }
    func EditEnvelope(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.editEnvelope
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["id":envelopeid,"user_id":idValue,"name":txtName.text ?? "", "link":txtLinks.text ?? "","description":txtVwDescp.text ?? "","expiryDate":txtExpireDate.text ?? ""]
        print(parms,"ooooooo")
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            self.message = response["message"] as? String ?? ""
            print(self.message)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: "Envelope Updated successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
            IJProgressView.shared.hideProgressView()
        }) { (Error) in
        }
        
        
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if movetoroot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if  (txtName.text?.isEmpty)!{
            ValidateData(strMessage: "Please enter Envelope Name")
        }
        else if txtLinks.text == "" {
            alert("Nutshell", message: "Please add link", view: self)
            print("empty")
        }
            
        else   if txtLinks.text != "" && verifyUrl(urlString:txtLinks.text) == false{
            alert("Nutshell", message: "Please add Valid link", view: self)
        }else if (txtExpireDate.text?.isEmpty)!{
            ValidateData(strMessage: " Please Add Expiry Date")
        }
        else if (txtVwDescp.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter description")
        }
        else{
            self.EditEnvelope()
        }
        
    }
    
    @objc func didEndediting(_ textField :UITextField!){
        if textField == txtExpireDate {
            RPicker.selectDate(title: "Select Date & Time", cancelText: "Cancel", datePickerMode: .dateAndTime, minDate: Date(), maxDate: Date().dateByAddingYears(5), didSelectDate: {[weak self] (selectedDate) in
                guard let self = self else {return}
                self.txtExpireDate.text = selectedDate.dateString()
            })
        }
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
}
