//
//  ForgetPasswordVC.swift
//  Nutshell
//
//  Created by Dharmani Apps on 12/06/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//

import UIKit

class ForgetPasswordVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var NNTBtn: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var selectedOneVw: UIView!
    var movetoroot = Bool()
    var message = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        NNTBtn.isUserInteractionEnabled = false
        txtEmail.delegate = self
        // Do any additional setup after loading the view.
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    @IBAction func backBtnClicked(_ sender: UIButton) {
        if movetoroot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            self.selectedOneVw.backgroundColor = #colorLiteral(red: 0.3412276506, green: 0.7920924425, blue: 0.7633551955, alpha: 1)
        default:break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case txtEmail:
            self.selectedOneVw.backgroundColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        default:break
        }
    }
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if (txtEmail.text?.isEmpty)!{
            ValidateData(strMessage: " Please enter email address")
        }
        else if isValidEmail(testStr: (txtEmail.text)!) == false{
            ValidateData(strMessage: "Enter valid email")
        }else{
            getData()
        }
    }
    func getData() {
        IJProgressView.shared.showProgressView()
        let signUpUrl = Constant.shared.baseUrl + Constant.shared.forgetPasswordUrl
        let parms : [String:Any] = ["email": txtEmail.text!]
        AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { (dict) in
            IJProgressView.shared.hideProgressView()
            let result = dict as AnyObject
            print(result)
            self.message = result["message"] as? String ?? ""
            let status = result["status"] as? Int ?? 0
            if status == 0{
                UserDefaults.standard.set(false, forKey: "Uid")
                alert("Nutshell", message: self.message, view: self)
                
            }else{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: self.message, preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                // alert("Limitless Nights", message: self.message, view: self)
                UserDefaults.standard.set(true, forKey: "uid")
                //                  let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                //                  self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
        }
    }
}
