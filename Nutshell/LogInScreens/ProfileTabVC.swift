//
//  ProfileTabVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//
import UIKit
import SDWebImage
import SafariServices
class ProfileTabVC: UIViewController {
    @IBOutlet weak var followingBtn: UIButton!
    @IBOutlet weak var followersBtn: UIButton!
    var sponserKey = ""
    var urlString = ""
    @IBOutlet weak var lblFollowing: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    var links = ""
    var dates = ""
    var descrp = ""
    var EnvelopeID = ""
    var locationName = ""
    var followerCount = ""
    var followingCount = ""
    @IBOutlet weak var LblNoData: UILabel!
    @IBOutlet weak var boostProfileBtn: UIButton!
    var linkarray = [String]()
    // @IBOutlet weak var noDataVw: UIView!
    @IBOutlet weak var linkNoDataFound: UIView!
    @IBOutlet weak var lblBio: UILabel!
    // @IBOutlet weak var linkaddBtn: UIButton!
    @IBOutlet weak var lblNameOnProfileTab: UILabel!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var UserImage: UIImageView!
    // @IBOutlet weak var envelopeAddBtn: UIButton!
    @IBOutlet weak var envelopeVw: UIView!
    var phonepe = ""
    var rapido = ""
    var skype = ""
    var google = ""
    var googlepay = ""
    var instagram = ""
    var tiktok = ""
    var snackVideo = ""
    var message = String()
    var EnvelopeSelectedArray = [EnvelopebleEnaDetailModel]()
    var GetLinksArray = [GetLinksModel]()
    var Name = String()
    var idArrays = [String]()
    var MainArrys = [String]()
    var ProfileImage = String()
    var Header = ""
    var SName = ""
    var suserName = ""
    var SEmail = ""
    var S_ProfileImg = ""
    var S_Role = ""
    var descriptionTxt = ""
    var UserRole = ""
    var LinkIds = String()
    var User_id = ""
    // var Clicks = ["0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks","0 Clicks"]
    var Envelope = ["FOOD ENVELOPE","SPORTS ENVELOPE","DANCE ENVELOPE"]
    var EnvelopeProfileIMg = ["fooed-enve","sports-enve","dance-enve","dance-enve"]
    @IBOutlet weak var linkTableVw: UITableView!
    // @IBOutlet weak var addLinksHideVw: UIView!
    // @IBOutlet weak var addLinksVw: UIView!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblEnvelope: UILabel!
    @IBOutlet weak var linkSelectedVw: UIView!
    @IBOutlet weak var envelopeselectedVw: UIView!
    @IBOutlet weak var envelopeTableVw: UITableView!
    @IBOutlet weak var linkBtn: UIButton!
    @IBOutlet weak var enveplopeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("111", forKey: "EN")
        linkTableVw.reorder.delegate = self
        // self.noDataVw.isHidden = true
        self.linkNoDataFound.isHidden = true
        //addLinksVw.isHidden = true
        // addLinksHideVw.isHidden = true
        //        envelopeVw.isHidden == true
        linkTableVw.rowHeight = UITableView.automaticDimension
        linkTableVw.estimatedRowHeight = 95
        envelopeTableVw.rowHeight = UITableView.automaticDimension
        envelopeTableVw.estimatedRowHeight = 75
        //    var tapGesture = UITapGestureRecognizer()
        //    self.hideKeyboardTappedAround()
        //    tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(tap:)))
        //    tapGesture.numberOfTapsRequired = 1
        // addLinksHideVw.addGestureRecognizer(tapGesture)
    }
    
    // MARK:--- User Details
    func getData(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.getUserDetails
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue]
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let result = response
            let names = result.value(forKeyPath: "data.name") as? String ?? "user"
            self.SName = names
            let userName = result.value(forKeyPath: "data.username") as? String ?? "user"
            self.suserName = userName
            let User_ID = result.value(forKeyPath: "data.user_id") as? String ?? "user"
            print(User_ID,"userrrr")
            self.User_id = User_ID
            let follwers = result.value(forKeyPath: "Followers") as? String ?? ""
            self.lblFollowers.text = follwers
            self.followerCount = follwers
            print(self.followerCount,"fffff")
            let following = result.value(forKeyPath: "Followings") as? String ?? ""
            self.lblFollowing.text = following
            self.followingCount = following
            let useremail = result.value(forKeyPath: "data.email") as? String ?? "user@gmail.com"
            self.SEmail = useremail
            let profilePic = result.value(forKeyPath: "data.photo") as? String ?? ""
            self.S_ProfileImg = profilePic
            let desrp = result.value(forKeyPath: "data.description") as? String ?? ""
            print(desrp)
            self.descriptionTxt = desrp
            UserDefaults.standard.set(profilePic, forKey: "userProfilePic")
            let UserRole = result.value(forKeyPath: "data.role") as? String ?? ""
            print(UserRole,"Roleeeee")
            let LocationName = result.value(forKeyPath: "data.location") as? String ?? ""
            print(LocationName,"Location")
            self.locationName = LocationName
            let sponsredkey = result.value(forKeyPath: "data.sponsored_key") as? String ?? ""
            self.sponserKey = sponsredkey
            print(sponsredkey,"spon")
            if sponsredkey == "1" {
                self.boostProfileBtn.setTitle("Manage Boost Profile", for: .normal )
            }else {
                self.boostProfileBtn.setTitle("Boost Profile", for: .normal )
            }
            let UserStatus = result.value(forKeyPath: "data.status") as? String ?? ""
            print(UserStatus,"statts")
            let Url = result.value(forKeyPath: "data.url") as? String ?? ""
            let nn = Url + User_ID
            print(nn,"loppp")
            self.urlString = nn
            self.UserRole = UserRole
            print(userName,useremail,profilePic,desrp)
            let passwords = UserDefaults.standard.value(forKey: "oldPassword") as? String ?? "1"
            self.lblNameOnProfileTab.text = userName
            self.lblRole.text = LocationName
            self.lblBio.text = desrp
            self.UserImage.sd_setImage(with: URL(string: profilePic), placeholderImage: UIImage(named: "img1"))
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func shareWith(){
        AFWrapperClass.presentShare(objectsToShare: [urlString], vc: self)
        
    }
    func postshareLink(){
        shareWith()
    }
    
    func GetSelectedEnvelope() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.EnvelopeEnableDetailsApi
        let parms : [String:Any] = ["user_id":user_ID,"otheruserid":""]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            // self.GetLinksApi()
            //            let Counts = response["count"] as? Int ?? 0
            //            print(Counts,"lll")
            //  let message = response["message"] as? String ?? ""
            if status == 1{
                //  self.noDataVw.isHidden = true
                self.linkNoDataFound.isHidden = true
                if let details = response["envelope_details"] as? [[String:Any]]{
                    print(details,"lll")
                    self.EnvelopeSelectedArray.removeAll()
                    for i in 0..<details.count {
                        self.EnvelopeSelectedArray.append(EnvelopebleEnaDetailModel(id: details[i]["id"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", name: details[i]["name"] as? String ?? "", link: details[i]["link"] as? String ?? "", description: details[i]["description"] as? String ?? "", enable: details[i]["enable"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? "", expirylink: details[i]["expirylink"] as? String ?? ""))
                    }
                }
            }else{
                self.EnvelopeSelectedArray.removeAll()
                self.linkNoDataFound.isHidden = false
                self.LblNoData.text = "No Envelopes Found"
                // self.noDataVw.isHidden = false
            }
            self.envelopeTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    func GetLinksApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetLinksListing
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            
            //            let Counts = response["count"] as? Int ?? 0
            //            print(Counts,"lll")
            //  let message = response["message"] as? String ?? ""
            if status == 1{
                self.linkNoDataFound.isHidden = true
                
                //   self.noDataVw.isHidden = true
                
                if let details = response["data"] as? [[String:Any]]{
                    print(details,"lll")
                    self.GetLinksArray.removeAll()
                    
                    for i in 0..<details.count {
                        self.GetLinksArray.append(GetLinksModel(social_id: details[i]["social_id"] as? String ?? "", title: details[i]["title"] as? String ?? "", imageurl: details[i]["imageurl"] as? String ?? "", user_id: details[i]["user_id"] as? String ?? "", sort_id: details[i]["sort_id"] as? String ?? "", linkurl: details[i]["linkurl"] as? String ?? "", create_date: details[i]["create_date"] as? String ?? "", linkcount: details[i]["linkcount"] as? String ?? "", link_id: details[i]["link_id"] as? String ?? ""))
                    }
                    
                }
            }else{
                self.linkNoDataFound.isHidden = false
                self.LblNoData.text = "No Links Found"
                self.GetLinksArray.removeAll()
            }
            self.linkTableVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    func UpdateLinkApi(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.UpdateLinkApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        let parms:[String:Any] = ["user_id":idValue,"sort_id":idArrays,"link_id":MainArrys]
        print(parms)
        AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            if status == 1{
                print("updated.....")
                
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
            IJProgressView.shared.hideProgressView()
        }) { (Error) in
        }
    }
    
    //    @objc func tapAction(tap:UITapGestureRecognizer){
    //        addLinksVw.isHidden = true
    //        addLinksHideVw.isHidden = true
    //    }
    
    @IBAction func envelopeBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set("222", forKey: "EN")
        self.GetSelectedEnvelope()
        self.envelopeVw.isHidden = false
        self.lblEnvelope.textColor = Constant.shared.appcolor
        self.lblLink.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        self.envelopeselectedVw.backgroundColor = Constant.shared.appcolor
        self.linkSelectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    @IBAction func linkBtnClicked(_ sender: UIButton) {
        UserDefaults.standard.set("111", forKey: "EN")
        self.GetLinksApi()
        self.envelopeVw.isHidden = true
        self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        self.lblLink.textColor = Constant.shared.appcolor
        self.linkSelectedVw.backgroundColor = Constant.shared.appcolor
        self.envelopeselectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
    }
    @IBAction func shareBtnClicked(_ sender: UIButton) {
        postshareLink()
    }
    @IBAction func settingsBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        vc.SName = suserName
        vc.SEmail = SEmail
        vc.S_ProfileImg = S_ProfileImg
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        // self.GetSelectedEnvelope()
        self.getData()
        
        let Check = UserDefaults.standard.value(forKey: "EN") as? String ?? ""
        if Check == "222" {
            self.GetSelectedEnvelope()
            self.envelopeVw.isHidden = false
            self.lblEnvelope.textColor = Constant.shared.appcolor
            self.lblLink.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
            self.envelopeselectedVw.backgroundColor = Constant.shared.appcolor
            self.linkSelectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        }else {
            self.GetLinksApi()
            self.envelopeVw.isHidden = true
            self.lblEnvelope.textColor = .init(cgColor: #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
            self.lblLink.textColor = Constant.shared.appcolor
            self.linkSelectedVw.backgroundColor = Constant.shared.appcolor
            self.envelopeselectedVw.backgroundColor = .init(cgColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        }
        //        self.GetLinksApi()self.GetSelectedEnvelope()
        
    }
    @IBAction func addLinksBtnClicked(_ sender: UIButton) {
        let Check = UserDefaults.standard.value(forKey: "EN") as? String ?? ""
        if Check == "222" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEnvelopeVC") as! AddEnvelopeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddLinkVC") as! AddLinkVC
            vc.followerCount = followerCount
            vc.followingCount = followingCount
            vc.C_ProfileImg = S_ProfileImg
            vc.phonepe = phonepe
            vc.rapido = rapido
            vc.skype = skype
            vc.google = google
            vc.googlepay = googlepay
            vc.instagram = instagram
            vc.tiktok = tiktok
            vc.snackVideo = snackVideo
            self.navigationController?.pushViewController(vc, animated: true)
        }
        //        addLinksVw.isHidden = false
        //        addLinksHideVw.isHidden = false
    }
    @IBAction func linkaddBtnClicked(_ sender: UIButton) {
        //        addLinksVw.isHidden = true
        //        addLinksHideVw.isHidden = true
        //        self.linkaddBtn.backgroundColor = .init(cgColor: #colorLiteral(red: 0.1039308235, green: 0.4328777194, blue: 0.4311453104, alpha: 1))
        //        self.envelopeAddBtn.backgroundColor = .init(cgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        //        self.linkaddBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        //        self.envelopeAddBtn.setTitleColor(#colorLiteral(red: 0.1039308235, green: 0.4328777194, blue: 0.4311453104, alpha: 1), for: .normal)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddLinkVC") as! AddLinkVC
        vc.followerCount = followerCount
        vc.followingCount = followingCount
        vc.C_ProfileImg = S_ProfileImg
        vc.phonepe = phonepe
        vc.rapido = rapido
        vc.skype = skype
        vc.google = google
        vc.googlepay = googlepay
        vc.instagram = instagram
        vc.tiktok = tiktok
        vc.snackVideo = snackVideo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func followersBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendsListVC") as! FriendsListVC
        vc.followType = "1"
        vc.OtherProfileId = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        vc.navTitle = "Followers"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func followingBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendsListVC") as! FriendsListVC
        vc.followType = "2"
        vc.OtherProfileId = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        vc.navTitle = "Following"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func EnvelopeAddBtnClicked(_ sender: UIButton) {
        //        addLinksVw.isHidden = true
        //        addLinksHideVw.isHidden = true
        //        self.envelopeAddBtn.backgroundColor = .init(cgColor: #colorLiteral(red: 0.1039308235, green: 0.4328777194, blue: 0.4311453104, alpha: 1))
        //        self.linkaddBtn.backgroundColor = .init(cgColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        //        self.envelopeAddBtn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        //        self.linkaddBtn.setTitleColor(#colorLiteral(red: 0.1039308235, green: 0.4328777194, blue: 0.4311453104, alpha: 1), for: .normal)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddEnvelopeVC") as! AddEnvelopeVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func editProfileBtnClicked(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        vc.closure = {
            self.getData()
        }
        vc.SName = SName
        vc.SEmail = SEmail
        vc.suserName = suserName
        vc.S_ProfileImg = S_ProfileImg
        vc.descriptionTxt = descriptionTxt
        vc.locationName = locationName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func boostProfileBtnClicked(_ sender: UIButton) {
        if sponserKey == "1" {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AllStatisticsVC") as! AllStatisticsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BoostProfileVC") as! BoostProfileVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}

extension ProfileTabVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == linkTableVw {
            return GetLinksArray.count
        } else {
            print(EnvelopeSelectedArray.count,"ee")
            return EnvelopeSelectedArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == linkTableVw {
            if let cell2 = linkTableVw.dequeueReusableCell(withIdentifier: "linkTableVwCell", for: indexPath) as? linkTableVwCell {
                if let spacer = tableView.reorder.spacerCell(for: indexPath) {
                    return spacer
                }
                cell2.lblLinkProfileName.text = GetLinksArray[indexPath.row].title
                //  cell2.linkProfileImg.image = UIImage(named: "map")
                cell2.linkProfileImg.sd_setImage(with: URL(string: GetLinksArray[indexPath.row].imageurl), placeholderImage: UIImage(named: ""))
                let img = GetLinksArray[indexPath.row].imageurl
                
                if img == "" {
                    cell2.lblFirstLetter.isHidden = false
                }else {
                    cell2.lblFirstLetter.isHidden = true
                }
                
                var firstLetter = self.GetLinksArray[indexPath.row].title.first?.description ?? ""
                print(firstLetter,"Data")
                cell2.lblFirstLetter.text = firstLetter
                cell2.lblFirstLetter.backgroundColor = UIColor.random
                cell2.lblLinkClicks.text = GetLinksArray[indexPath.row].linkcount
                let links = GetLinksArray[indexPath.row].linkurl
                
                // cell2.lblReason.text = (cancelArr[indexPath.row] as! String)
                cell2.selectionStyle = .none
                // cell2.lblLinkProfileName.text = GetLinksArray[indexPath.row].url
                
                
                return cell2
            }
        } else {
            let cell = envelopeTableVw.dequeueReusableCell(withIdentifier: "EnvelopeProfileTableVwCell", for: indexPath) as! EnvelopeProfileTableVwCell
            cell.selectionStyle = .none
            let envelopeId = EnvelopeSelectedArray[indexPath.row].id
            self.EnvelopeID = envelopeId
            cell.lblEnvelopeTitle.text = EnvelopeSelectedArray[indexPath.row].name
            var firstLetter = self.EnvelopeSelectedArray[indexPath.row].name.first?.description ?? ""
            print(firstLetter,"Data")
            cell.LblCornerTitle.text = firstLetter
            cell.LblCornerTitle.backgroundColor = UIColor.random
            //  cell.envelopeImg.image = UIImage(named: EnvelopeProfileIMg[indexPath.row] )
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == linkTableVw {
            let links = GetLinksArray[indexPath.row].linkurl
            if links.contains("https") {
                if let url = URL(string:links)
                {
                    let safariCC = SFSafariViewController(url: url)
                    present(safariCC, animated: true, completion: nil)
                }
            }else {
                let links = GetLinksArray[indexPath.row].linkurl
                let wtsap = "https://api.whatsapp.com/send?phone="
                UIApplication.shared.openURL(URL(string:wtsap + links)!)
            }
            
            //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileClicksVC") as! ProfileClicksVC
            //            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnvelopeVC") as! EnvelopeVC
            vc.OtherUSerID = ""
            vc.envelopeId = EnvelopeSelectedArray[indexPath.row].id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .default, title: "Edit") { (action, indexpath) in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditEnvelopeVC") as! EditEnvelopeVC
            
            vc.envelopeid = self.EnvelopeSelectedArray[indexPath.row].id
            vc.name = self.EnvelopeSelectedArray[indexPath.row].name
            vc.links = self.EnvelopeSelectedArray[indexPath.row].link
            vc.descrp = self.EnvelopeSelectedArray[indexPath.row].description
            vc.dates = self.EnvelopeSelectedArray[indexPath.row].expirylink
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return [editAction]
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == linkTableVw {
            return UITableView.automaticDimension
        }else {
            return UITableView.automaticDimension
            // return 70
        }
    }
}
extension ProfileTabVC: TableViewReorderDelegate {
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = GetLinksArray[sourceIndexPath.row]
        GetLinksArray.remove(at: sourceIndexPath.row)
        GetLinksArray.insert(item, at: destinationIndexPath.row)
        //        let nme = GetLinksArray[sourceIndexPath.row].link_id
        //        GetLinksArray.remove(at: sourceIndexPath.row).link_id
        //       // GetLinksArray.insert(nme, at: destinationIndexPath.row)
        //      //  linkarray
        //        self.GetLinksArray.forEach { (linkData) in
        //        DispatchQueue.main.async { [weak self] in
        //        self?.linkarray.append(linkData.id)
        //        debugPrint(self?.linkarray ?? "")
        //        }
        //              }
        //   self.cellsArr.removeAll()
        self.idArrays.removeAll()
        self.MainArrys.removeAll()
        self.GetLinksArray.forEach { (linkData) in
            DispatchQueue.main.async { [weak self] in
                self?.idArrays.append(linkData.sort_id)
                self?.MainArrys.append(linkData.link_id ?? "")
            }
        }
    }
    func tableViewDidFinishReordering(_ tableView: UITableView, from initialSourceIndexPath: IndexPath, to finalDestinationIndexPath: IndexPath) {
        self.UpdateLinkApi()
    }
}
extension String {
    func validateUrl () -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
}
extension UIColor {
    static var random: UIColor {
        // Seed (only once)
        srand48(Int(arc4random()))
        return UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: 1.0)
    }
}
