//
//  HomeFeedVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import ASPVideoPlayer
import AVFoundation
import AVKit
import SDWebImage
import CoreLocation
import ImagePicker
import GoogleMobileAds

class HomeFeedVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MultipleImagePickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var imgVw = Int()
    @IBOutlet weak var statusBgView: UIView!
    @IBOutlet weak var addImageStatus: UIImageView!
    @IBOutlet weak var userStatusImg: UIImageView!
    let secondNetworkURL = URL(string: "http://www.easy-fit.ae/wp-content/uploads/2014/09/WebsiteLoop.mp4")
    @IBOutlet weak var homefeedTableVw: UITableView!
    @IBOutlet weak var widthConst: NSLayoutConstraint!
    @IBOutlet weak var homestatusCollectionVw: UICollectionView!
    var userData = [UserData]()
    var friendListArray = [FriendListModel]()
    var postArray = [PostListData<AnyHashable>]()
    var lat = String()
    var long = String()
    @IBOutlet weak var userStatusIMg: UIImageView!
    @IBOutlet weak var addImgStatus: UIImageView!
    @IBOutlet weak var statusCornerVw: UIView!
    lazy var globalRoomId = String()
    lazy var chatUserId = String()
    lazy var chatUsername = String()
    var User_ids = String()
    var UserOwnStatus = String()
    var message = String()
    var ChnageStaus = String()
    var imgArr = [String]()
    var ratingAvarge = Int()
    var ratingCount = Int()
    var OtherUserId = String()
    var viewID = String()
    var ChatLoginUser = String()
    var ChatLoginUserimage = String()
    var statusimageArray = [UserStatusModel]()
    var imagePicker = UIImagePickerController()
    var imgData = [UIImage]()
    var status_id = String()
    var pickedImage:UIImage?
    @IBOutlet weak var statusProfileBtn: UIButton!
    @IBOutlet weak var homeUserCollectionVw: UICollectionView!
//    var friendListArray = [FriendListModel]()
    var DifferentArray = [FriendListModel]()
    var statusArray = [StatusModel]()
    var newData = [HomeDataModal]()
    var postId = ""
    @IBOutlet var storiesView: UIView!
    private var topStoriesView: IGHomeView?
    private var viewModel: IGHomeViewModel = IGHomeViewModel()
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func loadView() {
        super.loadView()
        topStoriesView = IGHomeView(frame: storiesView.bounds)
        if let topStoriesView = topStoriesView {
            storiesView.addSubview(topStoriesView)
            topStoriesView.collectionView.delegate = self
            topStoriesView.collectionView.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homefeedTableVw.register(UINib(nibName: "AdsTVC", bundle: nil), forCellReuseIdentifier: "AdsTVC")
        homestatusCollectionVw.dataSource = nil
        homestatusCollectionVw.delegate = nil
        
        let localFileURLs = NSFileManager.shared.getUnsavedStories()
        if localFileURLs.count > 0 {
            startUploadingStories(localFileURLs)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GetStatusApi()
        GetStatusApiByUserId()
        getPostApi()
        self.homefeedTableVw.setEmptyMessage("")
        //GetStatusApiByUserId()
    }
    
   
    
    open func getPostApi(){
        IJProgressView.shared.showProgressView()
        let signUpUrl = Constant.shared.baseUrl + Constant.shared.getPostDetailsApi
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        
        let parms : [String:Any] = ["user_id": idValue]
        self.newData.removeAll()
        AFWrapperClass.requestPOSTURL(signUpUrl, params: parms, success: { [self] (dict) in
            IJProgressView.shared.hideProgressView()
            let result = dict as AnyObject
            print(result)
            let message = result["message"] as? String ?? ""
            let status = result["status"] as? Int ?? 0
            
            if status == 1{
                let count = dict["count"] as? Int ?? 0
                print(count,"opop")
                
                //  self.tabBarController?.tabBar.items![2].badgeValue = "100"
                if count == 0 {
                    self.tabBarController?.tabBar.items![2].badgeValue = nil
                }else {
                    self.tabBarController?.tabBar.items![2].badgeValue = "\(count)"
                    self.tabBarController?.tabBar.items![2].badgeColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
                }
                let chatCount =  dict["chatCount"] as? Int ?? 0
                if chatCount == 0 {
                    self.tabBarController?.tabBar.items![3].badgeValue = nil
                }else {
                    self.tabBarController?.tabBar.items![3].badgeValue = "\(chatCount)"
                    self.tabBarController?.tabBar.items![3].badgeColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1))
                }
                
                if let postDetails = dict["post_details"] as? [[String:Any]]{
                    for i in 0..<postDetails.count{
                        self.newData.append(HomeDataModal(user_id: postDetails[i]["user_id"] as? String ?? "", name: postDetails[i]["username"] as? String ?? "", created_at: postDetails[i]["created_at"] as? String ?? "", description: postDetails[i]["description"] as? String ?? "", photo: postDetails[i]["photo"] as? String ?? "", post_id: postDetails[i]["post_id"] as? String ?? "", type: postDetails[i]["type"] as? String ?? "", uploads: postDetails[i]["uploads"] as? String ?? "", video_thumbnail: postDetails[i]["video_thumbnail"] as? String ?? "",likeCount: postDetails[i]["likeCount"] as? String ?? "0",commentCount: postDetails[i]["commentCount"] as? String ?? "0", isLike: postDetails[i]["isLike"] as? String ?? "0", ad: false))
                    }
                    var friendListArr = [HomeDataModal]()
                    var adCount = 4
                    for i in 0..<self.newData.count{
                        if (i % adCount) == 0 && i != 0{
                            print("ok",i)
                            adCount = i+4
                            friendListArr.append(HomeDataModal(user_id: "", name: "", created_at: "", description: "", photo: "", post_id: "", type: "", uploads: "", video_thumbnail: "",likeCount: "",commentCount: "", isLike: "", ad: true))
                            friendListArr.append(self.newData[i])
                        }else{
                            print(i)
                            
                            
                            friendListArr.append(self.newData[i])
                           
                        }
                    }
                    
                    self.newData = friendListArr
                }
                
            }else{
                self.newData.removeAll()
                alert(Constant.shared.AppName, message: message, view: self)
            }
            self.homefeedTableVw.reloadData()
            if self.newData.count == 0{
                self.homefeedTableVw.setEmptyMessage("No Posts Found")
            }
            if newData.count != 0 && postId != ""{
                for i in 0..<self.newData.count{
                   if newData[i].post_id == postId{
                    let indexPath = IndexPath(row: i,
                                              section: 0)
                    self.homefeedTableVw.scrollToRow(at: indexPath, at: .middle, animated: false)
                    }
                }
                postId = ""
            }
        }) { (error) in
        }
    }
    
    func statusInsta(){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.statusUpdateImage
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        if let imageValue = pickedImage{
            let imageData: Data? = imageValue.jpegData(compressionQuality: 0.4)
            let imageStr = imageData?.base64EncodedString(options: .lineLength64Characters) ?? ""
            print(imageStr,"imageString")
            
            let parms:[String:Any] = ["user_id":idValue,"status_images":imageStr]
            AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
                print(response)
                self.GetStatusApi()
                self.GetStatusApiByUserId()
                // self.navigationController?.popViewController(animated: true)
                IJProgressView.shared.hideProgressView()
            }) { (Error) in
            }
        }
    }
    
    func DeleteStory(status:String){
        IJProgressView.shared.showProgressView()
        let getUser = Constant.shared.baseUrl + Constant.shared.DeleteStatus
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
            let parms:[String:Any] = ["user_id":idValue,"status_id":status]
        print(parms)
            AFWrapperClass.requestPOSTURL(getUser, params: parms, success: { (response) in
                print(response)
                self.GetStatusApi()
                self.GetStatusApiByUserId()
                IJProgressView.shared.hideProgressView()
            }) { (Error) in
            }
    }
    
    
    func GetStatusApi() {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetStatusApi
        let parms : [String:Any] = ["user_id":user_ID]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            print(status)
            let result = response
            if status == 1{
                //self.noDataVw.isHidden = true
                let userName = result.value(forKeyPath: "userstatus.user_id") as? String ?? "user"
                let status_id = result.value(forKeyPath: "userstatus.status_id") as? String ?? "user"
                print(status_id,"userrrr")
                self.status_id = status_id
                let profilePic = result.value(forKeyPath: "userstatus.photo") as? String ?? ""
                let img = result.value(forKeyPath: "userstatus.status_image") as? String ?? ""
                print(img,"pppppp")
                self.UserOwnStatus = img
                let statusCount = result.value(forKeyPath: "userstatus.statuscount") as? String ?? ""
                print(statusCount,"countttt")
                let imageSHowVw = result.value(forKeyPath: "userstatus.image_view") as? Int ?? 4
                print(imageSHowVw,"oooooo")
                self.imgVw = imageSHowVw
                self.OtherUserId = userName
                self.viewID = status_id
                if self.UserOwnStatus == "" {
                    self.addImageStatus.isHidden = false
                    self.statusBgView.borderColor = #colorLiteral(red: 0.8675660491, green: 0.9408829808, blue: 1, alpha: 1)
                }else {
                    self.addImageStatus.isHidden = true
                    if imageSHowVw == 0 {
                        self.statusBgView.borderColor = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1)
                    }else {
                        self.statusBgView.borderColor = .lightGray
                    }
                }
                print(self.UserOwnStatus,"lll")
                self.userStatusImg.sd_setImage(with: URL(string: profilePic), placeholderImage: UIImage(named: "img1"))
                if let details = response["userallstatus"] as? [[String:Any]]{
                    print(details,"lll")
                    self.statusArray.removeAll()
                    self.imgArr.removeAll()
                    for i in 0..<details.count {
                        self.statusArray.append(StatusModel(user_id: details[i]["user_id"] as? String ?? "", name: details[i]["username"] as? String ?? "", email: details[i]["email"] as? String ?? "", role: details[i]["role"] as? String ?? "", password: details[i]["password"] as? String ?? "", photo: details[i]["photo"] as? String ?? "", country: details[i]["country"] as? String ?? "", countryCode: details[i]["countryCode"] as? String ?? "", twitterId: details[i]["twitterId"] as? String ?? "", latitude: details[i]["latitude"] as? String ?? "", longitude: details[i]["longitude"] as? String ?? "", description: details[i]["description"] as? String ?? "", verified: details[i]["verified"] as? String ?? "", totalFollowing: details[i]["totalFollowing"] as? String ?? "", totalFollowers: details[i]["totalFollowers"] as? String ?? "", verificateCode: details[i]["verificateCode"] as? String ?? "", created_at: details[i]["created_at"] as? String ?? "", disabled: details[i]["disabled"] as? String ?? "", allowPush: details[i]["allowPush"] as? String ?? "", device_type: details[i]["device_type"] as? String ?? "", device_token: details[i]["device_token"] as? String ?? "", status: details[i]["status"] as? String ?? "", changeStatus: details[i]["changeStatus"] as? String ?? "", status_image: details[i]["status_image"] as? String ?? "", status_id: details[i]["status_id"] as? String ?? "", image_view: details[i]["image_view"] as? Int ?? 0, statusdate: details[i]["statusdate"] as? String ?? ""))
                    }
                }
            }else{
                //   self.noDataVw.isHidden = false
                //  self.EnvelopeListArray.removeAll()
            }
            self.homestatusCollectionVw.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
//    :-----------------------------------------

//    MARK: Button Action
    
    @IBAction func addStatus(_ sender: Any) {
        
        if UserOwnStatus == "" {
            
            /*let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                self.openCamera()
            }))
            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                self.openGallary()
            }))
            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)*/
            
            let imagePickerManager = MultipleImagePickerManager.shared
            imagePickerManager.delegate = self
            imagePickerManager.present(target: self)
            
        } else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "StatusShowVC") as! StatusShowVC
            vc.ViewstatusID = viewID
            let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
            vc.OtherUserId = user_ID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Nutshell", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
         self.pickedImage = tempImage
        if pickedImage != nil{
            self.statusInsta()
        }
        //  ProfileImg.image  = tempImage
        imgData.append(tempImage)
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func openBarCode(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ScanScreenVC") as! ScanScreenVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addPostBtnAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPostVC") as! AddPostVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //------------------------------------------------------
    
    //MARK: Stories
    
    var isUploading: Bool = false
    
    fileprivate func startUploadingStories(_ localFileURLs: [URL]) {
        
        if isUploading { return }
        
        isUploading = true
        
        let localFileURLs = localFileURLs
        let fileName = "status_images[]"
        
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        
        IJProgressView.shared.showProgressView()
        
        let requestURL = Constant.shared.baseUrl + Constant.shared.MultipleStatusImage
        let parms : [String:Any] = ["user_id": user_ID]
        
        AFWrapperClass.multipartFormDataRequestPOSTURL(requestURL, params: parms, fileName: fileName, files: localFileURLs) { (jsonResponse: NSDictionary) in
            
            self.isUploading = false
            
            IJProgressView.shared.hideProgressView()
            
            localFileURLs.forEach { localFile in
                NSFileManager.shared.deleteFilesAt(localFile.path)
            }
            
        } failure: { (error: NSError) in
            
            self.isUploading = false
            
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    func GetStatusApiByUserId() {
       
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.GetStatusApiByUserId
        let parms : [String:Any] = ["user_id": user_ID]
        
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            
            IJProgressView.shared.hideProgressView()
               
            do {
                let jsonStories = (response as Dictionary).toJson
                let igStories = try IGStories(jsonStories)
                self.viewModel = IGHomeViewModel(stories: igStories)
                self.topStoriesView?.collectionView.reloadData()
            } catch let error {
                print(error)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    
    //------------------------------------------------------
    //MARK: MultipleImagePickerDelegate
    func multipleImagePickerManager(imagePicker: ImagePickerController, localFileURLs: [URL]) {
        guard localFileURLs.count > 0 else {
            return
        }
        startUploadingStories(localFileURLs)
    }
    //------------------------------------------------------
    //MARK: UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        DispatchQueue.main.async {
//            if self.widthConst.constant != UIScreen.main.bounds.size.width{
//                self.widthConst.constant = UIScreen.main.bounds.size.width
//            }
//        }
        if indexPath.row == 0 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IGAddStoryCell.reuseIdentifier, for: indexPath) as? IGAddStoryCell else { fatalError() }
            let photo = viewModel.myStories()?.user?.photo ?? String()
           // cell.userDetails = ("Your Story", photo)
           // let staut = viewModel.myStories()?.lastStatusID
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: IGStoryListCell.reuseIdentifier,for: indexPath) as? IGStoryListCell else { fatalError() }
            let story = viewModel.cellForItemAt(indexPath: indexPath)
            cell.story = story
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if Int(self.viewModel.myStories()?.snapsCount ?? String()) ?? .zero > .zero {
                let alert = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "Add Stories", style: .default, handler: { _ in
                    let imagePickerManager = MultipleImagePickerManager.shared
                    imagePickerManager.delegate = self
                    imagePickerManager.present(target: self)
                }))
                alert.addAction(UIAlertAction(title: "View", style: .default, handler: { _ in
                    
                    var myStories = try! IGStories("{}")
                    myStories.stories = self.viewModel.getStories()?.myStories                    
                    let storyPreviewScene = IGStoryPreviewController.init(stories: myStories, handPickedStoryIndex:  indexPath.row-1)
                    storyPreviewScene.modalPresentationStyle = .fullScreen
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }))
                
                alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
                    let alert = UIAlertController(title: "Nutshell", message: "Are you sure you want to Delete Status?", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { _ in
                    self.DeleteStory(status:self.status_id)
                    print("123")
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { _ in
                    }))
                    self.present(alert, animated: true)
                   
                }))
                
                
                alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                let imagePickerManager = MultipleImagePickerManager.shared
                imagePickerManager.delegate = self
                imagePickerManager.present(target: self)
               
            }
            
        } else {
            
            DispatchQueue.main.async {
                if let stories = self.viewModel.getStories() {
                    let storyPreviewScene = IGStoryPreviewController.init(stories: stories, handPickedStoryIndex:  indexPath.row-1)
                    storyPreviewScene.modalPresentationStyle = .fullScreen
                    self.present(storyPreviewScene, animated: true, completion: nil)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return indexPath.row == 0 ? CGSize(width: 100, height: 100) : CGSize(width: 80, height: 100)
    }
    
    //------------------------------------------------------
}

/*extension HomeFeedVC:UICollectionViewDelegate, UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if statusArray.count == 0 {
            self.homestatusCollectionVw.setEmptyMessage("No new updates")
        } else {
            self.homestatusCollectionVw.restore()
        }
        return statusArray.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = homestatusCollectionVw.dequeueReusableCell(withReuseIdentifier: "HomeStatusFeedCollectionCell", for: indexPath) as! HomeStatusFeedCollectionCell
        if indexPath.row == 0 {
            cell.userImg.sd_setImage(with: URL(string: statusArray[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
            if self.UserOwnStatus == "" {
                cell.cornerVw.borderColor = #colorLiteral(red: 0.8675660491, green: 0.9408829808, blue: 1, alpha: 1)
            }else {
                if imgVw == 0 {
                    cell.cornerVw.borderColor = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1)
                }else {
                cell.cornerVw.borderColor = .lightGray
                }
            }
        }
                       
        cell.userImg.sd_setImage(with: URL(string: statusArray[indexPath.row].photo), placeholderImage: UIImage(named: "img1"))
        cell.lblName.text = statusArray[indexPath.row].name
        let imageView = statusArray[indexPath.row].image_view
        print(imageView,"Viewwww")
        let images = statusArray[indexPath.row].status_image
        if images.isEmpty == true {
            cell.cornerVw.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else {
            if imageView == 0 {
                cell.cornerVw.borderColor = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1)
            }else {
                cell.cornerVw.borderColor = .lightGray
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let images = statusArray[indexPath.row].status_image
        if images.isEmpty == true {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            self.User_ids = statusArray[indexPath.row].user_id
            vc.User_ids = User_ids
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AnimationStausVC") as! AnimationStausVC
            vc.StatusArray = statusArray
            vc.otherUserId = statusArray[indexPath.row].user_id
            vc.currentPage = indexPath.row
            vc.viewstatusID = statusArray[indexPath.row].status_id
            self.navigationController?.pushViewController(vc, animated: true)
            
            /*if let stories = self.viewModel.getStories(), let stories_copy = try? stories.copy() {
                let storyPreviewScene = IGStoryPreviewController.init(stories: stories_copy, handPickedStoryIndex:  indexPath.row-1)
                storyPreviewScene.modalPresentationStyle = .fullScreen
                self.present(storyPreviewScene, animated: true, completion: nil)
            }*/
        }
       
    }
}

 extension HomeFeedVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 82, height:130)
        
    }
}
*/

extension HomeFeedVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if newData.count == 0 {
          //  self.homefeedTableVw.setEmptyMessage("No data")
        } else {
            self.homefeedTableVw.restore()
        }
        return newData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if newData[indexPath.row].ad == true{
            let cell1 = homefeedTableVw.dequeueReusableCell(withIdentifier: "AdsTVC", for: indexPath) as! AdsTVC
            cell1.adsShowVW.backgroundColor = .clear
            // MARK : --- AddMob Code
            cell1.adsShowVW.adUnitID = "ca-app-pub-3893356742493576/9405776910"
            // cell.adsVw.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            cell1.adsShowVW.rootViewController = self
            cell1.adsShowVW.adSize = kGADAdSizeSmartBannerPortrait
            cell1.adsShowVW.delegate = self
            print("Size: ",cell1.adsShowVW.bounds)
            cell1.adsShowVW.load(GADRequest())
            return cell1
            // return UITableViewCell()
        }else {
            
            let cell = homefeedTableVw.dequeueReusableCell(withIdentifier: "HomeFeedTableCell", for: indexPath) as! HomeFeedTableCell
            cell.selectionStyle = .none
            cell.lblUserName.text = newData[indexPath.row].name
            cell.btnPlay.tag = indexPath.row
            cell.btnProfileImage.tag = indexPath.row
            cell.userImg.sd_setShowActivityIndicatorView(true)
            if #available(iOS 13.0, *) {
                cell.userImg.sd_setIndicatorStyle(.large)
            } else {
                // Fallback on earlier versions
            }
            cell.userImg.sd_setImage(with: URL(string: newData[indexPath.row].photo), placeholderImage: UIImage(named: "img1"), options: SDWebImageOptions.continueInBackground, completed: nil)
            
            cell.txtVw.text = newData[indexPath.row].description
            cell.btnPlay.addTarget(self, action: #selector(playVideo(sender:)), for: .touchUpInside)
            cell.btnProfileImage.addTarget(self, action: #selector(gotoProfieVC(sender:)), for: .touchUpInside)
            cell.lblFavCount.text = newData[indexPath.row].likeCount
            cell.lblcommentCount.text = newData[indexPath.row].commentCount
            cell.likeBtn.tag = indexPath.row
            cell.likeBtn.addTarget(self, action: #selector(likeUnlikeBtnAction(sender:)), for: .touchUpInside)
            cell.commentBtn.tag = indexPath.row
            cell.commentBtn.addTarget(self, action: #selector(commentBtnAction(sender:)), for: .touchUpInside)
            cell.shareBtn.tag = indexPath.row
            cell.shareBtn.addTarget(self, action: #selector(shareBtnAction(sender:)), for: .touchUpInside)
            let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
            let userId = newData[indexPath.row].user_id
            cell.deleteImg.isHidden = user_ID == userId ? false : true
            cell.deleteBtn.isUserInteractionEnabled = user_ID == userId ? true : false
            cell.threeDotImg.isHidden = user_ID == userId ? true : false
            cell.threeDotBtn.isUserInteractionEnabled = user_ID == userId ? false : true
            cell.threeDotBtn.tag = indexPath.row
            cell.threeDotBtn.addTarget(self, action: #selector(threeDotBtnAction(sender:)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(sender:)), for: .touchUpInside)
            cell.likeImg.image = newData[indexPath.row].isLike == "0" ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "heartTap")
            if newData[indexPath.row].uploads == ""{
                cell.videoVwHeightConstraint.constant = 0
                cell.videoPlayBtn.isHidden = true
            }else{
                cell.videoVwHeightConstraint.constant = 180
                let urls = (URL(string:newData[indexPath.row].uploads) ?? secondNetworkURL)!
                if urls.containsImage == true{
                    cell.videoPlayBtn.isHidden = true
                    cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                    if #available(iOS 13.0, *) {
                        cell.thumbNailImgView.sd_setIndicatorStyle(.large)
                    } else {
                        // Fallback on earlier versions
                    }
                    cell.thumbNailImgView.sd_setImage(with: URL(string: newData[indexPath.row].uploads), placeholderImage: UIImage(named: "placeholderF"), options: SDWebImageOptions.continueInBackground, completed: nil)
                    
                }else{
                    cell.videoPlayBtn.isHidden = false
                    cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                    if #available(iOS 13.0, *) {
                        cell.thumbNailImgView.sd_setIndicatorStyle(.large)
                    } else {
                        // Fallback on earlier versions
                    }
                    cell.thumbNailImgView.sd_setImage(with: URL(string: newData[indexPath.row].video_thumbnail), placeholderImage: UIImage(named: "placeholderF"), options: SDWebImageOptions.continueInBackground, completed: nil)
                }
            }
            let unixtimeInterval = newData[indexPath.row].created_at
            let date = Date(timeIntervalSince1970:  unixtimeInterval.doubleValue)
            let now = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = .current
            dateFormatter.locale = NSLocale.current
            if compareDate(date1: now, date2: date){
                dateFormatter.dateFormat = "hh:mm a"
            }else{
                dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            }
            let strDate = dateFormatter.string(from: date)
            print(strDate,"date")
            cell.timeLbl.text = strDate
            return cell
        }
    }
    
    func compareDate(date1:Date, date2:Date) -> Bool {
        let order = NSCalendar.current.compare(date1, to: date2, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    @objc func playVideo(sender : UIButton) {
        let data = newData[sender.tag]
        if data.uploads.contains(".mp4"){
            let url : String = data.uploads
            let urlStr : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let convertedURL : URL = URL(string: urlStr)!
            print(convertedURL)
            let player = AVPlayer(url: convertedURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true)
            {
                playerViewController.player!.play()
            }
        }else{
            if newData[sender.tag].video_thumbnail == ""{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                vc.imgString = newData[sender.tag].uploads
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @objc func gotoProfieVC(sender : UIButton) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        if user_ID != newData[sender.tag].user_id{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            vc.User_ids = newData[sender.tag].user_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func likeUnlikeBtnAction(sender : UIButton) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.LikeAndUnlike
        let parms : [String:Any] = ["user_id":user_ID, "post_id": newData[sender.tag].post_id]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let msg = response["message"] as? String ?? ""
            print(status)
            if status == 1{
                self.newData[sender.tag].isLike = self.newData[sender.tag].isLike == "0" ? "1" : "0"
                var likeCount = NumberFormatter().number(from: self.newData[sender.tag].likeCount)?.intValue ?? 0
                likeCount =  self.newData[sender.tag].isLike == "1" ? likeCount+1 : likeCount-1
                self.newData[sender.tag].likeCount = "\(likeCount)"
                self.homefeedTableVw.reloadData()
            }else{
                alert(Constant.shared.AppName, message: msg, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
        
    }
    @objc func commentBtnAction(sender : UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
        vc.postId = newData[sender.tag].post_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func shareBtnAction(sender : UIButton) {
        IJProgressView.shared.showProgressView()
        AFWrapperClass.createContentDeepLink(title: Constant.shared.AppName, type: "Post", OtherId: newData[sender.tag].post_id, description: "Hey, look at this post.", image: newData[sender.tag].video_thumbnail == "" ? newData[sender.tag].uploads : newData[sender.tag].video_thumbnail, link: newData[sender.tag].video_thumbnail == "" ? newData[sender.tag].uploads : newData[sender.tag].video_thumbnail) { urlStr in
            IJProgressView.shared.hideProgressView()
            if let url = URL(string: urlStr ?? "") {
                print(urlStr)
                let image = #imageLiteral(resourceName: "Nutshell_logo")
                let objectsToShare = ["Hey, look at this post.", image,url] as [Any]
                AFWrapperClass.presentShare(objectsToShare: objectsToShare, vc: self)
            }
        }
    }
    @objc func deleteBtnAction(sender : UIButton) {
        showMessage(title: Constant.shared.AppName, message: "Are you sure you want to delete Post?", okButton: "OK", cancelButton: "Cancel", controller: self, okHandler: {
            let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
            IJProgressView.shared.showProgressView()
            let CategoryData = Constant.shared.baseUrl + Constant.shared.deletePost
            let parms : [String:Any] = ["user_id":user_ID, "post_id": self.newData[sender.tag].post_id]
            AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
                print(response)
                IJProgressView.shared.hideProgressView()
                let status = response["status"] as? Int ?? 0
                let msg = response["message"] as? String ?? ""
                print(status)
                if status == 1{
                    self.newData.remove(at: sender.tag)
                    self.homefeedTableVw.reloadData()
                }else{
                    alert(Constant.shared.AppName, message: msg, view: self)
                }
            })
            { (error) in
                IJProgressView.shared.hideProgressView()
                print(error)
            }
        }) {}
    }
    @objc func threeDotBtnAction(sender : UIButton) {
        let alert = UIAlertController(title: Constant.shared.AppName, message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = .init(cgColor: #colorLiteral(red: 0, green: 0.7959716916, blue: 0.0269425828, alpha: 1))
        alert.addAction(UIAlertAction(title: "Report this Post", style: .default, handler: { _ in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportUserVC") as! ReportUserVC
            vc.reportId = self.newData[sender.tag].post_id
            vc.reportType = "2"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Unfollow", style: .default, handler: { _ in
            let alert = UIAlertController(title: "Block", message: "Are you sure want to Unfollow this User?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                self.blockUserMethod(blockedId: self.newData[sender.tag].user_id, index: sender.tag)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in
            }))
            self.present(alert, animated: true)
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func blockUserMethod(blockedId : String, index: Int){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.followApi
        let parms : [String:Any] = ["user_id":user_ID,"followUserID":blockedId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            IJProgressView.shared.hideProgressView()
            print(response)
            let status = response["status"] as? Int ?? 0
            print(status)
            let message = response["message"] as? String ?? ""
            if status == 1{
                self.getPostApi()
//                showAlertMessage(title: Constant.shared.AppName, message: message, okButton: "OK", controller: self) {
//                    self.getPostApi()
//                }
            }else{
                alert(Constant.shared.AppName, message: message, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
extension HomeFeedVC: ASPVideoPlayerViewDelegate {
    func startedVideo() {
        print("Started video")
    }
    
    func stoppedVideo() {
        print("Stopped video")
    }
    
    func newVideo() {
        print("New Video")
    }
    
    func readyToPlayVideo() {
        print("Ready to play video")
    }
    
    func playingVideo(progress: Double) {
        //        print("Playing: \(progress)")
    }
    
    func pausedVideo() {
        print("Paused Video")
    }
    
    func finishedVideo() {
        print("Finished Video")
    }
    
    func seekStarted() {
        print("Seek started")
    }
    
    func seekEnded() {
        print("Seek ended")
    }
    
    func error(error: Error) {
        print("Error: \(error)")
    }
    
    func willShowControls() {
        print("will show controls")
    }
    
    func didShowControls() {
        print("did show controls")
    }
    
    func willHideControls() {
        print("will hide controls")
    }
    
    func didHideControls() {
        print("did hide controls")
    }
}

struct StatusDataModal {
    var image : String
    var des : String
    var statuscount : String
    
    init(image : String , des : String , statuscount : String) {
        self.image = image
        self.des = des
        self.statuscount = statuscount
    }
}
extension HomeFeedVC: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("received ad")
    }
//     func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
//        print(error)
//    }
}
