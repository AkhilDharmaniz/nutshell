//
//  PostVC.swift
//  Nutshell
//
//  Created by apple on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SDWebImage

class PostVC: UIViewController {
    
    @IBOutlet weak var postTableView: UITableView!
    var postArr = [HomeDataModal]()
    let secondNetworkURL = URL(string: "")
    var postId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        postTableView.delegate = self
        postTableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.postDetailsApi()
    }
    func postDetailsApi(){
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.SharePostDetails
        let parms : [String:Any] = ["user_id":user_ID, "post_id": postId]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let msg = response["message"] as? String ?? ""
            self.postArr.removeAll()
            print(status)
            if status == 1{
                if let data = response["data"] as? [String:Any]{
                    self.postArr.append(HomeDataModal(user_id: data["user_id"] as? String ?? "", name: data["username"] as? String ?? "", created_at: data["created_at"] as? String ?? "", description: data["description"] as? String ?? "", photo: data["photo"] as? String ?? "", post_id: data["post_id"] as? String ?? "", type: data["type"] as? String ?? "", uploads: data["uploads"] as? String ?? "", video_thumbnail: data["video_thumbnail"] as? String ?? "", likeCount: data["likeCount"] as? String ?? "", commentCount: data["commentCount"] as? String ?? "", isLike: data["isLike"] as? String ?? "", ad: false))
                    
                }
            }else{
                showMessage(title: Constant.shared.AppName, message: msg, okButton: "Follow", cancelButton: "Cancel", controller: self) {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
                    vc.User_ids = response["postUser"] as? String ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                } cancelHandler: {
                    AFWrapperClass.redirectToTabNavRVC(currentVC: self)
                }
                
                //    showAlertMessage(title: Constant.shared.AppName, message: msg, okButton: "OK", controller: self) {
                
                //    }
            }
            self.postTableView.reloadData()
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        AFWrapperClass.redirectToTabNavRVC(currentVC: self)
    }
}
extension PostVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = postTableView.dequeueReusableCell(withIdentifier: "HomeFeedTableCell", for: indexPath) as! HomeFeedTableCell
        cell.selectionStyle = .none
        cell.lblUserName.text = postArr[indexPath.row].name
        cell.btnPlay.tag = indexPath.row
        cell.btnProfileImage.tag = indexPath.row
        cell.userImg.sd_setShowActivityIndicatorView(true)
        if #available(iOS 13.0, *) {
            cell.userImg.sd_setIndicatorStyle(.large)
        } else {
            // Fallback on earlier versions
        }
        cell.userImg.sd_setImage(with: URL(string: postArr[indexPath.row].photo), placeholderImage: UIImage(named: "img1"), options: SDWebImageOptions.continueInBackground, completed: nil)
        
        cell.txtVw.text = postArr[indexPath.row].description
        cell.btnPlay.addTarget(self, action: #selector(playVideo(sender:)), for: .touchUpInside)
        cell.btnProfileImage.addTarget(self, action: #selector(gotoProfieVC(sender:)), for: .touchUpInside)
        cell.lblFavCount.text = postArr[indexPath.row].likeCount
        cell.lblcommentCount.text = postArr[indexPath.row].commentCount
        cell.likeBtn.tag = indexPath.row
        cell.likeBtn.addTarget(self, action: #selector(likeUnlikeBtnAction(sender:)), for: .touchUpInside)
        cell.commentBtn.tag = indexPath.row
        cell.commentBtn.addTarget(self, action: #selector(commentBtnAction(sender:)), for: .touchUpInside)
        cell.shareBtn.tag = indexPath.row
        cell.shareBtn.addTarget(self, action: #selector(shareBtnAction(sender:)), for: .touchUpInside)
        cell.likeImg.image = postArr[indexPath.row].isLike == "0" ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "heartTap")
        if postArr[indexPath.row].uploads == ""{
            cell.videoVwHeightConstraint.constant = 0
            cell.videoPlayBtn.isHidden = true
        }else{
            cell.videoVwHeightConstraint.constant = 180
            let urls = (URL(string:postArr[indexPath.row].uploads) ?? secondNetworkURL)!
            if urls.containsImage == true{
                cell.videoPlayBtn.isHidden = true
                cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                if #available(iOS 13.0, *) {
                    cell.thumbNailImgView.sd_setIndicatorStyle(.large)
                } else {
                    // Fallback on earlier versions
                }
                cell.thumbNailImgView.sd_setImage(with: URL(string: postArr[indexPath.row].uploads), placeholderImage: UIImage(named: "placeholderF"), options: SDWebImageOptions.continueInBackground, completed: nil)
                
            }else{
                cell.videoPlayBtn.isHidden = false
                cell.thumbNailImgView.sd_setShowActivityIndicatorView(true)
                if #available(iOS 13.0, *) {
                    cell.thumbNailImgView.sd_setIndicatorStyle(.large)
                } else {
                    // Fallback on earlier versions
                }
                cell.thumbNailImgView.sd_setImage(with: URL(string: postArr[indexPath.row].video_thumbnail), placeholderImage: UIImage(named: "placeholderF"), options: SDWebImageOptions.continueInBackground, completed: nil)
            }
        }
        let unixtimeInterval = postArr[indexPath.row].created_at
        let date = Date(timeIntervalSince1970:  unixtimeInterval.doubleValue)
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = NSLocale.current
        if compareDate(date1: now, date2: date){
            dateFormatter.dateFormat = "hh:mm a"
        }else{
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
        }
        let strDate = dateFormatter.string(from: date)
        print(strDate,"date")
        cell.timeLbl.text = strDate
        return cell
    }
    func compareDate(date1:Date, date2:Date) -> Bool {
        let order = NSCalendar.current.compare(date1, to: date2, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    @objc func playVideo(sender : UIButton) {
        let data = postArr[sender.tag]
        if data.uploads.contains(".mp4"){
            let url : String = data.uploads
            let urlStr : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let convertedURL : URL = URL(string: urlStr)!
            print(convertedURL)
            let player = AVPlayer(url: convertedURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true)
            {
                playerViewController.player!.play()
            }
        }else{
            if postArr[sender.tag].video_thumbnail == ""{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageVC") as! ShowImageVC
                vc.imgString = postArr[sender.tag].uploads
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @objc func gotoProfieVC(sender : UIButton) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        if user_ID != postArr[sender.tag].user_id{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
            vc.User_ids = postArr[sender.tag].user_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func likeUnlikeBtnAction(sender : UIButton) {
        let user_ID =  UserDefaults.standard.value(forKeyPath: "Uid") as? String ?? ""
        IJProgressView.shared.showProgressView()
        let CategoryData = Constant.shared.baseUrl + Constant.shared.LikeAndUnlike
        let parms : [String:Any] = ["user_id":user_ID, "post_id": postArr[sender.tag].post_id]
        AFWrapperClass.requestPOSTURL(CategoryData, params: parms, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as? Int ?? 0
            let msg = response["message"] as? String ?? ""
            print(status)
            if status == 1{
                self.postArr[sender.tag].isLike = self.postArr[sender.tag].isLike == "0" ? "1" : "0"
                var likeCount = NumberFormatter().number(from: self.postArr[sender.tag].likeCount)?.intValue ?? 0
                likeCount =  self.postArr[sender.tag].isLike == "1" ? likeCount+1 : likeCount-1
                self.postArr[sender.tag].likeCount = "\(likeCount)"
                self.postTableView.reloadData()
            }else{
                alert(Constant.shared.AppName, message: msg, view: self)
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
        }
        
    }
    @objc func commentBtnAction(sender : UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
        vc.postId = postArr[sender.tag].post_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func shareBtnAction(sender : UIButton) {
        IJProgressView.shared.showProgressView()
        AFWrapperClass.createContentDeepLink(title: Constant.shared.AppName, type: "Post", OtherId: postArr[sender.tag].post_id, description: "Hey, look at this post.", image: nil, link: postArr[sender.tag].video_thumbnail == "" ? postArr[sender.tag].uploads : postArr[sender.tag].video_thumbnail) { urlStr in
            IJProgressView.shared.hideProgressView()
            if let url = URL(string: urlStr ?? "") {
                print(urlStr)
                let objectsToShare = ["Hey, look at this post.", self.postArr[sender.tag].video_thumbnail == "" ? self.postArr[sender.tag].uploads : self.postArr[sender.tag].video_thumbnail,url] as [Any]
                AFWrapperClass.presentShare(objectsToShare: objectsToShare, vc: self)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        // return 70
    }
}
