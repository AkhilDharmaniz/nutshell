//
//  AddUpdateCollectionVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 05/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class AddUpdateCollectionVwCell: UICollectionViewCell {
    
    @IBOutlet weak var statusCornerVw: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var updateImg: UIImageView!
}
