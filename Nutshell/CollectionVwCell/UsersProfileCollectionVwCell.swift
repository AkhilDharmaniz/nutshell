//
//  UsersProfileCollectionVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 08/02/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class UsersProfileCollectionVwCell: UICollectionViewCell {
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var userImg: UIImageView!
}
