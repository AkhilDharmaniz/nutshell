//
//  AnimationCollectionCell.swift
//  InstagramStatusDemo
//
//  Created by Vivek Dharmani on 28/05/21.
//

import UIKit
import Gemini

class AnimationCollectionCell: GeminiCell {
    @IBOutlet weak var userTimeLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userImgProfile: UIImageView!
    @IBOutlet weak var userImg: UIImageView!
    var backBtn:(()->Void)?
    var ProfileBtn:(()->Void)?
//    func setcell(imageName:String) {
//        userImg.image = UIImage(named: imageName)
//    }
    @IBOutlet weak var backBtnClicked: UIButton!
    override func awakeFromNib() {
           super.awakeFromNib()
           // Initialization code
       }
    
    @IBAction func backBtn(_ sender: UIButton) {
        backBtn!()
    }
    
    
    @IBAction func profileDetailsBtnClicked(_ sender: UIButton) {
        ProfileBtn!()
    }
    
    
}
