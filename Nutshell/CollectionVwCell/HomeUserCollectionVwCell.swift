//
//  HomeUserCollectionVwCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 27/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HomeUserCollectionVwCell: UICollectionViewCell {
    var ChatBTN:(()->Void)?
    @IBOutlet weak var chatBtn: UIButton!
    // @IBOutlet weak var AdBannerVw: GADBannerView!
    @IBOutlet weak var UserProfilesImg: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var sponserImage: UIImageView!
    @IBOutlet weak var lblRole: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func chatBtnClicked(_ sender: UIButton) {
        self.ChatBTN?()
    }
}
