//
//  HomeStatusFeedCollectionCell.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 29/06/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class HomeStatusFeedCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cornerVw: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
}
