//
//  VideoCell.swift
//  Nutshell
//
//  Created by Apple on 13/07/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {

    @IBOutlet weak var btnPlayVideo: UIButton!
    @IBOutlet weak var imgThumbnail: UIImageView!
   
}
