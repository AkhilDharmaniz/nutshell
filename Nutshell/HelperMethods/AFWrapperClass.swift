//
//  AFWrapperClass.swift
//  Nutshell
//
//  Created by Dharmani Apps on 12/06/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import Alamofire
import Branch

//import NVActivityIndicatorView
class AFWrapperClass{
    
    class func requestPOSTURL(_ strURL : String, params : Parameters, success:@escaping (NSDictionary) -> Void, failure:@escaping (NSError) -> Void){
        let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(urlwithPercentEscapes!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"])
            .responseJSON { (response) in
             //   print(response)
                switch response.result {
                case .success(let value):
                    if let JSON = value as? [String: Any] {
                        success(JSON as NSDictionary)
                    }
                case .failure(let error):
                    let error : NSError = error as NSError
                    print(error)
                    failure(error)
                }
        }
    }
    
    class func multipartFormDataRequestPOSTURL(_ strURL : String, params : Parameters, fileName: String, files: [URL], success:@escaping (NSDictionary) -> Void, failure:@escaping (NSError) -> Void){
           
        let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let dataRequest = AF.request(urlwithPercentEscapes!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: ["Content-Type":"application/json"])
        let request = dataRequest.convertible
        print("request: \(request)")
        
        let formDataRequest = AF.upload(multipartFormData: { formData in
            
            for (key, value) in params {
                formData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
            }
            
            files.forEach { fileURL in
                formData.append(fileURL, withName: fileName)
            }
            
        }, with: request)
                
        formDataRequest.responseJSON { (response: AFDataResponse<Any>) in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    success(JSON as NSDictionary)
                }
            case .failure(let error):
                let error : NSError = error as NSError
                print(error)
                failure(error)
            }
        }
    }
    
    class func requestUrlEncodedPOSTURL(_ strURL : String, params : Parameters, success:@escaping (NSDictionary) -> Void, failure:@escaping (NSError) -> Void){
        let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(urlwithPercentEscapes!, method: .post, parameters: params, encoding: URLEncoding.default, headers: ["Content-Type":"application/x-www-form-urlencoded"])
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    if let JSON = value as? [String: Any] {
                        if response.response?.statusCode == 200{
                            success(JSON as NSDictionary)
                        }else if response.response?.statusCode == 400{
                            
                            let error : NSError = NSError(domain: "invalid user details", code: 400, userInfo: [:])
                            failure(error)
                        }
                    }
                case .failure(let error):
                    let error : NSError = error as NSError
                    print(error)
                    failure(error)
                }
        }
    }
    class func requestGETURL(_ strURL: String, params : [String : AnyObject]?, success:@escaping (AnyObject) -> Void, failure:@escaping (NSError) -> Void) {
        
        let urlwithPercentEscapes = strURL.addingPercentEncoding( withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(urlwithPercentEscapes!, method: .get, parameters: params, encoding: JSONEncoding.default)
            .responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    if let JSON = value as? Any {
                        success(JSON as AnyObject)
                        print(JSON)
                    }
                case .failure(let error):
                    let error : NSError = error as NSError
                    print(error)
                    failure(error)
                }
        }
    }
    class func presentShare(objectsToShare: [Any], vc: UIViewController) {
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = vc.view
        activityVC.popoverPresentationController?.sourceRect = vc.view.frame
        vc.present(activityVC, animated: true, completion: nil)
    }
    class func createContentDeepLink(title: String,type: String,OtherId: String, description: String, image: String?,link: String,completion: @escaping (String?) -> ()) {
        let buo = BranchUniversalObject(canonicalIdentifier: UUID().uuidString)
        buo.canonicalUrl = "https://nutshell.app/"
        buo.publiclyIndex = false
        buo.locallyIndex = false
        buo.title = title
        buo.contentDescription = description
        if image != "" {
            buo.imageUrl = image
        }
        let linkLP: BranchLinkProperties =  BranchLinkProperties()
        linkLP.addControlParam("link", withValue: link)
        linkLP.addControlParam("OtherID", withValue: OtherId)
        linkLP.addControlParam("type", withValue: type)
        linkLP.addControlParam("environment", withValue: AFWrapperClass.returnEnv().rawValue)
        buo.getShortUrl(with: linkLP) { (url, error) in
            if error == nil {
                completion(url)
            }
        }
    }
    class func svprogressHudShow(title:String = "",view:UIViewController) -> Void
    {
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setForegroundColor(#colorLiteral(red: 0.07608450204, green: 0.7891181111, blue: 0.1405828595, alpha: 1))
        SVProgressHUD.setBackgroundColor(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        SVProgressHUD.setRingThickness(5)
        DispatchQueue.main.async {
            view.view.isUserInteractionEnabled = false;
        }
    }
    class func svprogressHudDismiss(view:UIViewController) -> Void
    {
        SVProgressHUD.dismiss();
        view.view.isUserInteractionEnabled = true;
    }
    class func returnEnv()->Env{
        return Constant.shared.baseUrl == "https://www.dharmani.com/Nutshell/webservice/" ? .prod : .dev
    }
    enum Env:String{
        case dev
        case prod
    }
    class func returnCurrentUserId()->String{
        return UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
    }
    
    
    class func showImagePickerWithGifVideo(delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate, vc: UIViewController, editAllowed: Bool, maxDuration: Double?){
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = editAllowed
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = editAllowed ? ["public.movie"] : ["public.image","public.gif"]
                let maxVideoDuration = maxDuration ?? 75
                imagePicker.videoMaximumDuration = maxVideoDuration //40 sec
                imagePicker.navigationBar.tintColor = .black
                imagePicker.navigationBar.barStyle = .black
                imagePicker.delegate = delegate
                imagePicker.modalPresentationStyle = .currentContext
                vc.present(imagePicker, animated: true, completion:nil)
            } else {
//                Globals().alert("Error", message: "You denied permissions to access your photos. You'll need to allow permissions to choose photos to post.", actionTitle: "OK", viewController: vc)
            }
        }
    static func redirectToTabNavRVC(currentVC: UIViewController){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Tab") as! TabBarClass
        vc.isFrom = "push"
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)
        if #available(iOS 13.0, *){
            if let scene = UIApplication.shared.connectedScenes.first{
                guard let windowScene = (scene as? UIWindowScene) else { return }
                print(">>> windowScene: \(windowScene)")
                let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                window.windowScene = windowScene //Make sure to do this
                window.rootViewController = nav
                window.makeKeyAndVisible()
                appDelegate.window = window
            }
        } else {
            appDelegate.window?.rootViewController = nav
            appDelegate.window?.makeKeyAndVisible()
        }
    }
}



