//  Nutshell
//
//  Created by Dharmani Apps on 12/05/20.
//  Copyright © 2020 Dharmani Apps. All rights reserved.
//
import UIKit
class Constant: NSObject {
    
   static let shared = Constant()
     let AppName = "Nutshell"
    //  old base url
   //  live Url : ""
   //  localServer : ""
    //MARK: -- staging BaseUrl
 //   let baseUrl = "https://www.dharmani.com/Nutshell/webservice/"
    
    //MARK: -- Live BaseUrl
    let baseUrl = "http://nutshellapp.com/webservice/"
    let websiteUrl = "http://nutshellapp.com/"

    let googleAPIKey = "AIzaSyBWHXNtJbJeXj-R-JCFw9ksxWnqKrOMEDs"
    // MARK: -- Registeration Api's
    let signUpUrl = "SignUp.php"
    let signInUrl = "Login.php"
    let forgetPasswordUrl = "ForgetPassword.php"
    let getPostDetailsApi = "Home.php"
//    let getPostDetailsApi = "GetPostDetails.php"
  
    // UserDetails
    let addPost = "AddPost.php"
    let ChangePasswordApi = "ChangePassword.php"
    let getUserDetails = "GetProfileDetails.php"
    let EditProfileApi = "EditUserProfile.php"
    let AppEnvelopeApi = "AddEnvelope.php"
    let PrivatePublic = "UpadtePublicAndPrivate.php"
    let SearchApi = "GetAllUsersSearch.php"
    let NearByUser = "GetNearbyusers.php"
    let ScanQrProfileAPi = "qrCodeProfile.php"
    let GetEnvelopeApi = "GetEnvelope.php"
    let enableDisableApi = "DisableEnvelope.php"
    let EnvelopeEnableDetailsApi = "GetAllEnvelopeEnable.php"
    let AddLinks = "addlinks.php"
    let followApi = "addFollowAndUnfollow.php"
    let HomeUserlistApi = "GetFollowUsersv2.php"
    let statusUpdateImage = "StatusImagesUpload.php"
    let GetLInk = "GetLinkUseriId.php"
    let EnvelopeClicksCount = "EnvelopeLinkcount.php"
    let StaticticsApi = "GetEnvelopeLinkCount.php"
    let AddRating = "AddRating.php"
    let GetReviewApi = "GetUserReview.php"
    let GetStatusApi = "GetStatusDetails.php"
    let NotificationApi = "GetNotificationActivity.php"
    let ViewStatusApi = "statusImagesUpdate.php"
    let approveRejectApi = "appoveRejectFollowRequest.php"
    let requestChat = "RequestChat.php"
    let addMessageApi = "AddMessage.php"
    let GetchatUser = "GetChatUsers.php"
    let GetChatListing = "GetChatListing.php"
    let UnseenChatApi = "UpdateMessageSeen.php"
    let AddLinkCount = "AddLinkCount.php"
    let OtherUserApi = "GetUserProfileDetails.php"
    let Purchase = "AddSponsored.php"
    let UpdateLinkApi = "UpdteAddlink.php"
    let updateNotification = "updateNotificationReadStatus.php"
    let editEnvelope = "EditEnvelope.php"
    let GetAddLinkApi = "GetsocialLinks.php"
    let addLinksAPi = "addlinks.php"
    let customLinks = "AddsocialLinks.php"
    let AllStaticsLinks = "GetstaticsEnvelopeCount.php"
    let GetLinksListing = "GetAddLinkeDetailsByUserId.php"
    let AllstatictisAPi = "GetProfileCount.php"
    let FriendsListApi = "GetFollowAnFollowingUsers.php"
    let LikeAndUnlike = "LikeAndUnlike.php"
    let GetAllComments = "GetAllCommentBypostId.php"
    let AddComment = "AddComment.php"
    let GetAllReportReasons = "GetAllReportReasons.php"
    let AddReport = "AddReport.php"
    let BlockUser = "blockUser.php"
    let DeleteComment = "DeleteComment.php"
    let SharePostDetails = "SharePostDetails.php"
    let PrivacyLink = "privacy-policy.html"
    let TermsConditionsLink = "terms&services.html"
    let deletePost = "DeletePostByPostid.php"
    
    // MARK:-- Contact Us
    let ContactUsApi = "ContactUs.php"
    let logOutApi = "Logout.php"
    
    let MultipleStatusImage = "MultipleStatusImage.php"
    let GetStatusApiByUserId = "GetStatusDetalsByuser_id.php"
    let DeleteStatus = "DeleteStatus.php"
    let appcolor = #colorLiteral(red: 0, green: 0.7960784314, blue: 0.02745098039, alpha: 1)
    private override init(){}
}

