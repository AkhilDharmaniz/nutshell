//
//  AppDelegate.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 07/12/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps
import UserNotifications
import Branch
import GoogleMobileAds

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarPreviousNextAllowedClasses = [UIScrollView.self, UIView.self, UIStackView.self, UICollectionViewCell.self, UICollectionView.self]
        IQKeyboardManager.shared.disabledToolbarClasses = [IGStoryMessageController.self]
            
        //   self.configureNotification()
        GMSServices.provideAPIKey("AIzaSyARGfIaz6ZTTA3fESLnlwePwqvx5nSwk5M")
        GMSPlacesClient.provideAPIKey("AIzaSyARGfIaz6ZTTA3fESLnlwePwqvx5nSwk5M")
        // IQKeyboardManager.shared.canGoPrevious
        //IQKeyboardManager.shared.canGoNext
        Thread.sleep(forTimeInterval: 2)
        GADMobileAds.sharedInstance().start(completionHandler: nil)
         branchForAdvancedOrLowerVersions(launchOptions: launchOptions)
        //    MARK:--- NEW One
        if let userId = UserDefaults.standard.value(forKey: "Uid") as? String{
            if userId != ""{
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let rootVc = storyBoard.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                let nav = UINavigationController(rootViewController: rootVc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
            else if UserDefaults.standard.bool(forKey: "ONETIME") == true {
                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                let rootVc = storyBoard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                let nav = UINavigationController(rootViewController: rootVc)
                nav.isNavigationBarHidden = true
                self.window?.rootViewController = nav
                self.window?.makeKeyAndVisible()
            }
        }
        //   MARK : --- NEW ONE
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        return true
    }
    func branchForAdvancedOrLowerVersions(launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        if #available(iOS 13.0, *) {
            BranchScene.shared().initSession(launchOptions: launchOptions) { (params, error, scene) in
                if error != nil{
                    print(error?.localizedDescription ?? "")
                }
                if let dict = params as? [String:Any],let uid: String = dict["OtherID"] as? String {
                    print(uid,"Wow")
                    let env = dict["environment"] as? String ?? ""
                    if env == AFWrapperClass.returnEnv().rawValue && uid != "" &&  AFWrapperClass.returnCurrentUserId() != ""{
                        print("valid redirect")
                       self.checkAndNavigateToProfileDetails(OtherId:uid)
                    }else{
                        print("invalid redirect")
                    }
                }
            }
            
        }else{
            Branch.getInstance().initSession(launchOptions: launchOptions, andRegisterDeepLinkHandler: {params, error in
                if error != nil{
                    print(error?.localizedDescription ?? "")
                }
                if let dict = params as? [String:Any],let uid: String = dict["OtherID"] as? String {
                    print(uid)
                    let env = dict["environment"] as? String ?? ""
                    if env == AFWrapperClass.returnEnv().rawValue && uid != "" &&  AFWrapperClass.returnCurrentUserId() != ""{
                        print("valid redirect")
                       self.checkAndNavigateToProfileDetails(OtherId:uid)
                    }else{
                        print("invalid redirect")
                    }
                }
            })
        }
    }
    func checkAndNavigateToProfileDetails(OtherId: String){
       
         let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
         let rootVc = storyBoard.instantiateViewController(withIdentifier: "PostVC") as! PostVC
         rootVc.postId = OtherId
         let nav = UINavigationController(rootViewController: rootVc)
         nav.isNavigationBarHidden = true
         if #available(iOS 13.0, *){
             if let scene = UIApplication.shared.connectedScenes.first{
                 guard let windowScene = (scene as? UIWindowScene) else { return }
                 print(">>> windowScene: \(windowScene)")
                 let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                 window.windowScene = windowScene //Make sure to do this
                 window.rootViewController = nav
                 window.makeKeyAndVisible()
                 self.window = window
             }
         } else {
             self.window?.rootViewController = nav
             self.window?.makeKeyAndVisible()
         }
    }
    func logOutSuccess(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        if let _ = UserDefaults.standard.value(forKey: "Uid") as? String{
            UserDefaults.standard.removeObject(forKey: "Uid")
        }
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let rootVc = storyBoard.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        let nav = UINavigationController(rootViewController: rootVc)
        nav.setNavigationBarHidden(true, animated: true)
        appdelegate.window!.rootViewController = nav
    }
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}

func appDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}
extension AppDelegate: UNUserNotificationCenterDelegate{
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        }else{
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
        UNUserNotificationCenter.current().delegate = self
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //touch action
        // tell the app that we have finished processing the user’s action / response
        
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let userInfo = response.notification.request.content.userInfo as? [String:Any]{
            print(userInfo)
            if let apnsData = userInfo["aps"] as? [String:Any]{
                if let dataObj = apnsData["data"] as? [String:Any]{
             //       let typeDict = dataObj["type"] as? [String:Any] ?? [:]
                    let notificationType = dataObj["notification_type"] as? String ?? "3"
                    print(notificationType,"Tusjs")
                    let state = UIApplication.shared.applicationState
                    
                  //  if state != .active{
                        if notificationType == "0"{
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let rootVc = storyBoard.instantiateViewController(withIdentifier: "OtherUserProfilesVC") as! OtherUserProfilesVC
                            rootVc.User_ids = dataObj["otherID"] as? String ?? ""
                            rootVc.fromAppDelegate = "YES"
                            
                            let nav = UINavigationController(rootViewController: rootVc)
                            nav.isNavigationBarHidden = true
                            if #available(iOS 13.0, *){
                                if let scene = UIApplication.shared.connectedScenes.first{
                                    guard let windowScene = (scene as? UIWindowScene) else { return }
                                    print(">>> windowScene: \(windowScene)")
                                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                    window.windowScene = windowScene //Make sure to do this
                                    window.rootViewController = nav
                                    window.makeKeyAndVisible()
                                    self.window = window
                                }
                            } else {
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }else if notificationType == "1"{
                            if let groupDetails = dataObj["detail"] as? [String:Any]{
                                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let rootVc = storyBoard.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                                rootVc.selectedIndex = 3
                                //  rootVc.fromAppDelegate = "YES"
                                let nav = UINavigationController(rootViewController: rootVc)
                                nav.isNavigationBarHidden = true
                                if #available(iOS 13.0, *){
                                    if let scene = UIApplication.shared.connectedScenes.first{
                                        guard let windowScene = (scene as? UIWindowScene) else { return }
                                        print(">>> windowScene: \(windowScene)")
                                        let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                        window.windowScene = windowScene //Make sure to do this
                                        window.rootViewController = nav
                                        window.makeKeyAndVisible()
                                        self.window = window
                                    }
                                } else {
                                    self.window?.rootViewController = nav
                                    self.window?.makeKeyAndVisible()
                                }
                            }
                        } else if notificationType == "2" {
                            if let groupDetails = dataObj["detail"] as? [String:Any]{
                                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let rootVc = storyBoard.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                                rootVc.selectedIndex = 3
                                let nav = UINavigationController(rootViewController: rootVc)
                                nav.isNavigationBarHidden = true
                                if #available(iOS 13.0, *){
                                    if let scene = UIApplication.shared.connectedScenes.first{
                                        guard let windowScene = (scene as? UIWindowScene) else { return }
                                        print(">>> windowScene: \(windowScene)")
                                        let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                        window.windowScene = windowScene //Make sure to do this
                                        window.rootViewController = nav
                                        window.makeKeyAndVisible()
                                        self.window = window
                                    }
                                } else {
                                    self.window?.rootViewController = nav
                                    self.window?.makeKeyAndVisible()
                                }
                            }
                            
                        }else if notificationType == "3" {
                            if let groupDetails = dataObj["detail"] as? [String:Any]{
                                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let rootVc = storyBoard.instantiateViewController(withIdentifier: "Tab") as! TabBarClass
                                rootVc.selectedIndex = 2
                                
                                let nav = UINavigationController(rootViewController: rootVc)
                                nav.isNavigationBarHidden = true
                                if #available(iOS 13.0, *){
                                    if let scene = UIApplication.shared.connectedScenes.first{
                                        guard let windowScene = (scene as? UIWindowScene) else { return }
                                        print(">>> windowScene: \(windowScene)")
                                        let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                        window.windowScene = windowScene //Make sure to do this
                                        window.rootViewController = nav
                                        window.makeKeyAndVisible()
                                        self.window = window
                                    }
                                } else {
                                    self.window?.rootViewController = nav
                                    self.window?.makeKeyAndVisible()
                                }
                            }
                            
                        }else if notificationType == "4" {
                            if let groupDetails = dataObj["detail"] as? [String:Any]{
                                let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                                let rootVc = storyBoard.instantiateViewController(withIdentifier: "HomeFeedVC") as! HomeFeedVC
                                
                                let nav = UINavigationController(rootViewController: rootVc)
                                nav.isNavigationBarHidden = true
                                if #available(iOS 13.0, *){
                                    if let scene = UIApplication.shared.connectedScenes.first{
                                        guard let windowScene = (scene as? UIWindowScene) else { return }
                                        print(">>> windowScene: \(windowScene)")
                                        let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                        window.windowScene = windowScene //Make sure to do this
                                        window.rootViewController = nav
                                        window.makeKeyAndVisible()
                                        self.window = window
                                    }
                                } else {
                                    self.window?.rootViewController = nav
                                    self.window?.makeKeyAndVisible()
                                }
                            }
                            
                        }
                        else if notificationType == "5"{
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let rootVc = storyBoard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                            rootVc.fromAppDelegate = "YES"
                            rootVc.senderId = dataObj["otherID"] as? String ?? ""
                            rootVc.receiverName = dataObj["username"] as? String ?? ""
                            rootVc.roomId = dataObj["room_id"] as? String ?? ""
                            let nav = UINavigationController(rootViewController: rootVc)
                            nav.isNavigationBarHidden = true
                            if #available(iOS 13.0, *){
                                if let scene = UIApplication.shared.connectedScenes.first{
                                    guard let windowScene = (scene as? UIWindowScene) else { return }
                                    print(">>> windowScene: \(windowScene)")
                                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                    window.windowScene = windowScene //Make sure to do this
                                    window.rootViewController = nav
                                    window.makeKeyAndVisible()
                                    self.window = window
                                }
                            } else {
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }else if notificationType == "6"{
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let rootVc = storyBoard.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
                            rootVc.isFrom = "push"
                            rootVc.postId = dataObj["otherID"] as? String ?? ""
                            let nav = UINavigationController(rootViewController: rootVc)
                            nav.isNavigationBarHidden = true
                            if #available(iOS 13.0, *){
                                if let scene = UIApplication.shared.connectedScenes.first{
                                    guard let windowScene = (scene as? UIWindowScene) else { return }
                                    print(">>> windowScene: \(windowScene)")
                                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                    window.windowScene = windowScene //Make sure to do this
                                    window.rootViewController = nav
                                    window.makeKeyAndVisible()
                                    self.window = window
                                }
                            } else {
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }else if notificationType == "7"{
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let rootVc = storyBoard.instantiateViewController(withIdentifier: "CommentsVC") as! CommentsVC
                            rootVc.isFrom = "push"
                            rootVc.postId = dataObj["otherID"] as? String ?? ""
                            let nav = UINavigationController(rootViewController: rootVc)
                            nav.isNavigationBarHidden = true
                            if #available(iOS 13.0, *){
                                if let scene = UIApplication.shared.connectedScenes.first{
                                    guard let windowScene = (scene as? UIWindowScene) else { return }
                                    print(">>> windowScene: \(windowScene)")
                                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                    window.windowScene = windowScene //Make sure to do this
                                    window.rootViewController = nav
                                    window.makeKeyAndVisible()
                                    self.window = window
                                }
                            } else {
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }else{
                            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
                            let rootVc = storyBoard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                            //  rootVc.fromAppDelegate = "YES"
                            let nav = UINavigationController(rootViewController: rootVc)
                            nav.isNavigationBarHidden = true
                            if #available(iOS 13.0, *){
                                if let scene = UIApplication.shared.connectedScenes.first{
                                    guard let windowScene = (scene as? UIWindowScene) else { return }
                                    print(">>> windowScene: \(windowScene)")
                                    let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
                                    window.windowScene = windowScene //Make sure to do this
                                    window.rootViewController = nav
                                    window.makeKeyAndVisible()
                                    self.window = window
                                }
                            } else {
                                self.window?.rootViewController = nav
                                self.window?.makeKeyAndVisible()
                            }
                        }
                  //  }
                }
            }
        }
    }
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("message received")
        completionHandler([])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("device token", deviceTokenString)
        UserDefaults.standard.set(deviceTokenString as String, forKey: "deviceToken")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
}
