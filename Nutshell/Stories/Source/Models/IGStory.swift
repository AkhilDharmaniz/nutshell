//
//  IGStory.swift
//
//  Created by Ranjith Kumar on 9/8/17
//  Copyright (c) DrawRect. All rights reserved.
//

import Foundation

public struct IGStory: Codable {
    
    // Note: To retain lastPlayedSnapIndex value for each story making this type as class
    /*public var snapsCount: Int
    public var snaps: [IGSnap]
    public var internalIdentifier: String
    public var lastUpdated: Int
    public var user: IGUser
    var lastPlayedSnapIndex = 0
    var isCompletelyVisible = false
    var isCancelledAbruptly = false
    
    enum CodingKeys: String, CodingKey {
        case snapsCount = "snaps_count"
        case snaps = "snaps"
        case internalIdentifier = "id"
        case lastUpdated = "last_updated"
        case user = "user"
    }*/
    
    public var internalIdentifier: String?
    
    let lastStatusID, lastUpdateDate: String?
    let user: IGUser?
    let snapsCount: String?
    var snaps: [IGSnap]?
//    public var _snaps: [IGSnap] {
//        return snaps!.filter{!($0.isDeleted)}
//    }
    var lastPlayedSnapIndex = 0
    var isCompletelyVisible = false
    var isCancelledAbruptly = false
    
    enum CodingKeys: String, CodingKey {
        case lastStatusID = "last_status_id"
        case lastUpdateDate = "last_update_date"
        case user
        case snapsCount = "snaps_count"
        case snaps
    }
}

// MARK: Story convenience initializers and mutators

extension IGStory {
    
    init(data: Data) throws {
        self = try newJSONDecoder().decode(IGStory.self, from: data)
    }

    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }

    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }

    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }       
}

extension IGStory: Equatable {
    public static func == (lhs: IGStory, rhs: IGStory) -> Bool {
        return lhs.internalIdentifier == rhs.internalIdentifier
    }
}

extension Dictionary {    
    var toJson: String {
        let invalidJson = String("{}")
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
}
