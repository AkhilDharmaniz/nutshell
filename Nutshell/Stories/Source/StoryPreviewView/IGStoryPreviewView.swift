//
//  IGStoryPreviewView.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class IGStoryPreviewView: UIView {
    
    //MARK:- iVars
    var layoutType: layoutType?
    /**Layout Animate options(ie.choose which kinda animation you want!)*/
    lazy var layoutAnimator: (LayoutAttributesAnimator, Bool, Int, Int) = (layoutType!.animator, true, 1, 1)
    lazy var snapsCollectionViewFlowLayout: AnimatedCollectionViewLayout = {
        let flowLayout = AnimatedCollectionViewLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.animator = layoutAnimator.0
        flowLayout.minimumLineSpacing = 0.0
        flowLayout.minimumInteritemSpacing = 0.0
        return flowLayout
    }()
    
    lazy var imgBackground: UIImageView! = {
        let iv = UIImageView.init(frame: UIScreen.main.bounds)
        iv.backgroundColor = .white
        //iv.image = UIImage(named: "6")
        iv.image = UIImage(named: "P4")
        iv.contentMode = .scaleToFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var snapsCollectionView: UICollectionView! = {
        let cv = UICollectionView.init(frame: UIScreen.main.bounds, collectionViewLayout: snapsCollectionViewFlowLayout)
        cv.showsVerticalScrollIndicator = false
        cv.showsHorizontalScrollIndicator = false
        cv.register(IGStoryPreviewCell.self, forCellWithReuseIdentifier: IGStoryPreviewCell.reuseIdentifier)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.isPagingEnabled = true
        cv.isPrefetchingEnabled = false
        cv.collectionViewLayout = snapsCollectionViewFlowLayout
        cv.backgroundColor = .clear
        return cv
    }()
    
    //MARK:- Overridden functions
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    convenience init(layoutType: layoutType) {
        self.init()
        self.layoutType = layoutType
        createUIElements()
        installLayoutConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //MARK: - Private functions
    private func createUIElements(){
        
        backgroundColor = .black
        
        imgBackground.applyBlurEffect()
        addSubview(imgBackground)
        
        addSubview(snapsCollectionView)
    }
    private func installLayoutConstraints(){
        //Setting constraints for snapsCollectionview
        NSLayoutConstraint.activate([
            leftAnchor.constraint(equalTo: snapsCollectionView.leftAnchor),
            topAnchor.constraint(equalTo: snapsCollectionView.topAnchor),
            snapsCollectionView.rightAnchor.constraint(equalTo: rightAnchor),
            snapsCollectionView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
}

extension UIImageView {
    func applyBlurEffect() {
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(blurEffectView)
    }
}
