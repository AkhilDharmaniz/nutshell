//
//  Color+Additions.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func rgb(from hex: Int, alpha: CGFloat = 1.0) -> UIColor {
        let red = CGFloat((hex & 0xFF0000) >> 16) / 0xFF
        let green = CGFloat((hex & 0x00FF00) >> 8) / 0xFF
        let blue = CGFloat(hex & 0x0000FF) / 0xFF
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
