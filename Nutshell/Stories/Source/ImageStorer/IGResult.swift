//
//  Result.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import Foundation

public enum IGResult<V, E> {
    case success(V)
    case failure(E)
}
