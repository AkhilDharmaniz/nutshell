//
//  IGStoryMessageController.swift
//  Nutshell
//
//  Created by Dharmesh Avaiya on 9/4/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

protocol IGStoryMessageDelegate {
    
    func storyMessageView(controller: IGStoryMessageController, didSendTap message: String?)
    func storyMessageView(controller: IGStoryMessageController, didBeginEditing textView: UITextView)
    func storyMessageView(controller: IGStoryMessageController, didEndEditing textView: UITextView)
}

class IGStoryMessageController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var overlayView: UIView!
    @IBOutlet weak var messageView: UITextView!
    @IBOutlet weak var messageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSend: UIButton!
    
    
    var delegate: IGStoryMessageDelegate?
    
    var globalRoomId: String?
    var otherUserId : String?
    var statusId : String?
    
    //------------------------------------------------------
    
    //MARK: Customs
    
    func setup() {
        
        messageView.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(overlayDidTap))
        overlayView.addGestureRecognizer(tapGesture)
        
        messageView.becomeFirstResponder()
    }
    func getRoomID(successHandler: @escaping (() -> Void)){
        let chatData = Constant.shared.baseUrl + Constant.shared.requestChat
        let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
        let params:[String:Any] = [
            "user_id":userid,
            "owner_id":self.otherUserId ?? String()
        ]
        IJProgressView.shared.showProgressView()
        AFWrapperClass.requestPOSTURL(chatData, params: params, success: { (response) in
            print(response)
            IJProgressView.shared.hideProgressView()
            let status = response["status"] as! Int
         if status == 1{
            self.globalRoomId = response["room_id"] as? String ?? ""
             successHandler()
         }else{
         
            }
        })
        { (error) in
            IJProgressView.shared.hideProgressView()
            print(error)
         alert(Constant.shared.AppName, message: error.localizedDescription, view: self)
        }
    }
    func addMessageApi() {
        let userid = UserDefaults.standard.value(forKey: "Uid") ?? ""
        let url = Constant.shared.baseUrl + Constant.shared.addMessageApi
        let params: [String:Any] = [
            "room_id": self.globalRoomId ?? String(),
            "sender_id": userid,
            "message": self.messageView.text!,
            "receiver_id" : self.otherUserId ?? String(),
            "message_type":"1",
            "status_id": self.statusId ?? String()
        ]
        print(params)
        AFWrapperClass.svprogressHudShow(title: "Loading...", view: self)
        AFWrapperClass.requestPOSTURL(url, params: params, success: { (dict) in
            AFWrapperClass.svprogressHudDismiss(view: self)
        
            let result = dict as AnyObject
            print(result)
            let status = result["status"] as? Int ?? 0
            if status == 1 {
                
                let appendData = result["data"] as? [String:Any] ?? [:]
                SocketManger.shared.socket.emit("newMessage", self.globalRoomId ?? String(), appendData)
                
                self.messageView.resignFirstResponder()
                self.messageView.text = ""
                self.messageViewHeight.constant = 45
                
                self.delegate?.storyMessageView(controller: self, didSendTap: String())
                
            } else {
                print("lll")
            }
            
        }) { (error) in
            alert("Nutshell", message: error.localizedDescription, view: self)
            AFWrapperClass.svprogressHudDismiss(view: self)
        }
      
       
       
    }
    
    //------------------------------------------------------
    
    //MARK: Actions
    
    @IBAction func btnSendTap(_ sender: UIButton) {
        
        let trimmedString = messageView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString != "" {
            messageView.resignFirstResponder()
//            messageView.text = ""
//            self.messageViewHeight.constant = 45
            getRoomID {
                self.addMessageApi()
            }
        }
//        messageView.resignFirstResponder()
//        getRoomID {
//            self.addMessageApi()
//        }
        
    }
    
    //------------------------------------------------------
    
    //MARK: UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
    
        delegate?.storyMessageView(controller: self, didBeginEditing: messageView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let padding: CGFloat = 45
        let minHeight: CGFloat = 95
        let maxHeight: CGFloat = 200
        
        if (textView.contentSize.height + padding) <= minHeight {
            messageViewHeight.constant = minHeight
        } else if (textView.contentSize.height + padding) <= maxHeight && (textView.contentSize.height + padding) > minHeight {
            messageViewHeight.constant = (textView.contentSize.height + padding)
        }else{
            messageViewHeight.constant = maxHeight
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        //delegate?.storyMessageView(controller: self, didEndEditing: messageView)
    }
    
    //------------------------------------------------------
    
    //MARK: Selector
    
    @objc func overlayDidTap(_ sender: UITapGestureRecognizer) {
        
        self.messageView.endEditing(true)
        self.delegate?.storyMessageView(controller: self, didSendTap: String())
    }
    
    //------------------------------------------------------
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    //------------------------------------------------------
}
