//
//  ErrorModal.swift
//  Podmany
//
//  Created by Dharmesh Avaiya on 6/16/20.
//  Copyright © 2020 dharmesh. All rights reserved.
//

import Foundation

enum PodmanyError: Error {
    
    case InvalidURL
}

struct ErrorModal: Error {
   
    var code: Int
    var errorDescription: String
    
    init(code: Int, errorDescription: String) {
        self.code = code
        self.errorDescription = errorDescription
    }
}

struct Status {
    
    struct Code {
        
        static let emailNotVerified = 108
        static let success = 200
        static let unauthorized = 401
        static let notfound = 404
        static let sessionExpired = 500
    }
    
    struct Text {
        static let errorCode = "errorCode"
        static let errorDesc = "errorDesc"
        static let errorMessage = "errorMessage"
    }
    
    struct Domain {
        static let urlErrorDomain = "NSURLErrorDomain"
    }
}
