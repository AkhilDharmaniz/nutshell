//
//  NSFileManager.swift
//  Nutshell
//
//  Created by Dharmesh Avaiya on 8/26/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

class NSFileManager: NSObject {

    //------------------------------------------------------
    
    //MARK: Shared
    
    static var shared = NSFileManager()
    
    private let storiesFolder = "Stories"
    var stories: URL?
    
    var randomFileName: String {
        var darwinTime : timeval = timeval(tv_sec: 0, tv_usec: 0)
        gettimeofday(&darwinTime, nil)
        return String("\((Int64(darwinTime.tv_sec) * 1000) + Int64(darwinTime.tv_usec / 1000)).jpeg")
    }
    
    //------------------------------------------------------
    
    //MARK: Directory Paths
    
    func createFolderInDocumentsDirectory(folderName: String) -> URL{
        
        let documentsDirectory = getDocumentsDirectory()
        let dataPath = documentsDirectory.appendingPathComponent(folderName)

        if !isFileAvalibleAt(path: dataPath) {
            do {
                try FileManager.default.createDirectory(atPath: dataPath.relativePath, withIntermediateDirectories: true, attributes: nil)
                return dataPath
            } catch {
                print(error.localizedDescription);
            }
        }
        return dataPath
    }
    
    func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getCachesDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    public func isFileAvalibleAt(path: URL) -> Bool {
        
        if FileManager.default.fileExists(atPath: path.relativePath) {
            return true
        }
        return false
    }
    
    public func getUnsavedStories() -> [URL] {
        
        do {
            
            let directoryContents = try FileManager.default.contentsOfDirectory(at: stories!, includingPropertiesForKeys: nil)
            let jpegs = directoryContents.filter{ $0.pathExtension == "jpeg" }
            debugPrint("jpeg urls:", jpegs)
            return jpegs
            
        } catch let error {
            debugPrint(error)
            return []
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Write File
    
    func write(jsonString: String, localFilePath: String, sucess:@escaping ((_ flag: Bool)->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonString, options: JSONSerialization.WritingOptions.fragmentsAllowed)
            try jsonData.write(to: URL(fileURLWithPath: localFilePath))
        } catch let error {
            failure(ErrorModal(code: error._code, errorDescription: error.localizedDescription))
        }
    }
    
    func write(jsonData: Data, localFilePath: String, sucess:@escaping ((_ flag: Bool)->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        do {
            let directoryPath = URL(fileURLWithPath: localFilePath).deletingLastPathComponent()
            if FileManager.default.fileExists(atPath: directoryPath.path) == false {
                try FileManager.default.createDirectory(at: directoryPath, withIntermediateDirectories: true, attributes: nil)
            }
            try jsonData.write(to: URL(fileURLWithPath: localFilePath), options: Data.WritingOptions.atomic)
            sucess(true)
        } catch let error {
            failure(ErrorModal(code: error._code, errorDescription: error.localizedDescription))
        }
    }
    
    func write(image: UIImage, localFilePath: String, sucess:@escaping ((_ flag: Bool)->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        do {
            let directoryPath = URL(fileURLWithPath: localFilePath).deletingLastPathComponent()
            if FileManager.default.fileExists(atPath: directoryPath.path) == false {
                try FileManager.default.createDirectory(at: directoryPath, withIntermediateDirectories: true, attributes: nil)
            }
            if let data = image.jpegData(compressionQuality: 0.8) {
                let storiesPath = stories?.appendingPathComponent(URL(fileURLWithPath: localFilePath).lastPathComponent)
                try data.write(to: storiesPath!)
                sucess(true)
            } else {
                sucess(false)
            }
        } catch let error {
            print(error)
            failure(ErrorModal(code: error._code, errorDescription: error.localizedDescription))
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Read File
    
    func parseLocalJsonFile<T: Decodable>(fileName: String, decodingType: T.Type, sucess:@escaping ((_ response: T)->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        
        guard let localFilePath = Bundle.main.path(forResource: fileName, ofType: "json") else {
            failure(ErrorModal(code: Status.Code.notfound, errorDescription: "file not found"))
            return
        }
        
        guard let jsonFileData = FileManager.default.contents(atPath: localFilePath) else {
            failure(ErrorModal(code: Status.Code.notfound, errorDescription: "file not found"))
            return
        }
        
        do {
            
            if let jsonResult = try JSONSerialization.jsonObject(with: jsonFileData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] {
                debugPrint("--------------------")
                debugPrint("parseLocalJsonFile:\(String(describing: jsonResult)))")
                debugPrint("--------------------")
                let result = try JSONDecoder().decode(decodingType, from: jsonFileData)
                sucess(result)
            } else if let jsonResult = try JSONSerialization.jsonObject(with: jsonFileData, options: JSONSerialization.ReadingOptions.allowFragments) as? [Any] {
                debugPrint("--------------------")
                debugPrint("parseLocalJsonFile:\(String(describing: jsonResult)))")
                debugPrint("--------------------")
                let result = try JSONDecoder().decode(decodingType, from: jsonFileData)
                sucess(result)
            }
            
        } catch let error {
            
            failure(ErrorModal(code: error._code, errorDescription: error.localizedDescription))
        }
    }
    
    func parseLocalJsonFile<T: Decodable>(localFilePath: String, decodingType: T.Type, sucess:@escaping ((_ response: T)->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        
        guard let jsonFileData = try? Data(contentsOf: URL(fileURLWithPath: localFilePath)) else {
            failure(ErrorModal(code: Status.Code.notfound, errorDescription: "file not found"))
            return
        }
        
        do {
            
            if let jsonResult = try JSONSerialization.jsonObject(with: jsonFileData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: Any] {
                debugPrint("--------------------")
                debugPrint("parseLocalJsonFile:\(String(describing: jsonResult)))")
                debugPrint("--------------------")
                let result = try JSONDecoder().decode(decodingType, from: jsonFileData)
                sucess(result)
            }
            
        } catch let error {
            
            failure(ErrorModal(code: error._code, errorDescription: error.localizedDescription))
        }
    }
    
    func parseLocalTextJsonFile(jasonFilePath: String, sucess:@escaping ((_ response: [String:String])->Void), failure:@escaping ((_ error : ErrorModal) -> Void)) {
        
        guard let jsonFileData = FileManager.default.contents(atPath: jasonFilePath) else {
            return
        }
        
        do {
            if let jsonResult = try JSONSerialization.jsonObject(with: jsonFileData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: String] {
                debugPrint("parseLocalJsonFile:\(String(describing: jsonResult)))")
                sucess(jsonResult)
            }
        } catch let error {
            debugPrint("\(error)")
        }
    }
    
    func parseLocalTextJsonFile(jasonFilePath: String) -> [String: Any]? {
        
        guard let jsonFileData = FileManager.default.contents(atPath: jasonFilePath) else {
            return nil
        }
        
        do {
            if let jsonResult = try JSONSerialization.jsonObject(with: jsonFileData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: String] {
                return jsonResult
            }
        } catch let error {
            debugPrint("\(error)")
        }
        return nil
    }
    
    //------------------------------------------------------
    
    //MARK: Delete
    
    func deleteFilesAt(_ filePaths: String) {
        
        do {
            try FileManager.default.removeItem(atPath: filePaths)
            debugPrint("fileToDelete:\(filePaths)")
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }

    //------------------------------------------------------
    
    //MARK: Init
    
    override init() {
        super.init()
        
        stories = createFolderInDocumentsDirectory(folderName: storiesFolder)
    }
    
    //------------------------------------------------------
}
