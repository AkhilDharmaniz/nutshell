//
//  DateTimeManager.swift
//  Nutshell
//
//  Created by Dharmesh Avaiya on 8/26/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit

public struct DateFormate {
    
    static let MMM_DD_COM_yyyy = "MMM dd, yyyy"
}

class DateTimeManager: NSObject {
    
    let dateFormatter = DateFormatter()
    
    //------------------------------------------------------
    
    //MARK: Shared
    
    static let shared = DateTimeManager()
    
    //------------------------------------------------------
    
    //MARK: Public
    
    func dateFrom(unix: Int) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(unix))
    }
    
    func dateFrom(unix: Int, inFormate: String) -> String {
        let date = dateFrom(unix: unix)
        dateFormatter.dateFormat = inFormate
        return dateFormatter.string(from: date)
    }
    
    //------------------------------------------------------
}
