//
//  MultipleImagePickerManager.swift
//  Nutshell
//
//  Created by Dharmesh Avaiya on 8/26/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import Foundation
import ImagePicker

protocol MultipleImagePickerDelegate {
    
    func multipleImagePickerManager(imagePicker: ImagePickerController, localFileURLs: [URL])
}

class MultipleImagePickerManager: NSObject, ImagePickerDelegate {
    
    static let shared = MultipleImagePickerManager()
    
    let config = Configuration()
    var imagePicker: ImagePickerController?
    var delegate: MultipleImagePickerDelegate?
    
    //------------------------------------------------------
    
    //MARK: Private
    
    func saveLocally(images: [UIImage], success: @escaping()->Void) {
        var imagesToSave = images
        if let first = imagesToSave.first {
            if let filePath = NSFileManager.shared.stories?.appendingPathComponent(NSFileManager.shared.randomFileName) {
                NSFileManager.shared.write(image: first, localFilePath: filePath.absoluteString) { isWrite in
                    imagesToSave.removeFirst()
                    self.saveLocally(images: imagesToSave, success: success)
                } failure: { error in
                    //Failed to write
                    success()
                }
            }
        }
        success()
    }
    
    //------------------------------------------------------
    
    //MARK: Public
    
    func present(target: UIViewController) {
        
        imagePicker = ImagePickerController(configuration: config)
        imagePicker?.delegate = self
        
        if let imagePicker = imagePicker {
            imagePicker.modalPresentationStyle = .fullScreen            
            target.present(imagePicker, animated: true, completion: nil)
        }
    }
      
    //------------------------------------------------------
    
    //MARK: ImagePickerDelegate
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        //TODO: Handle preview
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        guard images.count > 0 else {
            return
        }
        
        if images.count > 20 {
            let alert  = UIAlertController(title: "Nutshell", message: "You are exceeds the maximum image selection", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            imagePicker.present(alert, animated: true, completion: nil)
            return
        }
        
        saveLocally(images: images) {
            imagePicker.dismiss(animated: true) {
                let localFileURLs = NSFileManager.shared.getUnsavedStories()
                self.delegate?.multipleImagePickerManager(imagePicker: imagePicker, localFileURLs: localFileURLs)
            }
        }
    }
    
    //------------------------------------------------------
    
    //MARK: Init
    
    override init() {
        super.init()
        
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
    }
    
    //------------------------------------------------------
}
