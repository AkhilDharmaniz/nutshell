//
//  IGHomeViewModel.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/08/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import Foundation

struct IGHomeViewModel {
    
    var stories: IGStories?
    
    /*
    //MARK: - iVars
    //Keep it Immutable! don't get Dirty :P
    private let stories: IGStories? = {
        do {
            return try IGMockLoader.loadMockFile(named: "stories.json", bundle: .main)
        }catch let e as MockLoaderError {
            debugPrint(e.description)
        }catch{
            debugPrint("could not read Mock json file :(")
        }
        return nil
    }()
    */
    
    //MARK: - Public functions
    public func getStories() -> IGStories? {
        return stories
    }
    
    public func numberOfItemsInSection(_ section:Int) -> Int {
        if let count = stories?.stories?.count {
            return count + 1 // Add Story cell
        }
        return 1
    }
    
    public func myStories() -> IGStory? {
        return stories?.myStories?.first
    }
    
    public func cellForItemAt(indexPath: IndexPath) -> IGStory? {
        return stories?.stories?[indexPath.row-1]
    }
    
}
