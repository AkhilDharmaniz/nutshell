//
//  BoostProfileVC.swift
//  Nutshell
//
//  Created by Vivek Dharmani on 18/01/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import UIKit
import SafariServices
import SwiftyStoreKit
import SVProgressHUD

class BoostProfileVC: UIViewController {
    
    @IBOutlet weak var boostProfileVw: UIView!
    var message = String()
    var retrievedProducts:[PurchaseProducts] = []{
        
    didSet{
    if let first = retrievedProducts.first{
        
    }
    }
    }
    override func viewDidLoad() {
    super.viewDidLoad()
    retrieveProducts()
    }
    //MARK:- Purchase Method(s)
    func retrieveProducts(){
        IJProgressView.shared.showProgressView()
        SwiftyStoreKit.retrieveProductsInfo([IAPProduct.WeeklyPlan.rawValue]) { result in
            IJProgressView.shared.hideProgressView()
            self.retrievedProducts.removeAll()
            result.retrievedProducts.forEach { (product) in
                print(product.localizedTitle, product.priceLocale)
                let title = product.localizedTitle
                self.retrievedProducts.append(PurchaseProducts(name: title, price: product.localizedPrice ?? "", productID: product.productIdentifier))
            }
        }
    }
    
    func restorePurchases(){
        AFWrapperClass.svprogressHudShow(view: self)
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            AFWrapperClass.svprogressHudDismiss(view: self)
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            
            else if results.restoredPurchases.count > 0 {
                let filteredSubIds = results.restoredPurchases.filter({$0.productId == IAPProduct.WeeklyPlan.rawValue})
                for purchase in filteredSubIds{
                    if purchase.needsFinishTransaction{
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                }
                
          if filteredSubIds.count > 0{
                    let sorted = filteredSubIds.sorted { (arg0, arg1) -> Bool in
                        guard let date1 = arg0.transaction.transactionDate,let date2 = arg1.transaction.transactionDate else {
                            return false
            }
            return date1.compare(date2) == .orderedDescending
            }
            self.verifyPurchase(productIDPrefix: sorted.first?.productId ?? "")
            }else{
            alert("No purchases to restore", message: "There are no purchases with these subscriptions to restore related to your account.", view: self)
                }
            }else{
            alert("No purchases to restore", message: "There are no purchases with these subscriptions to restore related to your account.", view: self)
            }
        }
    }
    
    func verifyPurchase(productIDPrefix: String,_ callback : ((Bool)->Void)? = nil, service: AppleReceiptValidator.VerifyReceiptURLType = .production){
        let appleValidator = AppleReceiptValidator(service: service, sharedSecret: sharedSecret)
        AFWrapperClass.svprogressHudShow(view: self)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            AFWrapperClass.svprogressHudDismiss(view: self)
            switch result {
            
            case .success(let receipt):
                // Verify the purchase of a Subscription
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId: productIDPrefix,
                    inReceipt: receipt)
                print("case passed in", service)
                // Then call the callback if available
                if callback != nil {
                    callback!(true)
                }
                switch purchaseResult {
                case .purchased(let expiryDate, let receiptItems):
                    print("\(productIDPrefix) is valid until \(expiryDate.timeIntervalSince1970)\n\(receiptItems)\n")
                    if let _ = receiptItems.filter({$0.productId == productIDPrefix && $0.subscriptionExpirationDate == expiryDate}).first{
                        //hit backend api
                        self.IAPPurchase(date: expiryDate)
                        AFWrapperClass.svprogressHudShow(view: self)
                    }
                case .expired(let expiryDate, let items):
                    print("\(productIDPrefix) is expired on \(expiryDate.timeIntervalSince1970)\n\(items)\n")
                    alert("Not Subscribed", message: "It doesn't look like you're a subscribed member or your subscription expired. Please purchase a subscription to continue.", view: self)
                    AFWrapperClass.svprogressHudDismiss(view: self)
                case .notPurchased:
                    alert("Not Subscribed", message: "It doesn't look like you're a subscribed member or your subscription expired. Please purchase a subscription to continue.", view: self)
                    AFWrapperClass.svprogressHudDismiss(view: self)
                    print("The user has never purchased \(productIDPrefix)")
                }
            case .error(let error):
                if service == .production {
                    self.verifyPurchase(productIDPrefix: productIDPrefix, callback, service: .sandbox)
                }else{
                    AFWrapperClass.svprogressHudDismiss(view: self)
                    print("Trying again with sandbox")
                    if callback != nil {
                        callback!(false)
                    }
                }
                print("Receipt verification failed: \(error)")
            }
        }
    }
    
    func purchaseProduct(productIDPrefix: String){
        AFWrapperClass.svprogressHudShow(view: self)
        SwiftyStoreKit.purchaseProduct(productIDPrefix, quantity: 1, atomically: true) { result in
            switch result {
            case .success(let purchase):
                if purchase.needsFinishTransaction{
                    SwiftyStoreKit.finishTransaction(purchase.transaction)
                }
                AFWrapperClass.svprogressHudDismiss(view: self)
                self.checkAfterPurchase(productIDPrefix: productIDPrefix)
            case .error(let error):
                AFWrapperClass.svprogressHudDismiss(view: self)
                print((error as NSError).localizedDescription)
                switch error.code {
                case .unknown:
                    print("Unknown error. Please contact support")
                case .clientInvalid:
                    print("Not allowed to make the payment")
                case .paymentCancelled: break
                case .paymentInvalid:
                    print("The purchase identifier was invalid")
                case .paymentNotAllowed:
                    print("The device is not allowed to make the payment")
                case .storeProductNotAvailable:
                    print("The product is not available in the current storefront")
                case .cloudServicePermissionDenied:
                    print("Access to cloud service information is not allowed")
                case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                default: print((error as NSError).localizedDescription)
                }
            }
        }
    }
    
    func checkAfterPurchase(productIDPrefix: String,_ callback : ((Bool)->Void)? = nil, service: AppleReceiptValidator.VerifyReceiptURLType = .production){
        //   AFWrapperClass.svprogressHudShow(view: self)
        let appleValidator = AppleReceiptValidator(service: service, sharedSecret: sharedSecret)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            AFWrapperClass.svprogressHudDismiss(view: self)
            switch result{
            case .success(let receipt):
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable,
                    productId: productIDPrefix,
                    inReceipt: receipt)
                
                // Then call the callback if available
                if callback != nil {
                    callback!(true)
                }
                switch purchaseResult {
                case .purchased(let expiryDate, let receiptItems):
                    print("Product is valid until \(expiryDate.timeIntervalSince1970)", receiptItems)
                    if let _ = receiptItems.filter({$0.productId == productIDPrefix && $0.subscriptionExpirationDate == expiryDate}).first{
                        //hit backend api
                        self.IAPPurchase(date: expiryDate)
                        AFWrapperClass.svprogressHudShow(view: self)
                    }
                case .expired(let expiryDate, let receiptItems):
                    print("Product is expired since \(expiryDate)", receiptItems)
                    //show popup regarding plan expire
                    alert("Not Subscribed", message: "It doesn't look like you're a subscribed member or your subscription expired. Please purchase a subscription to continue.", view: self)
                    AFWrapperClass.svprogressHudDismiss(view: self)
                case .notPurchased:
                    print("This product has never been purchased")
                    //show popup regarding plan not purchased
                    alert("Not Subscribed", message: "It doesn't look like you're a subscribed member or your subscription expired. Please purchase a subscription to continue.", view: self)
                    AFWrapperClass.svprogressHudDismiss(view: self)
                }
            case .error(let error):
                if service == .production {
                    self.checkAfterPurchase(productIDPrefix: productIDPrefix, callback, service: .sandbox)
                }else{
                    AFWrapperClass.svprogressHudDismiss(view: self)
                    print("Trying again with sandbox")
                    if callback != nil {
                        callback!(false)
                    }
                }
                print("Receipt verification failed: \(error)")
            }
        }
    }
    
    func IAPPurchase(date: Date) {
        let idValue = UserDefaults.standard.value(forKey: "Uid") as? String ?? ""
        AFWrapperClass.svprogressHudShow(view: self)
        let ContactUs = Constant.shared.baseUrl + Constant.shared.Purchase
        let parms : [String:Any] = ["user_id":idValue,"purchasePlan":"weekly","expireDate":date.timeIntervalSince1970]
        print(parms)
        AFWrapperClass.requestPOSTURL(ContactUs, params: parms, success: { (response) in
            AFWrapperClass.svprogressHudDismiss(view: self)
            let status = response["status"] as? Int ?? 0
            self.message = response["message"] as? String ?? ""
            print(self.message, response)
            if status == 1{
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "Nutshell", message: "Purchase successfully", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }else {
                alert("Nutshell", message: self.message, view: self)
            }
        }) { (error) in
            AFWrapperClass.svprogressHudDismiss(view: self)
            print(error)
        }
    }
    
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func termsBtnClicked(_ sender: UIButton) {
        if let url = URL(string: Constant.shared.websiteUrl+Constant.shared.TermsConditionsLink)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func privacyBtnClicked(_ sender: UIButton) {
        if let url = URL(string: Constant.shared.websiteUrl+Constant.shared.PrivacyLink)
        {
            let safariCC = SFSafariViewController(url: url)
            present(safariCC, animated: true, completion: nil)
        }
    }
    @IBAction func restoreBtnClicked(_ sender: UIButton) {
        restorePurchases()
    }
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        purchaseProduct(productIDPrefix: IAPProduct.WeeklyPlan.rawValue)
       
    }
}
struct PurchaseProducts{
    var name: String
    var price: String
    var productID: String
    var selected: Bool = false
}
