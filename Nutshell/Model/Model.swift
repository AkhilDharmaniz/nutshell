//
//  File.swift
//  Fahrschule star member
//
//  Created by Vivek Dharmani on 12/05/20.
//  Copyright © 2020 Vivek Dharmani. All rights reserved.
//

import Foundation
import UIKit

struct ScanUserProfileModel {
    var QrCode = String()
    var email = String()
    var name = String()
    var photo = String()
    var role = String()
    init(QrCode:String,email:String,name:String,photo:String,role:String) {
        self.QrCode = QrCode
        self.email = email
        self.name = name
        self.photo = photo
        self.role = role
    }
}

struct EnvelopeDetailModel {
    var id = String()
    var user_id = String()
    var name = String()
    var link = String()
    var description = String()
    var enable = String()
    var created_at = String()
    var envelopeid = String()
    init(id:String,user_id:String,name:String,link:String,description:String,enable:String,created_at:String,envelopeid:String) {
        self.id = id
        self.user_id = user_id
        self.name = name
        self.link = link
        self.description = description
        self.enable = enable
        self.created_at = created_at
        self.envelopeid = envelopeid
    }
}

struct EnvelopebleEnaDetailModel {
    var id = String()
    var user_id = String()
    var name = String()
    var link = String()
    var description = String()
    var enable = String()
    var created_at = String()
    var expirylink = String()
    init(id:String,user_id:String,name:String,link:String,description:String,enable:String,created_at:String,expirylink:String) {
        self.id = id
        self.user_id = user_id
        self.name = name
        self.link = link
        self.description = description
        self.enable = enable
        self.created_at = created_at
        self.expirylink = expirylink
    }
}

struct SearchDetailsModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var ratingavg = Int()
    var ratingcount = Int()
    var sponsored_key = String()
    var username = String()
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,country:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,ratingavg:Int,ratingcount:Int,sponsored_key:String,username:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.country = country
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.ratingavg = ratingavg
        self.ratingcount = ratingcount
        self.sponsored_key = sponsored_key
        self.username = username
    }
}

struct GetLinksModel {
    var social_id = String()
    var title = String()
    var imageurl = String()
    var user_id = String()
    var sort_id = String()
    var linkurl = String()
    var create_date = String()
    var linkcount = String()
    var link_id = String()
    
    init(social_id:String,title:String,imageurl:String,user_id:String,sort_id:String,linkurl:String,create_date:String,linkcount:String,link_id:String) {
        self.social_id = social_id
        self.title = title
        self.imageurl = imageurl
        self.user_id = user_id
        self.sort_id = sort_id
        self.linkurl = linkurl
        self.create_date = create_date
        self.linkcount = linkcount
        self.link_id = link_id
    }
}

struct NearByUser:Codable {
    var status:Int?
    var message:String?
    var data:[NearUserDetails]?
}
struct NearUserDetails:Codable {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
}

struct FriendListModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var ratingavg = Int()
    var ratingcount = Int()
    var sponsored_key = String()
    var ad = Bool()
    
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,country:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,ratingavg:Int,ratingcount:Int,ad:Bool,sponsored_key:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.country = country
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.ratingavg = ratingavg
        self.ratingcount = ratingcount
        self.ad = ad
        self.sponsored_key = sponsored_key
    }
}


struct ReviewListModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var review = String()
    var id = String()
    var ratingby = String()
    var rating = String()
    var ratingCount = String()
    var createrreview = String()
    var profile_type = String()
    
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,country:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,review:String,id:String,ratingby:String,ratingCount:String,createrreview:String,rating:String,profile_type:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.country = country
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.review = review
        self.id = id
        self.ratingby = ratingby
        self.rating = rating
        self.ratingCount = ratingCount
        self.createrreview = createrreview
        self.profile_type = profile_type
    }
}

struct StatusModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var status_image = String()
    var status_id = String()
    var image_view = Int()
    var statusdate = String()
    
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,country:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,status_image:String,status_id:String,image_view:Int,statusdate:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.country = country
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.status_image = status_image
        self.status_id = status_id
        self.image_view = image_view
        self.statusdate = statusdate
    }
}
struct UserStatusModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var country = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var status_image = String()
    var status_id = String()
    
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,country:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,status_image:String,status_id:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.country = country
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.status_image = status_image
        self.status_id = status_id
    }
}

struct NotificationModel {
    var notification_id = String()
    var message = String()
    var notification_type = String()
    var user_id = String()
    var otherID = String()
    var room_id = String()
    var followID = String()
    var creation_date = String()
    var viewed_status = String()
    var notification_read_status = String()
    var creation_date_timestamp = String()
    var name = String()
    var image = String()
    var title_message = String()
    
    init(notification_id:String,message:String,notification_type:String,user_id:String,otherID:String,room_id:String,followID:String,creation_date:String,viewed_status:String,notification_read_status:String,creation_date_timestamp:String,name:String,image:String,title_message:String) {
        self.notification_id = notification_id
        self.message = message
        self.notification_type = notification_type
        self.user_id = user_id
        self.otherID = otherID
        self.room_id = room_id
        self.followID = followID
        self.creation_date = creation_date
        self.viewed_status = viewed_status
        self.notification_read_status = notification_read_status
        self.creation_date_timestamp = creation_date_timestamp
        self.name = name
        self.image = image
        self.title_message = title_message
    }
    
}
struct GetAddLinkModel {
    var social_id = String()
    var user_id = String()
    var title = String()
    var imageurl = String()
    var createDate = String()
    var linkurl = String()
    var text = String()
    init(social_id:String,user_id:String,title:String,imageurl:String,createDate:String,linkurl:String,text:String) {
        self.social_id = social_id
        self.user_id = user_id
        self.title = title
        self.imageurl = imageurl
        self.createDate = createDate
        self.linkurl = linkurl
        self.text = text
    }
}
struct FollowingListingModel {
    var user_id = String()
    var name = String()
    var email = String()
    var role = String()
    var password = String()
    var photo = String()
    var location = String()
    var countryCode = String()
    var twitterId = String()
    var latitude = String()
    var longitude = String()
    var description = String()
    var verified = String()
    var totalFollowing = String()
    var totalFollowers = String()
    var verificateCode = String()
    var created_at = String()
    var disabled = String()
    var allowPush = String()
    var device_type = String()
    var device_token = String()
    var status = String()
    var changeStatus = String()
    var profile_type = String()
    var sponsored_key = String()
    var ratingavg = String()
    var ratingcount = String()
    var isfollow = String()
    var followID = String()
    var username = String()
    
    init(user_id:String,name:String,email:String,role:String,password:String,photo:String,location:String,countryCode:String,twitterId:String,latitude:String,longitude:String,description:String,verified:String,totalFollowing:String,totalFollowers:String,verificateCode:String,created_at:String,disabled:String,allowPush:String,device_type:String,device_token:String,status:String,changeStatus:String,profile_type:String,sponsored_key:String,ratingavg:String,ratingcount:String,isfollow:String,followID:String,username:String) {
        self.user_id = user_id
        self.name = name
        self.email = email
        self.role = role
        self.password = password
        self.photo = photo
        self.location = location
        self.countryCode = countryCode
        self.twitterId = twitterId
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.verified = verified
        self.totalFollowing = totalFollowing
        self.totalFollowers = totalFollowers
        self.verificateCode = verificateCode
        self.created_at = created_at
        self.disabled = disabled
        self.allowPush = allowPush
        self.device_type = device_type
        self.device_token = device_token
        self.status = status
        self.changeStatus = changeStatus
        self.profile_type = profile_type
        self.ratingavg = ratingavg
        self.ratingcount = ratingcount
        self.isfollow = isfollow
        self.followID = followID
        self.username = username
    }
}


struct PostData {
    var image : String
    var name : String
    var des : String
    var userId : String
    
    init(image : String , name : String , des : String , userId : String) {
        self.image = image
        self.name = name
        self.des = des
        self.userId = userId
    }
}


struct Media {
    var type:MediaType
    var url: String
    
    enum MediaType {
        case video
        case image
    }
}
