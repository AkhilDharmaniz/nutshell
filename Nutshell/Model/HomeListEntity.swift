//
//  HomeListEntity.swift
//  Nutshell
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 Vivek Dharmani. All rights reserved.
//

import Foundation
//{
//    message = "All post";
//    "post_data" =     (
//                {
//            "created_at" = 1625141995;
//            description = "testing data ";
//            name = "Srinu Reddy";
//            photo = "https://www.dharmani.com/Nutshell/webservice/ProfileImages/60b9c5e36d816_1622787555.png";
//            "post_id" = 14;
//            uploads = "https://www.dharmani.com/Nutshell/webservice/postImg/60ddb2eb4eaefaaa.png";
//            "user_id" = 2;
//            verified = 1;
//        }
//    );
//    status = 1;
//}
struct HomeListData<T>{
  
  
    var status: Int
    var message: String
    var postArray:[PostListData<T>]
   

    init?(dict:[String:T]) {
        let status  = dict["status"] as? Int ?? 0
        let alertMessage = dict["message"] as? String ?? ""
        let  dataArr = dict["post_data"] as? [[String:T]] ?? [[:]]
     
        self.status = status
        self.message = alertMessage
        var hArray = [PostListData<T>]()
        for obj in dataArr{
            let childListObj = PostListData(dataDict:obj)!
            hArray.append(childListObj)
        }
        self.postArray = hArray
    }
}


struct PostListData<T>{
    var created_at:String
    var description:String
    var name:String
    var photo:String
    var post_id:String
    var uploads:String
    var user_id:String
    var verified:String
    var thumbnail : String
    init?(dataDict:[String:T]) {
   
        let created_at = dataDict["created_at"] as? String ?? ""
        let description = dataDict["description"] as? String ?? ""
        let name = dataDict["username"] as? String ?? ""
        let photo = dataDict["photo"] as? String ?? ""
        let post_id = dataDict["post_id"] as? String ?? ""
        let uploads = dataDict["uploads"] as? String ?? ""
        let user_id = dataDict["user_id"] as? String ?? ""
        let verified = dataDict["verified"] as? String ?? ""
        let thumbnail = dataDict["video_thumbnail"] as? String ?? ""
      
        self.created_at = created_at
        self.description = description
        self.name = name
        self.photo = photo
        self.post_id = post_id
        self.uploads = uploads
        self.user_id = user_id
        self.verified = verified
        self.thumbnail = thumbnail
    }

}


struct HomePoatData {
//    internal init(status: String, message: String, userData: <<error type>>, FollowingList: <<error type>>, postData: <<error type>>, ownPostData: <<error type>>) {
//
//    }

    var status : String
    var message : String
    var userData : [UserData]
    var FollowingList : [FollwingLiat]
//    var postData : [PostDataa]
//    var ownPostData : [OwnPost]

    init(status : String,message : String,userData : [UserData],FollowingList : [FollwingLiat]) {
        self.status = status
        self.message = message
        self.userData = userData
        self.FollowingList = FollowingList
//        self.postData = postData
//        self.ownPostData = ownPostData
    }

}

struct UserData {
    var userId : String
    

    init(userId : String) {
        self.userId = userId
        
    }
}

struct FollwingLiat {
    var img : String
    var name : String

    init(img : String,name : String) {
        self.img = img
        self.name = name
    }
}
struct PostDataa {
    var postImage : String
    var link : String
    var title : String

    init(postImage : String,link : String,title : String) {
        self.postImage = postImage
        self.link = link
        self.title = title
    }
}

//String

//struct UserData {
//    let status, message : String
//    let user_data: [UserProfileData]
//    let userfollowlist: [(FollowersListing)]
//}
//
//struct UserProfileData {
//    let user_id, name, email, photo, totalFollowing, totalFollowers, profile_type, changeStatus, url, ratingavg, ratingcount: String
//    let postDetails: [PostDetails]
//}
//
//enum PostDetails {
//    case postData([PostDataa])
//    case string(String)
//}
//
//struct PostDataa {
//    let post_id, user_id, description, uploads, type, video_thumbnail, created_at, name, photo, verified: String
//}
//
//enum FollowersListing {
//    case historyElementArray([FollowersData])
//    case string(String)
//}
//
//struct FollowersData {
//    let user_id, name, email, location, totalFollowing, totalFollowers, changeStatus, profile_type, image: String
//    let postDetailsForFollowers: [ForFollowers]
//}
//
//enum ForFollowers {
//    case postData([PostDataForFollowers])
//    case string(String)
//}
//
//struct PostDataForFollowers {
//    let post_id, user_id, description, uploads, type, video_thumbnail, created_at, name, photo, verified: String
//}


struct HomeDataModal{
    
    var user_id: String
    var name: String
    var created_at: String
    var description: String
    var photo: String
    var post_id: String
    var type: String
    var uploads: String
    var video_thumbnail: String
    var likeCount: String
    var commentCount: String
    var isLike: String
    var ad = Bool()
    init(user_id: String, name : String,created_at : String,description: String, photo: String, post_id: String, type: String, uploads: String,video_thumbnail: String,likeCount: String, commentCount: String, isLike: String,ad:Bool) {
        self.user_id = user_id
        self.name = name
        self.created_at = created_at
        self.description = description
        self.photo = photo
        self.post_id = post_id
        self.type = type
        self.uploads = uploads
        self.video_thumbnail = video_thumbnail
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.isLike = isLike
        self.ad = ad
    }
}
 
struct CommentsModel {
    var comment : String
    var comment_by : String
    var comment_id : String
    var created_at : String
    var name : String
    var photo : String
    var post_id : String
    
   
    init(comment:String,comment_by:String,comment_id:String,created_at:String,name:String,photo:String,post_id:String) {
        self.comment = comment
        self.comment_by = comment_by
        self.comment_id = comment_id
        self.created_at = created_at
        self.name = name
        self.photo = photo
        self.post_id = post_id        
    }
}
struct ReportsModel {
    var reasonId : String
    var reportReasons : String
    var selected: Bool
   
    init(reasonId:String,reportReasons:String,selected:Bool) {
        self.reasonId = reasonId
        self.reportReasons = reportReasons
        self.selected = selected
    }
}

class UserOwnPostDetails : NSObject {
    
    var userID: String = String()
    var name: String = String()
    
    init(data : [String:Any]) {
        self.userID = data["userid"] as? String ?? ""
        self.name = data["username"] as? String ?? ""
    }
}

class FollowerData : NSObject {
    var userID: String = String()
    var name: String = String()
    var arrayData: [UserFollowerDetails] = [UserFollowerDetails]()
    
    init(data : [String:Any]) {
        self.userID = data["userid"] as? String ?? ""
        self.name = data["username"] as? String ?? ""
        
        let postData = data["postDetails"] as? [[String:Any]] ?? [[:]]
        for obj in postData {
            self.arrayData.append(UserFollowerDetails(data: obj))
        }
    }
}


class UserFollowerDetails : NSObject{
    
    var userID: String = String()
    var name: String = String()
    
    init(data : [String:Any]) {
        self.userID = data["userid"] as? String ?? ""
        self.name = data["username"] as? String ?? ""
    }
}
